let productCategoryId =  document.getElementById("productCategoryId")
let ktiOfASubstrate =  document.getElementById("ktiOfASubstrate")
let ktiOfAItemGtCode =  document.getElementById("ktiOfAItemGtCode")
let ktiOfADtsCode =  document.getElementById("ktiOfADtsCode")
let ktiOfAKgPkgConv =  document.getElementById("ktiOfAKgPkgConv")
let ktiOfASluWtConv =  document.getElementById("ktiOfASluWtConv")
let newItem =  document.getElementById("newItem")

//Add new moq with Product category
newItem.onclick = () => {
    fetch(`http://localhost:4000/api/moq/add`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ 
                productCategoryId: productCategoryId.value,
                ktiOfASubstrate: ktiOfASubstrate.value,
                ktiOfAItemGtCode: ktiOfAItemGtCode.value,
                ktiOfADtsCode: ktiOfADtsCode.value,
                ktiOfAKgPkgConv: ktiOfAKgPkgConv.value,
                ktiOfASluWtConv: ktiOfASluWtConv.value,
        })
    }).then(() => {
        alert('Successfully Created Item')
    })
}


let inputProductCategoryId = document.getElementById("inputProductCategoryId")
let inputCheeses = document.getElementById("inputCheeses")
let inputMinimumOrder = document.getElementById("inputMinimumOrder")
let addMoqToProductCategory = document.getElementById("addMoqToProductCategory")
let RemoveMoqToProductCategory = document.getElementById("RemoveMoqToProductCategory")

//push minimumOrder to minimum order quantity array
addMoqToProductCategory.onclick = () => {
    fetch(`http://localhost:4000/api/moq/push`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ 
                productCategoryId: inputProductCategoryId.value,
                Cheeses:  inputCheeses.value,
                minimumOrder: inputMinimumOrder.value
        })
    }).then(() => {
        alert(`Successfully added minimum order to `+`${inputProductCategoryId.value}`)
    })
}

//remove minimumOrder to minimum order quantity array
RemoveMoqToProductCategory.onclick = () => {
    fetch(`http://localhost:4000/api/moq/pull`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ 
                productCategoryId: inputProductCategoryId.value,
                Cheeses:  inputCheeses.value,
                minimumOrder: inputMinimumOrder.value
        })
    }).then(() => {
        alert(`Successfully remove minimum order to `+`${inputProductCategoryId.value}`)
    })
}

let inputforDeleteProductCategoryId = document.getElementById("inputforDeleteProductCategoryId")
let DeleteProductCategory = document.getElementById("DeleteProductCategory")

DeleteProductCategory.onclick = () => {
    fetch(`http://localhost:4000/api/moq/deleteMoq`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ 
                productCategoryId: inputforDeleteProductCategoryId.value,
        })
    }).then(() => {
        alert(`Successfully remove minimum order to `+`${inputProductCategoryId.value}`)
    })
}

//note
// View a Specific MOQ from the database
//Parameter Query String Parameter/ Argument productCategory Id and minimum Order
//http://localhost:4000/api/moq/view/specific?productCategoryId=1569M-036DC&minimumOrder=123

// View All MOQ from the database
//Add queryString for parameter
//http://localhost:4000/api/moq/view/all