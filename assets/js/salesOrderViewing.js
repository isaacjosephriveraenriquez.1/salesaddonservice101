// Retrieving Sales Order No to View
let params = new URLSearchParams(window.location.search)
let docId = params.get('documentId')
let fromWhat = params.get('from')

// Global Variables
let token = localStorage.getItem("token")
let firstName = localStorage.getItem("firstName")
let lastName = localStorage.getItem("lastName")
let accountNameId = ''
let shipToId = ''
let termsId = ''
let taxIdentifier = '';
let employeePosition = '';
let leadTime = '';
let productCategory = '';
let totalPerLineContainer = '';
let accountsReceivableValue = 0;
let productArray = []

let accountNameContainer = document.getElementById('accountName')
let docStatusContainer = document.getElementById('status')
let cslContainer = document.getElementById('currentSpendingLimit')
let addressContainer = document.getElementById('address')
let creditLimitContainer = document.getElementById('creditLimit')
let creationDateContainer = document.getElementById('createdOn')
let shipToContainer = document.getElementById("shipTo")
let salesQuoteNoContainer = document.getElementById('salesOrderId')
let paymentTermsContainer = document.getElementById("paymentTerms")
let salesOrderNoContainer = document.getElementById('sapSoNumber')
let currencyContainer = document.getElementById("currency")
let receivedDateContainer = document.getElementById('receivedDate')
let externalReferenceContainer = document.getElementById('externalReference')
let requestedDateContainer = document.getElementById('requestedDate')
let tpsContainer = document.getElementById('tps')
let deliveryDateContainer = document.getElementById('deliveryDate')
let customerGroupContainer = document.getElementById('customerGroup')
let deliveryPriorityContainer = document.getElementById('deliveryPriority')
let distributionChannelContainer = document.getElementById('distributionChannel')
let salesGroupContainer = document.getElementById('salesGroup')
let salesOriginContainer = document.getElementById('salesOrigin')
let completeDeliveryContainer = document.getElementById('completeDelivery')
let mbAccountIdContainer = document.getElementById('mbAccountId')
let customerInformationContainer = document.getElementById('custInformation')
let overToleranceContainer = document.getElementById('overTolerance')
let underToleranceContainer = document.getElementById('underTolerance')
let internalCommentContainer = document.getElementById('comment')
let employeeNameContainer = document.getElementById('requestor')
let employeeIdContainer = document.getElementById('employeeId')
let deliveryBlockContainer = document.getElementById('deliveryBlock')
let invoiceBlockContainer = document.getElementById('invoiceBlock')
let cancellationReasonContainer = document.getElementById('cancellationReason')
let postingErrorContainer = document.getElementById('postingError')

let productCategoryContainer = document.getElementById('productCategory')
let configContainer = document.getElementById('configId')
let productIdContainer = document.getElementById('productId')
let productDescriptionContainer = document.getElementById("productDescription")
let stockQuantityContainer = document.getElementById('stockQuantity')
let leadTimeContainer = document.getElementById('leadTime')
let partNumberContainer = document.getElementById('partNumber')
let moqContainer = document.getElementById('moq')
let quantityContainer = document.getElementById('quantity')
let uomContainer = document.getElementById("uom")
let listPriceContainer = document.getElementById("listPrice")
let discountContainer = document.getElementById('discount')
let netPriceContainer = document.getElementById('netPrice')
let taxAmountContainer = document.getElementById('taxAmount')
let netValueContainer = document.getElementById('netValue')

let totalNetValueContainer = document.getElementById('totalNetValue')
let totalTaxAmountContainer = document.getElementById('totalTaxAmount')
let itemstotalAmountContainer = document.getElementById('itemstotalAmount')

let addItemsButton = document.getElementById('addItems')

let initialTpsOption = document.getElementById('initialTpsOption')
let initialCustomerGroupOption = document.getElementById('initialCustomerGroupOption')
let initialDeliveryPriorityOption = document.getElementById('initialDeliveryPriorityOption')
let initialDistributionChannelOption = document.getElementById('initialDistributionChannelOption')

// Welcome Banner
let userLoggedIn = document.getElementById('userLoggedIn')
userLoggedIn.innerHTML = `${firstName} ${lastName}`

// Sales Order No.
let documentIdContainer = document.getElementById('documentId')
documentIdContainer.innerHTML = docId

// Logout function
function logoutFunction() {
	localStorage.clear();
	window.location.replace('./logout.html')
}

document.getElementById('logoutButton').onclick = function() {
	logoutFunction();
}

// Populating Fields
fetch(`http://localhost:4000/api/salesOrder/function/view/${docId}`, {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
}).then(res => res.json()).then(data => {
	let splitReceivedDate = data.receivedDate.split('/')
	let splitRequestedDate = data.requestedDate.split('/')
	let updatedRequestedDate = `${splitRequestedDate[2]}-${splitRequestedDate[0]}-${splitRequestedDate[1]}`
	let updatedReceivedDate = `${splitReceivedDate[2]}-${splitReceivedDate[0]}-${splitReceivedDate[1]}`
  
	accountNameId = data.accountId
	accountNameContainer.value = data.accountName
	docStatusContainer.value = data.docStatus
	addressContainer.value = data.address
  	creditLimitContainer.value = data.creditLimit
	creationDateContainer.value = data.creationDate
	shipToId = data.shipToPartyId
	shipToContainer.value = data.shipToPartyDescription
	salesQuoteNoContainer.value = data.salesQuoteNo
	termsId = data.paymentTermsId
	paymentTermsContainer.value = data.paymentTerms
	salesOrderNoContainer.value = data.salesOrderNo
	currencyContainer.value = data.currency
	receivedDateContainer.value = updatedReceivedDate
	externalReferenceContainer.value = data.externalReference
	requestedDateContainer.value = updatedRequestedDate
	initialTpsOption.value = data.tps
	initialTpsOption.innerText = data.tps
	deliveryDateContainer.value = data.deliveryDate
	initialCustomerGroupOption.value = data.customerGroup
	initialCustomerGroupOption.innerText = data.customerGroup
	initialDeliveryPriorityOption.value = data.deliveryPriorityCode
	initialDeliveryPriorityOption.innerText = data.deliveryPriority
	initialDistributionChannelOption.value = data.distributionChannelCode
	initialDistributionChannelOption.innerText = data.distributionChannel
	salesGroupContainer.value = data.salesGroup
	salesOriginContainer.value = data.salesOrigin
	mbAccountIdContainer.value = data.mbAccountId
	customerInformationContainer.value = data.customerInformation
	overToleranceContainer.value = data.overTolerance
	underToleranceContainer.value = data.underTolerance
	internalCommentContainer.value = data.internalComment
	employeeNameContainer.value = data.employeeName
	employeeIdContainer.value = data.employeeId
	deliveryBlockContainer.value = data.deliveryBlockReason
	invoiceBlockContainer.value = data.invoiceBlockReason
	cancellationReasonContainer.value = data.cancelReason
	postingErrorContainer.value = data.postingError

	if(data.completeDelivery === 'true') {
		completeDeliveryContainer.checked = true
	} else {
		completeDeliveryContainer.checked = false
	}

	await = fetch(`http://localhost:4000/getCurrentSpendingLimit/${data.accountId}`, {
		method: 'POST'
	}).then(res => res.json()).then(data => {
        csl = data['AccountOpenAmounts']['0']['CurrentSpendingLimitAmount']['0']['_']

        if(data['AccountOpenAmounts']['0'].hasOwnProperty('ReceivablesBalanceAmount')) {
            accountsReceivableValue = data['AccountOpenAmounts']['0']['ReceivablesBalanceAmount']['0']['_']
        } else {
            accountsReceivableValue = 0
        }
		
        updateCSL(csl);
	})

	let existingProductContainer = document.getElementById("productTableContainer")
	let items;	
	items = data.items.map(result => {
		productArray.push(result)
		return (  
			`
			<tr>
				<td style="text-alig: center;">${productArray.length}</td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.productCategoryId}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.configId}" disabled></td>
        <td><input type="text" style="width: 100%; text-align: center;" value="${result.productId}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.productDescription}" disabled></td>
        <td><input type="text" style="width: 100%; text-align: center;" value="${result.stock}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.leadTime}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.partNumber}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.moq}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${parseFloat(result.quantity).toLocaleString('en-US', { minimumFractionDigits: 0 })}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.unitOfMeasure}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${parseFloat(result.listPrice).toLocaleString('en-US', {minimumFractionDigits: 2})}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.discount}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${parseFloat(result.netPrice).toLocaleString('en-US', {minimumFractionDigits: 2})}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${parseFloat(result.netValue).toLocaleString('en-US', {minimumFractionDigits: 2})}" disabled></td>
        <td><input type="text" style="width: 100%; text-align: center;" value="${parseFloat(result.taxAmount).toLocaleString('en-US', {minimumFractionDigits: 2})}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${parseFloat(result.totalPerLine).toLocaleString('en-US', {minimumFractionDigits: 2})}" disabled></td>
				<td>
					<div class="d-flex justify-content-center">
						<button class="crudButton button btn ms-1 removeButton" id="removeButton" onclick="remove_tr(this)" disabled>
							<i class="far fa-trash-alt"></i>
						</button>
					</div>
				</td>
			</tr>
			`
		)
	}).join("")
	existingProductContainer.innerHTML = items;
}).then(() => {
	fetch(`http://localhost:4000/api/customer/view/specific/${accountNameContainer.value}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(result => {
		taxIdentifier = result.taxStatus
		calculateTotalNetValue();
		calculateTotalTaxAmount();
		calculateTotalAmount();
	})
});

//Update Current Spending Limit - CSL from BYD minus In Preparation and For Approval on SO Add-On
function updateCSL(csl) {
	fetch(`http://localhost:4000/api/salesOrder/allSalesOrderinDatabase/${accountNameId}`,{
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(result => {
		let totalValueForSpecificCustomer = 0

        for(a = 0; a < result.length; a++) {
            let totalAmountPerItem = 0
            totalAmountPerItem = parseFloat(result[a].taxAmount) + parseFloat(result[a].netValue)
            totalValueForSpecificCustomer += totalAmountPerItem
        }

		cslContainer.value = (parseFloat(csl) - parseFloat(totalValueForSpecificCustomer)).toLocaleString('en-US', { minimumFractionDigits: 2 })
	})
}

// Remove Line Item Function
function remove_tr(This) {
	productArray.splice(This.closest('tr').rowIndex - 1, 1);
	This.closest('tr').remove();
	calculateTotalNetValue();
	calculateTotalTaxAmount();
	calculateTotalAmount();
}

// Buttons
let buttonContainer = document.getElementById("buttonRow")

if(localStorage.getItem("isApprover") === "false" && localStorage.getItem("isRequestor") === "false") {
	buttonContainer.innerHTML = 
	`
	<button id="closeButton" class="functionalButtons">Close</button>
 	`

	document.getElementById("closeButton").onclick = function(e) {
		e.preventDefault()
		closeFunctionRequestor()
	}
}

if(localStorage.getItem("isApprover") === "true") {
	buttonContainer.innerHTML = 
	`
	<button id="approveButton" class="functionalButtons">Approve</button>
	<button id="rejectButton" class="functionalButtons">Reject</button>
	<button id="closeButton" class="functionalButtons">Close</button>
 	`

	document.getElementById("approveButton").onclick = function(e) {
		e.preventDefault()
		if(docStatusContainer.value !== 'For Approval') {
			alert(`Sales Quote is already ${docStatusContainer.value}`)
		} else {
			askInvoiceBlockModalFunction()
		}
	}

	document.getElementById("rejectButton").onclick = function(e) {
		e.preventDefault()
		if(docStatusContainer.value !== 'For Approval') {
			alert(`Sales Quote is already ${docStatusContainer.value}`)
		} else {
			rejectFunction()
		}
	}

	document.getElementById("closeButton").onclick = function(e) {
		e.preventDefault()
		closeFunctionApprover()
	}
};

// Approve Function - Ask if the Approver wants to set Invoice Block or Not
function askInvoiceBlockModalFunction() {
	let myModal = document.getElementById("yesOrNoModal");
	$(myModal).modal("show")

	let closeButtonModal = document.getElementById('closeYesOrNoModalButton');
	closeButtonModal.onclick = function() {
		$(myModal).modal("hide")
	}

	window.onclick = function(event) {
		if (event.target === myModal) {
			$(myModal).modal("hide")
		}
	}

	let yesButton = document.getElementById('yesButton')
	yesButton.onclick = function() {
		setInvoiceBlockModalFunction()
		$(myModal).modal("hide")
	}

	let noButton = document.getElementById('noButton')
	noButton.onclick = function() {
		approveFunction()
		$(myModal).modal("hide")
	}
}

// To select Invoice Block if Approver selected Yes Button
function setInvoiceBlockModalFunction() {
	let myModal = document.getElementById("invoiceBlockModal");
	$(myModal).modal("show")

	let closeButtonModal = document.getElementById('closeInvoiceBlockModalButton');
	closeButtonModal.onclick = function() {
		$(myModal).modal("hide")
	}

	window.onclick = function(event) {
		if (event.target === myModal) {
			$(myModal).modal("hide")
		}
	}

	let invoiceBlockModalButton = document.getElementById('invoiceBlockModalButton')
	invoiceBlockModalButton.onclick = function() {
		let reasonCode = document.getElementById('invoiceBlockSelection').value
		let reasonDescription = document.getElementById('invoiceBlockSelection').options[invoiceBlockSelection.selectedIndex].text
		setInvoiceBlock(reasonCode, reasonDescription)
		$(myModal).modal("hide")
	}
}

// To set Invoice Block Reason
function setInvoiceBlock(reasonCode, reasonDescription) {
	fetch(`http://localhost:4000/api/salesOrder/function/setInvoiceBlock/${docId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			invoiceBlockReasonCode: reasonCode,
			invoiceBlockReason: reasonDescription
		})
	}).then(() => {
		approveFunction()
		alert('Successfully Set Invoice Block and Sales Quote Approved')
		window.location.reload()
	})
}

// Approve Function
// function approveFunction() {
//     let validatorValue = `${firstName} ${lastName}`

//     fetch(`http://localhost:4000/api/salesOrder/function/approve/${docId}`, {
// 		method: 'POST',
//         headers: {
//             'Content-Type': 'application/json'
//         },
//         body: JSON.stringify({
//             validator: validatorValue
//         })
// 	}).then(() => {
// 		document.getElementById('header').setAttribute('class', 'blur')
//         document.getElementById('salesOrderViewingForm').setAttribute('class', 'blur')

//         $("#myimgdivshowhide").show()

//         statusChecker()
// 	})
// }

// Reject Function
function rejectFunction() {
	fetch(`http://localhost:4000/api/salesOrder/function/reject/${docId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(result => {
		alert('Successfully Rejected the Sales Quote')
		window.location.reload()
	})
}

// Close Function
function closeFunctionApprover(){
	window.location.replace('./salesOrderMonitoring.html')
}

let buttonContainerView = document.getElementById("buttonRow")

if(localStorage.getItem("isRequestor") === "true") {

	buttonContainerView.innerHTML =
	`
	<button id="newButton" class="functionalButtons">New</button>
	<button id="saveButton" class="functionalButtons">Save</button>
	<button id="editButton" class="functionalButtons">Edit</button>
	<button id="postButton" class="functionalButtons">Post</button>
	<button id="cancelButton" class="functionalButtons">Cancel</button>	
	<button id="closeButton" class="functionalButtons">Close</button>
	`

	document.getElementById('newButton').onclick = function(e) {
		e.preventDefault();
		newFunction();
	}

	document.getElementById('saveButton').onclick = function(e) {
		e.preventDefault();
		saveFunction();
	};

	document.getElementById('editButton').onclick = function(e) {
		e.preventDefault();
		if(docStatusContainer.value === 'In Preparation') {
			editFunction()
		} else {
			alert(`Error! Sales Quote is already ${docStatusContainer.value}`)
		}
	};

	document.getElementById('postButton').onclick = function(e){
		e.preventDefault()
		if(docStatusContainer.value !== 'In Preparation') {
			alert(`Error! Sales Quote is already ${docStatusContainer.value}`)
		} else if(cslContainer.value === '') {
			alert(`Please Wait for Current Spending Limit (CSL) Amount`)
		} else if(parseFloat(cslContainer.value.replace(/,/g, "")) < 0 || parseFloat(itemstotalAmountContainer.value) > parseFloat(cslContainer.value.replace(/,/g, ""))) {
			addAccountsReceivableWithIssue()
		} else {
			addAccountsReceivableWithoutIssue();
		}
	}

	document.getElementById("cancelButton").onclick = function(e) {
		e.preventDefault();
		if(docStatusContainer.value !== 'In Preparation') {
			alert(`Error! Sales Quote is already ${docStatusContainer.value}`)
		} else {
			cancelModalFunction();
		}
	}

	document.getElementById("closeButton").onclick = function(e) {
		e.preventDefault();
		closeFunctionRequestor();
	}
}

// New Function
function newFunction() {
	window.location.replace('./salesOrderCreation.html')
}

// Save Function
function saveFunction() {
	accountNameContainer.setAttribute('readonly', true)
	paymentTermsContainer.setAttribute('readonly', true)
	currencyContainer.setAttribute('readonly', true)
	receivedDateContainer.setAttribute('readonly', true)
	externalReferenceContainer.setAttribute('readonly', true)
	requestedDateContainer.setAttribute('readonly', true)
	tpsContainer.setAttribute('disabled', true)
	customerGroupContainer.setAttribute('disabled', true)
	deliveryPriorityContainer.setAttribute('disabled', true)
	distributionChannelContainer.setAttribute('disabled', true)
  salesOriginContainer.setAttribute('disabled', true)
	customerInformationContainer.setAttribute('readonly', true)
	internalCommentContainer.setAttribute('readonly', true)
	productCategoryContainer.setAttribute('readonly', true)
	addItemsButton.setAttribute('disabled', true)
	document.getElementById("removeButton").setAttribute('disabled', true)

	// Setting disabled attribute into old product array
	let oldItems = document.getElementById("productTableContainer");
	let oldRemove = oldItems.getElementsByTagName("button");

	for(let i = 0; i < oldRemove.length; i++) {
		oldRemove[i].setAttribute('disabled', true)
	}

	let completeDeliveryValue = false;

	if(completeDeliveryContainer.checked) {
		completeDeliveryValue = true
	}

  let splitReceivedDate = receivedDateContainer.value.split('-')
	let splitRequestedDate = requestedDateContainer.value.split('-')

  let updatedReceivedDate = `${splitReceivedDate[1]}/${splitReceivedDate[2]}/${splitReceivedDate[0]}`
	let updatedRequestedDate = `${splitRequestedDate[1]}/${splitRequestedDate[2]}/${splitRequestedDate[0]}`

	fetch(`http://localhost:4000/api/salesOrder/function/edit/${docId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			accountId: accountNameId,
			accountName: accountNameContainer.value,
			address: addressContainer.value,
			shipToPartyId: shipToId,
			shipToPartyDescription: shipToContainer.value,
			paymentTermsId: termsId,
			paymentTerms: paymentTermsContainer.value,
			currency: currencyContainer.value,
			receivedDate: updatedReceivedDate,
			externalReference: externalReferenceContainer.value,
			requestedDate: updatedRequestedDate,
			tps: tpsContainer.value,
			deliveryDate: deliveryDateContainer.value,
			customerGroup: customerGroupContainer.options[customerGroupContainer.selectedIndex].text,
			customerGroupCode: customerGroupContainer.value,
			deliveryPriority: deliveryPriorityContainer.options[deliveryPriorityContainer.selectedIndex].text,
			deliveryPriorityCode: deliveryPriorityContainer.value,
			distributionChannel: distributionChannelContainer.options[distributionChannelContainer.selectedIndex].text,
			distributionChannelCode: distributionChannelContainer.value,
			salesGroup: salesGroupContainer.value,
			salesOrigin: salesOriginContainer.value,
			completeDelivery: completeDeliveryValue,
			mbAccountId: mbAccountIdContainer.value,
			customerInformation: customerInformationContainer.value,
			overTolerance: overToleranceContainer.value,
			underTolerance: underToleranceContainer.value,
			internalComment: internalCommentContainer.value,
			items: productArray
		})
	}).then(() => {
		alert('Successfully Edited the Sales Quote')
		historyTrail()
		alert("Audit History Saved!")
		window.location.reload()
	})
}

// Edit Function
function editFunction() {
	accountNameContainer.removeAttribute('readonly')
	paymentTermsContainer.removeAttribute('readonly')
	currencyContainer.removeAttribute('readonly')
	receivedDateContainer.removeAttribute('readonly')
	externalReferenceContainer.removeAttribute('readonly')
	requestedDateContainer.removeAttribute('readonly')
	tpsContainer.removeAttribute('disabled')
	customerGroupContainer.removeAttribute('disabled')
	deliveryPriorityContainer.removeAttribute('disabled')
	distributionChannelContainer.removeAttribute('disabled')
  salesOriginContainer.removeAttribute('disabled')
	customerInformationContainer.removeAttribute('readonly')
	internalCommentContainer.removeAttribute('readonly')
	productCategoryContainer.removeAttribute('readonly')
	addItemsButton.removeAttribute('disabled')
	document.getElementById("removeButton").removeAttribute('disabled')

	// Removing disabled attribute into old product array
	let oldItems = document.getElementById("productTableContainer");
	let oldRemove = oldItems.getElementsByTagName("button");

	for(let i = 0; i < oldRemove.length; i++) {
		oldRemove[i].removeAttribute('disabled')
	}

	// Change Status to In Preparation
	let inStatus = document.getElementById("status")
	inStatus.value = 'In Preparation'
	
	//Look Up - Account Name
	let accountsDatalist = document.getElementById("accounts")
	let accounts;

	fetch('http://localhost:4000/api/customer/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		accounts = data.map(result => {
			return  (
				`
				<option value="${result.accountName}">
				${result.accountId} - ${result.searchName}
				</option>
				`
			)
		})
		accountsDatalist.innerHTML = accounts;
	}) 
	
	//Populate Fields - Address, Ship To, Payment Terms, Currency, Customer Group, Sales Group, MB Account ID, Over Tolerance, Under Tolerance, Tax Identifier
	function populateCustomerFields() {
		fetch(`http://localhost:4000/api/customer/view/specific/${accountNameContainer.value}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			accountNameId = data.accountId
			addressContainer.value = data.address
			shipToId = data.shipToPartyId
			shipToContainer.value = data.shipToPartyDescription
			termsId = data.paymentTermsId
			paymentTermsContainer.value = data.paymentTerms
			currencyContainer.value = data.currency
			customerGroupContainer.value = data.customerGroup
			salesGroupContainer.value = data.salesGroup
			mbAccountIdContainer.value = data.mbAccountId
			overToleranceContainer.value = data.overTolerance
			underToleranceContainer.value = data.underTolerance
			taxIdentifier = data.taxStatus
		}).then(() => {
			fetchBydDetails()
		})
	}

	// Retrieve Current Spending Limit from BYD
	function fetchBydDetails() {
        fetch(`http://localhost:4000/getCurrentSpendingLimit/${accountNameId}`, {
            method: 'POST'
        }).then(res => res.json()).then(data => {
            csl = data['AccountOpenAmounts']['0']['CurrentSpendingLimitAmount']['0']['_']
            updateCSL(csl);
        })
    }

	//Update Current Spending Limit - CSL from BYD minus In Preparation and For Approval on SO Add-On
	function updateCSL(csl) {
		fetch(`http://localhost:4000/api/salesOrder/allSalesOrderinDatabase/${accountNameId}`,{
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(result => {
		let totalValueForSpecificCustomer = 0

        for(a = 0; a < result.length; a++) {
            let totalAmountPerItem = 0
            totalAmountPerItem = parseFloat(result[a].taxAmount) + parseFloat(result[a].netValue)
            totalValueForSpecificCustomer += totalAmountPerItem
        }

		cslContainer.value = (parseFloat(csl) - parseFloat(totalValueForSpecificCustomer)).toLocaleString('en-US', { minimumFractionDigits: 2 })
	})
	}

	accountNameContainer.oninput = function() {
		populateCustomerFields()
	};

	// Look Up - Payment Terms
	let termsDatalist = document.getElementById('terms')
	let terms;

	fetch('http://localhost:4000/api/terms/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		terms = data.map(result => {
			return  (
				`
				<option value="${result.paymentTermsDescription}">
				${result.paymentTermsId}
				</option>
				`
			)
		})
		termsDatalist.innerHTML = terms;
	}) 

	//Look Up - Currency
	let currencyDatalist = document.getElementById('currencies')
	let curr;
	fetch('http://localhost:4000/api/currency/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		curr = data.map(result => {
			return  (
				curr =
				`
				<option value="${result.currencyTicker.toUpperCase()}">
				${result.currencyName}
				</option>
				`
			)
		})
		currencyDatalist.innerHTML = curr;
	}) 

	// Look Up - TPS
	let tps;
	fetch('http://localhost:4000/api/tps/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		tps = data.map(result => {
			return  (
				`
				<option value="${result.code} - ${result.vendorAccount}">${result.code} - ${result.vendorAccount}</option>
				`
			)
		})
		tpsContainer.innerHTML += tps;      
	})

	//Look Up - Customer Group
	let grp;
	fetch('http://localhost:4000/api/customerGroup/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		grp = data.map(result => {
			return  (
				`
				<option value="${result.code}">${result.description}</option>
				`
			)
		})
		customerGroupContainer.innerHTML += grp;      
	})

	//Look Up - Distribution Channel
	let chnl;
	fetch('http://localhost:4000/api/distributionChannel/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		chnl = data.map(result => {
			return  (
				`
				<option value="${result.code}">${result.description}</option>
				`
			)
		})
		distributionChannelContainer.innerHTML += chnl;      
	})

	// Generate Order Delivery Date
	function generateOrderDeliveryDate() {
		let deliveryDate = document.getElementById('deliveryDate')
		let today = new Date()
		let newDate = new Date()
		newDate.setDate(today.getDate() + parseInt(leadTime))

		newMonth = newDate.toLocaleString('default', {month: 'long'})

		if(newMonth === 'January'){
			newMonth = '01'
		} else if(newMonth === 'February') {
			newMonth = '02'
		} else if(newMonth === 'March') {
			newMonth = '03'
		} else if(newMonth === 'April') {
			newMonth = '04'
		} else if(newMonth === 'May') {
			newMonth = '05'
		} else if(newMonth === 'June') {
			newMonth = '06'
		} else if(newMonth === 'July') {
			newMonth = '07'
		} else if(newMonth === 'August') {
			newMonth = '08'
		} else if(newMonth === 'September') {
			newMonth = '09'
		} else if(newMonth === 'October') {
			newMonth = '10'
		} else if(newMonth === 'November') {
			newMonth = '11'
		} else if(newMonth === 'December') {
			newMonth = '12'
		}

		newDay = newDate.getDate()
		
		if(newDay.toString().length === 1) {
			newDay = `0${newDay}`
		}
		
		newYear = newDate.getFullYear()
		
		deliveryDate.value = `${newMonth}/${newDay}/${newYear}`
	}

	// Look Up - Product Categories
	let productDatalist = document.getElementById('productCategories')
	let product;
	fetch('http://localhost:4000/api/productCategory/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		data.sort()
		product = data.map(result => {
			return  (
				`
				<option value="${result.categoryId}"></option>
				`
			)
		})
		productDatalist.innerHTML = product;
	})

	// To clear Config ID Datalist
	function clearConfigIdDatalist() {
		let configDatalist = document.getElementById('configIds')
		configDatalist.innerHTML = ''
		configContainer.value = ''
	}

	// Look Up - Materials with Product Category indicated
    function configIdLookUp() {
        let configDatalist = document.getElementById('configIds')
        let configs;
        fetch(`http://localhost:4000/api/products/view/byProductCategory/${productCategoryContainer.value}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            data.sort()
            configs = data.map(result => {
                return  (
                    `
                    <option value="${result.configId}">${result.materialDescription}</option>
                    `
                )
            })
            configDatalist.innerHTML = configs;
        }).then(() => {
            validateMoq()
        })
    }

	productCategoryContainer.oninput = function() {
		configContainer.removeAttribute('readonly')
		clearConfigIdDatalist()
		configIdLookUp()
	};

    // To Concatenate Product Category ID and Config ID
    function concatenateIds() {
        if(configContainer.value.length > 4) {
            productIdContainer.value = `${productCategoryContainer.value}-${configId.value}`
        } else {
            productIdContainer.value = ""
        }
        getStockCounts()
    }

    // To Retrieve Stock Quantity of the Item
    function getStockCounts() {
        let materialId = productIdContainer.value
        
        fetch(`http://localhost:4000/getCurrentStockQuantity/${materialId}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            return res.json()
        }).then(result => {
            stockQuantityContainer.value = result.toLocaleString('en-US', { minimumFractionDigits: 0 })
        })
    }

	//Populate Fields - Product Description, Lead Time, Part Number, Stocks, MOQ, UoM, List Price, Net Price
    function populateProductFields() {
        fetch(`http://localhost:4000/api/products/view/specific/${productIdContainer.value}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            productDescriptionContainer.value = data.materialDescription
            leadTimeContainer.value = data.leadTime
            partNumberContainer.value = data.partNumber
            uomContainer.value = data.uomDescription
            listPriceContainer.value = data.price
            netPriceContainer.value = data.price

            if(leadTime === '') {
                leadTime = data.leadTime
            } else if(leadTime < parseInt(data.leadTime)) {
                leadTime = data.leadTime
            }
        })
    }

	// Look-Up and Validation of Minimum Order Quantity
    function validateMoq() {
        fetch(`http://localhost:4000/api/moq/validate/${productCategoryContainer.value}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(result => {
            if(result === true) {
                fetch(`http://localhost:4000/api/moq/view/specific/${productCategoryContainer.value}`, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json()).then(data => {
                    moqContainer.value = data.moq[0]
                })
            } else {
                moqContainer.value = ''
            }
        })
    }

	function enableFields() {
		uomContainer.removeAttribute('readonly')
		quantityContainer.removeAttribute('readonly')
		discountContainer.removeAttribute('readonly')
		partNumberContainer.removeAttribute('readonly')
	
		if(employeePosition === 'Customer Service Supervisor') {
			listPriceContainer.removeAttribute('readonly')
		}
	}

	//Look Up - Unit of Measure
	function uomLookUp() {
		let uomDatalist = document.getElementById('uoms')
		let uom;
		fetch('http://localhost:4000/api/uom/view/all', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			uom = data.map(result => {
				return  (
					uom =
					`
					<option value="${result.description}">
					${result.code}
					</option>
					`
				)
			})
			uomContainer.innerHTML = uom;
		})
	}

	configContainer.oninput = function() {
        concatenateIds()
        populateProductFields()
        enableFields()
        uomLookUp()
    }
	
	// Generating Net Value
	function generateNetValue() {
		let quantityValue = document.getElementById('quantity').value
		let netValueContainer = document.getElementById('netValue')
		let listPriceValue = document.getElementById('listPrice').value

		netValueContainer.value = quantityValue * listPriceValue
	};

	quantityContainer.oninput = function() {
		generateNetValue()
	};

	// Retrieving Discount Amount to Change Net Price
	function applyDiscount(discountValue) {
		let netPriceValue = netPriceContainer.value
		let listPriceValue = listPriceContainer.value
		netPriceValue = listPriceValue;
		discountValue = discountValue/100
		netPriceValue = (netPriceValue * (1 - discountValue)).toFixed(2)

		changeNetPrice(netPriceValue)
	}

	// Changing the Net Price based on Discount Field
	function changeNetPrice(netPriceValue) {
		let netPriceContainer = document.getElementById("netPrice")
		let netValueContainer = document.getElementById("netValue")
		let quantityValue = document.getElementById("quantity").value

		netPriceContainer.value = netPriceValue
		netValueContainer.value = (quantityValue * netPriceContainer.value).toFixed(2)

		calculateTaxAmount();
	}

	// Calculating Tax
	function calculateTaxAmount() {
		let netValueValue = document.getElementById("netValue").value
		let taxAmountContainer = document.getElementById("taxAmount")

		if(taxIdentifier === 'Vatable') {
			taxAmountContainer.value = (netValueValue * 0.12).toFixed(2)
		} else {
			taxAmountContainer.value = netValueValue * 0
		}
		
		itemsPush();
		itemsDisplay();
		clearFunction();
	}

	//Array for Products/Items
	function itemsPush() {	
		perLineTotal = parseFloat(taxAmountContainer.value) + parseFloat(netValueContainer.value)
		productArray.push({
			productCategoryId: productCategoryContainer.value,
			configId: configContainer.value,
            productId: productIdContainer.value,
			productDescription: productDescriptionContainer.value,
            stock: stockQuantityContainer.value,
			leadTime: leadTimeContainer.value,
			partNumber: partNumberContainer.value,
			moq: moqContainer.value,
			quantity: quantityContainer.value,
			unitOfMeasure: uomContainer.value,
			listPrice: listPriceContainer.value,
			discount: discountContainer.value,
			netPrice: netPriceContainer.value,
			taxAmount: taxAmountContainer.value,
			netValue: netValueContainer.value,
			totalPerLine: perLineTotal
		})
	}

	// Add Row
	let productTableContainer = document.getElementById("productTableContainer");
	let items;

	function itemsDisplay() {
		let a = 0;
		items = productArray.map(itemsList=> {
			return(
				a++,
				`
				<tr>
					<td>${a}</td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.productCategoryId}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.configId}" disabled></td>
                    <td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.productId}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.productDescription}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.stock}" disabled></td>
                    <td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.leadTime}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.partNumber}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.moq}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.quantity}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.unitOfMeasure}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.listPrice}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.discount}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.netPrice}" disabled></td>
                    <td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.netValue}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.taxAmount}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.totalPerLine}" disabled></td>
					<td>
					<div class="d-flex justify-content-center">
						<button class="crudButton button btn ms-1" id="removeButton" onclick="remove_tr(this)">
							<i class="far fa-trash-alt"></i>
						</button>
					</div>
					</td>
				</tr>
				`
			)
		}).join("");
		productTableContainer.innerHTML = items; 
	}

	// To Clear Look Ups
	function clearFunction() {
		productCategoryContainer.value = ''
		configContainer.value = ''
        productIdContainer.value = ''
		productDescriptionContainer.value = ''
        stockQuantityContainer.value = ''
		leadTimeContainer.value = ''
		partNumberContainer.value = ''
		moqContainer.value = ''
		quantityContainer.value = ''
		uomContainer.value = ''
		listPriceContainer.value = ''
		discountContainer.value = ''
		netPriceContainer.value = ''
		taxAmountContainer.value = ''
		netValueContainer.value = ''
	}

	function disableFields() {
		configContainer.setAttribute('readonly', true)
		uomContainer.setAttribute('readonly', true)
		quantityContainer.setAttribute('readonly', true)
		discountContainer.setAttribute('readonly', true)
		listPriceContainer.setAttribute('readonly', true)
		partNumberContainer.setAttribute('readonly', true)
	}

	document.getElementById("addItems").onclick = function(e) {
        e.preventDefault();
        if(parseFloat(quantityContainer.value) < parseFloat(moqContainer.value)) {
            let moqQty = document.getElementById('moqQty')
            moqQty.innerText = moqContainer.value
    
            let myModal = document.getElementById("acceptOrRejectModal");
            $(myModal).modal("show")
    
            let closeButtonModal = document.getElementById('closeAcceptOrRejectModalButton');
            closeButtonModal.onclick = function() {
                $(myModal).modal("hide")
            }
    
            window.onclick = function(event) {
                if (event.target === myModal) {
                    $(myModal).modal("hide")
                }
            }
    
            let acceptModalButton = document.getElementById('acceptModalButton')
            acceptModalButton.onclick = function() {
                acceptFunction()
                $(myModal).modal("hide")
            }
    
            let rejectModalButton = document.getElementById('rejectModalButton')
            rejectModalButton.onclick = function() {
                rejectFunction()
                $(myModal).modal("hide")
            }
        } else {
            let discountValue = discountContainer.value
            applyDiscount(discountValue);
            disableFields();
            generateOrderDeliveryDate();
            calculateTotalNetValue();
            calculateTotalTaxAmount();
            calculateTotalAmount();
        }		
    };

    // Accept Function
    function acceptFunction() {
        quantityContainer.value = moqContainer.value
        let discountValue = discountContainer.value
        applyDiscount(discountValue);
        disableFields();
        generateOrderDeliveryDate();
        calculateTotalNetValue();
        calculateTotalTaxAmount();
        calculateTotalAmount();
    }

    // // Reject Function
    // function rejectFunction() {
    //     let discountValue = discountContainer.value
    //     applyDiscount(discountValue);
    //     disableFields();
    //     generateOrderDeliveryDate();
    //     calculateTotalNetValue();
    //     calculateTotalTaxAmount();
    //     calculateTotalAmount();
    // }

	// Remove Functions
	function remove_tr(This) {
		productArray.splice(This.closest('tr').rowIndex - 1, 1);
		This.closest('tr').remove();

		calculateTotalNetValue();
		calculateTotalTaxAmount();
		calculateTotalAmount();
	}
}

// Calculating Total Net Value of Sales Quote
function calculateTotalNetValue() {
	let sumOfAllNetValue = 0
	productArray.forEach(item => {
		sumOfAllNetValue += parseFloat(item.netValue)
	})
	totalNetValueContainer.value = sumOfAllNetValue.toLocaleString('en-US', { minimumFractionDigits: 2 })
}

// Calculating Total Tax Amount of Sales Quote
function calculateTotalTaxAmount() {
	let sumOfAllTaxAmount = 0
	productArray.forEach(item => {
		sumOfAllTaxAmount += parseFloat(item.taxAmount)
	})
	totalTaxAmountContainer.value = sumOfAllTaxAmount.toLocaleString('en-US', { minimumFractionDigits: 2 })
}

// Calculating Total Gross Amount of Sales Quote
function calculateTotalAmount() {
	let sumOfAllItemsTotalAmount = 0
	productArray.forEach(item => {
		sumOfAllItemsTotalAmount += parseFloat(item.taxAmount) + parseFloat(item.netValue)
	})
	itemstotalAmountContainer.value = sumOfAllItemsTotalAmount.toLocaleString('en-US', { minimumFractionDigits: 2 })
}

// Add Accounts Receivable Upon Post - With Credit Limit Issue
function addAccountsReceivableWithIssue() {
    fetch(`http://localhost:4000/api/salesOrder/add/accountsReceivable/${docId}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            accountsReceivable: accountsReceivableValue
        })
    }).then(() => {
        setDeliveryBlock()
    })
}

// To Set Delivery Block Reason if there is Credit Limit Issue
// function deliveryBlockModalFunction() {
// 	let myModal = document.getElementById("deliveryBlockModal");
// 	$(myModal).modal("show")

// 	let closeButtonModal = document.getElementById('closeDeliveryBlockModalButton')
// 	closeButtonModal.onclick = function() {
// 		$(myModal).modal("hide")
// 	}

// 	window.onclick = function(event) {
// 		if (event.target === myModal) {
// 			$(myModal).modal("hide")
// 		}
// 	}

// 	let deliveryBlockButton = document.getElementById('deliveryBlockModalButton')
// 	deliveryBlockButton.onclick = function() {
// 		let reasonCode = document.getElementById('deliveryBlockSelection').value
// 		let reasonDescription = document.getElementById('deliveryBlockSelection').options[deliveryBlockSelection.selectedIndex].text
// 		setDeliveryBlock(reasonCode, reasonDescription)
// 		$(myModal).modal("hide")
// 	}
// }

// To Save Delivery Block Reason
function setDeliveryBlock() {
	fetch(`http://localhost:4000/api/salesOrder/function/setDeliveryBlock/${docId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			deliveryBlockReasonCode: '01',
			deliveryBlockReason: 'Credit Limit',
			docStatus: 'For Approval'
		})
	}).then(() => {
        // Email to Approver
		alert('Successfully Set Delivery Block and Sales Quote Submitted For Approval')
		window.location.reload()
	})
}

// Add Accounts Receivable Upon Post - Without Credit Limit Issue
function addAccountsReceivableWithoutIssue() {
    fetch(`http://localhost:4000/api/salesOrder/add/accountsReceivable/${docId}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            accountsReceivable: accountsReceivableValue
        })
    }).then(() => {
        postFunction()
    })
}

// Post Function
function postFunction() {
    let validatorValue = `${firstName} ${lastName}`
    
	fetch(`http://localhost:4000/api/salesOrder/function/post/${docId}`, {
		method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            validator: validatorValue
        })
	}).then(() => {
		document.getElementById('header').setAttribute('class', 'blur')
        document.getElementById('salesOrderViewingForm').setAttribute('class', 'blur')

        $("#myimgdivshowhide").show()

        statusChecker()
	})
}

async function statusChecker() {
    let response = await fetch(`http://localhost:4000/api/salesOrder/function/viewStatus/${docId}`)
    let updatedResponse = await response.json()

    if(updatedResponse.docStatus === "In Preparation" || updatedResponse.docStatus === "For Approval") {
        statusChecker()
    } else {
        $("#myimgdivshowhide").hide()
        window.location.reload()
    }
}

// Cancel Modal Function
function cancelModalFunction() {
	let myModal = document.getElementById("cancelModal");
	$(myModal).modal("show")

	let closeButtonModal = document.getElementById('closeCancelModalButton')
	closeButtonModal.onclick = function() {
		$(myModal).modal("hide")
	}

	window.onclick = function(event) {
		if (event.target === myModal) {
			$(myModal).modal("hide")
		}
	}

	let cancelButton = document.getElementById('cancelModalButton')
	cancelButton.onclick = function() {
		let explanation = document.getElementById('explanation').value
		cancelFunction(explanation)
		$(myModal).modal("hide")
	}
}

// Cancel Function
function cancelFunction(explanation) {
    let validatorValue = `${firstName} ${lastName}`

	fetch(`http://localhost:4000/api/salesOrder/function/cancel/${docId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			cancelReason: explanation,
            validator: validatorValue
		})
	}).then(() => {
		alert('Successfully Cancelled the Sales Quote')
		window.location.reload()
	})
}

// Close Function
function closeFunctionRequestor() {
	if(typeof(fromWhat) === 'string') {
		window.location.replace('./allSalesOrder.html')
	} else {
		window.location.replace('./salesOrderMonitoring.html')	
	}
}

// Auditrail Function
function historyTrail() {
    fetch(`http://localhost:4000//api/History/addHistory`, {
		method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
			body: JSON.stringify({
				accountId: accountNameId,
				accountName: accountNameContainer.value,
				address: addressContainer.value,
				shipToPartyId: shipToId,
				shipToPartyDescription: shipToContainer.value,
				paymentTermsId: termsId,
				paymentTerms: paymentTermsContainer.value,
				currency: currencyContainer.value,
				receivedDate: updatedReceivedDate,
				externalReference: externalReferenceContainer.value,
				requestedDate: updatedRequestedDate,
				tps: tpsContainer.value,
				deliveryDate: deliveryDateContainer.value,
				customerGroup: customerGroupContainer.options[customerGroupContainer.selectedIndex].text,
				customerGroupCode: customerGroupContainer.value,
				deliveryPriority: deliveryPriorityContainer.options[deliveryPriorityContainer.selectedIndex].text,
				deliveryPriorityCode: deliveryPriorityContainer.value,
				distributionChannel: distributionChannelContainer.options[distributionChannelContainer.selectedIndex].text,
				distributionChannelCode: distributionChannelContainer.value,
				salesGroup: salesGroupContainer.value,
				salesOrigin: salesOriginContainer.value,
				completeDelivery: completeDeliveryValue,
				mbAccountId: mbAccountIdContainer.value,
				customerInformation: customerInformationContainer.value,
				overTolerance: overToleranceContainer.value,
				underTolerance: underToleranceContainer.value,
				internalComment: internalCommentContainer.value,
				items: productArray,
				dateChange: dateChange,
				changeBy: firstName + lastName
        })
	}).then(() => {
		document.getElementById('header').setAttribute('class', 'blur')
        document.getElementById('salesOrderViewingForm').setAttribute('class', 'blur')

        $("#myimgdivshowhide").show()

        statusChecker()
	})
}