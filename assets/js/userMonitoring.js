// Global Variables
let firstName = localStorage.getItem('firstName')
let lastName = localStorage.getItem('lastName')
let isApprover = localStorage.getItem('isApprover')
let isRequestor = localStorage.getItem('isRequestor')

let selectedUsers = []

if(localStorage.getItem('token') === null) {
	window.location.replace('../index.html')
}

// Welcome Banner
let userLoggedIn = document.getElementById('userLoggedIn')
userLoggedIn.innerHTML = `${firstName} ${lastName}`

// Logout Function
function logoutFunction() {
	localStorage.clear();
	window.location.replace('./logout.html')
}

document.getElementById("logoutButton").onclick = function() {
	logoutFunction();
}

// Buttons
let buttonContainer = document.getElementById("buttonContainer")

buttonContainer.innerHTML = 
`
<button id="registerButton">Register User</button>
<button id="viewButton">View User</button>
<button id="enableOrDisableButton">Enable/Disable User</button>
<button id="closeButton">Close</button>
`

document.getElementById('registerButton').onclick = function(e) {
    e.preventDefault()
    registerFunction()
}

document.getElementById('viewButton').onclick = function(e) {
    e.preventDefault()
    getValuesOfCheckedBox()
    viewFunction()
}

document.getElementById("enableOrDisableButton").onclick = function(e) {
    e.preventDefault()
    getValuesOfCheckedBox()
    enableOrDisableFunction()
}

document.getElementById("closeButton").onclick = function(e) {
    e.preventDefault()
    closeFunction();
}

// Select All Checkbox
function toggle(source) {
    let checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (let i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source) {
            checkboxes[i].checked = source.checked;
        }
    }
}

// Selecting Checkbox
function getValuesOfCheckedBox() {
    //Reference the Table.
    let grid = document.getElementById("salesQuoteTable");

    //Reference the CheckBoxes in Table.
    let checkBoxes = grid.getElementsByTagName("INPUT");
    
    //Loop through the CheckBoxes.
    for (let i = 0; i < checkBoxes.length; i++) {
        if (checkBoxes[i].checked) {
            let row = checkBoxes[i].parentNode.parentNode
            data = {
                employeeId: row.cells[1].innerText,
                employeeName: row.cells[2].innerText,
                isActive: row.cells[5].innerHTML

            }
            selectedUsers.push(data)
        }
    }
}

// Register Function
function registerFunction() {
    window.location.replace('./userCreation.html')
}

// View Function
function viewFunction() {
    if(selectedUsers.length < 1) {
        alert('Please select a User')
    } else if(selectedUsers.length > 1) {
        selectedUsers.length = 0
        alert('Action cannot be performed on multiple line items. Please select a single line item')
    } else {
        let employeeId = selectedUsers[0].employeeId
        window.location.replace(`./userViewing.html?documentId=${employeeId}`)
    }
}

// Enable or Disable Function
function enableOrDisableFunction() {
    if(selectedUsers.length < 1) {
        alert('Please select a User')
    } else if (selectedUsers.length > 1) {
        alert('Action cannot be performed on multiple line items. Please select a single line item')
    } else {
        let documentId = selectedUsers[0].employeeId
        document.getElementById('empName').innerText = selectedUsers[0].employeeName

        let myModal = document.getElementById("exampleModal");
        $(myModal).modal("show")

        let closeButtonModal = document.getElementsByClassName("close")[0];
        closeButtonModal.onclick = function() {
            $(myModal).modal("hide")
        }

        window.onclick = function(event) {
            if (event.target === myModal) {
                $(myModal).modal("hide")
            }
        }

        let enableButton = document.getElementById('enableButton')
        enableButton.onclick = function() {
            enableFunction(documentId)
            $(myModal).modal("hide")
        }

        let disableButton = document.getElementById('disableButton')
        disableButton.onclick = function() {
            disableFunction(documentId)
            $(myModal).modal("hide")
        }
    }
    selectedUsers.length = 0
}

// Enable Function
function enableFunction(docId) {
    fetch(`http://localhost:4000/api/users/function/enable/${docId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(() => {
        alert('Successfully Enabled the User Account')
        window.location.reload();
    })
}

// Disable Function
function disableFunction(docId) {
    fetch(`http://localhost:4000/api/users/function/disable/${docId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(() => {
        alert('Successfully Disabled the User Account')
        window.location.reload();
    })
}

// Close Function
function closeFunction() {
	window.location.replace('./launchpad.html')	
}

fetch('http://localhost:4000/api/users/view/all/', {
    method: 'GET',
    headers: {
        'Content-Type' : 'application/json'
    }
}).then(res => res.json()).then(data => {
    salesQuote = data.map(result=> {
        if(result.addOn === 'Sales Quote') {
            return(
                `
                <tr style="text-align: center;">
                    <td style="width: 7rem"><input type="checkbox" class="checkBox" ></td>
                    <td style="width: 12rem"><a href='userViewing.html?documentId=${result.employeeId}'>${result.employeeId}</a></td>
                    <td style="width: 12rem">${result.firstName} ${result.lastName}</td>
                    <td style="width: 12rem">${result.position}</td>
                    <td style="width: 12rem">${result.userName}</td>
                    <td style="width: 7rem">${result.isActive}</td>
                </tr>
                `
            ) 
        }  
    }).join("");
    salesQuoteTable.innerHTML = salesQuote;             
})