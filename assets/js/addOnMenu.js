// Global Variables
let employeeId =localStorage.getItem("employeeId")
let token = localStorage.getItem("token");
let firstName = localStorage.getItem('firstName')
let lastName = localStorage.getItem('lastName')

// Logout Function
function logoutFunction() {
  localStorage.clear();
  window.location.replace('./logout.html')
}

// Sales Order Menu
let SalesOrderMenu = document.getElementById("SalesOrderMenu")

if( userData.isApprover === true) {
  
	SalesOrderMenu.innerHTML = 
	`
  <div>
    <Button class="iconBtn" onclick="salesOrderMonitoring()">
      <div class="btnHeader">Sales Order For Approval</div>
        <div class="btn-flex">  
          <img src="../assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
        </div>
        <div id="approverCounter" style="margin-left: 2rem ; margin-top: 1rem">0</div>
      </div>         
    </Button>
  </div>

  <div>
  <Button class="iconBtn" onclick="tpsTable()">
    <div class="btnHeader">Third Party Specifier Maintenance</div>
      <div class="btn-flex">  
        <img src="../assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
      </div>
      <div id="approverCounter" style="margin-left: 2rem ; margin-top: 1rem">0</div>
    </div>         
  </Button>
</div>

<div>
<Button class="iconBtn" onclick="moqTable()">
  <div class="btnHeader">Minimum Order Qunatity Maintenace</div>
    <div class="btn-flex">  
      <img src="../assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
    </div>
    <div id="approverCounter" style="margin-left: 2rem ; margin-top: 1rem">n/a</div>
  </div>         
</Button>
</div>

<div>
<Button class="iconBtn" onclick="priceTable()">
  <div class="btnHeader">Price List Maintenance</div>
    <div class="btn-flex">  
      <img src="../assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
    </div>
    <div id="approverCounter" style="margin-left: 2rem ; margin-top: 1rem"></div>
  </div>         
</Button>
</div>
  `
  
  let approverCounter = document.getElementById("approverCounter");
  fetch(`http://localhost:4000/api/salesOrder/status/all/forApproval`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(res => res.json()).then(data => {
    approverCounter.innerHTML = data.length
  })

} else if( userData.isRequestor == true) {

	SalesOrderMenu.innerHTML =
	`
  <div>
    <Button class="iconBtn" onclick="allSalesOrder()">
      <div class="btnHeader">All Sales Quotes</div>
        <div class="btn-flex">  
          <img src="../assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
        </div>
        <div id="counterOne" style="margin-left: 2rem ; margin-top: 1rem">0</div>
      </div>         
    </Button>
  </div>

  <div>
    <Button class="iconBtn" onclick="salesOrderMonitoring()">
      <div class="btnHeader">My Sales Quotes</div>
        <div class="btn-flex">  
          <img src="../assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
        </div>
        <div id="counterTwo" style="margin-left: 2rem ; margin-top: 1rem">0</div>
      </div>         
    </Button>
  </div>

  <div>
  <Button class="iconBtn" onclick="tpsTable()">
    <div class="btnHeader">Third Party Specifier Maintenance</div>
      <div class="btn-flex">  
        <img src="../assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
      </div>
      <div id="approverCounter" style="margin-left: 2rem ; margin-top: 1rem"></div>
    </div>         
  </Button>
</div>

<div>
<Button class="iconBtn" onclick="moqTable()">
  <div class="btnHeader">Minimum Order Qunatity Maintenace</div>
    <div class="btn-flex">  
      <img src="../assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
    </div>
    <div id="approverCounter" style="margin-left: 2rem ; margin-top: 1rem">n/a</div>
  </div>         
</Button>
</div>

<div>
<Button class="iconBtn" onclick="priceTable()">
  <div class="btnHeader">Price List Maintenance</div>
    <div class="btn-flex">  
      <img src="../assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
    </div>
    <div id="approverCounter" style="margin-left: 2rem ; margin-top: 1rem"></div>
  </div>         
</Button>
</div>
	`

  let counterOne = document.getElementById("counterOne");
  fetch(`http://localhost:4000/api/salesOrder/status/all`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
    counterOne.innerHTML = data.length
  })

  let counterTwo = document.getElementById("counterTwo");
  fetch(`http://localhost:4000/api/salesOrder/status/all/requestor/${employeeId}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
    counterTwo.innerHTML = data.length
  })

} else {
  SalesOrderMenu.innerHTML =
	`
  <div class="btnContainer">
    <Button class="iconBtn" onclick="allSalesOrder()">
      <div class="btnHeader">All Sales Quotes</div>
        <div class="btn-flex">  
          <img src="../assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
        </div>
        <div id="counterOne" style="margin-left: 2rem ; margin-top: 1rem">0</div>
      </div>         
    </Button>
  </div>

  <div class="btnContainer">
    <Button class="iconBtn" onclick="userMaintenance()">
      <div class="btnHeader">User Maintenance</div>
        <div class="btn-flex">  
          <img src="../assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
        </div>
        <div id="counterTwo" style="margin-left: 2rem ; margin-top: 1rem">0</div>
      </div>         
    </Button>
  </div>
  `

  let counterOne = document.getElementById("counterOne");
  fetch(`http://localhost:4000/api/salesOrder/status/all`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
    counterOne.innerHTML = data.length
  })

  let counterTwo = document.getElementById("counterTwo");
  fetch(`http://localhost:4000/api/users/view/all`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
    let soUsers = []
    for(let i = 0; i < data.length; i++) {
      if(data[i].addOn === 'Sales Quote') {
        soUsers.push(data[i])
      }
    }

    counterTwo.innerHTML = soUsers.length
  })
}

function salesOrderMonitoring() {
  window.location.replace("./salesOrderMonitoring.html")
}

function allSalesOrder() {
  window.location.replace("./allSalesOrder.html")
}

function userMaintenance() {
  window.location.replace("./userMonitoring.html")
}

const tpsTable = () => window.location.replace("/pages/thirdPartySpecifier.html");
const priceTable = () => window.location.replace("/pages/prcieList.html");
// const moqTable = () => window.location.replace("/pages/minimumOrderQuantity.html");