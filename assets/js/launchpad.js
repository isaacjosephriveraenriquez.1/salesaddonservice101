let addOnMenu = document.getElementById('addOnMenu');
let maintenaceMenu = document.getElementById('maintenanceMenu');

addOnMenu.innerHTML =   
`
<button type="button" class="menuButton" onclick="openSalesQuote()">
        <div class="addOnName">Sales Quote</div>
        <div class="counterDivContainer">
            <div class="iconContainer"></div>
            <div clas="counterContainer"></div>
        </div>
</button>
<button type="button" class="menuButton" onclick="">
        <div class="addOnName">Collection</div>
        <div class="counterDivContainer">
            <div class="iconContainer"></div>
            <div clas="counterContainer"></div>
        </div>
</button>
`

openSalesQuote = () => {
    window.location.replace('/pages/addOnMenu.html')
}

maintenanceMenu.innerHTML =   
`
<button type="button" class="menuButton" onclick="openUserMaintenance()">
        <div class="addOnName">User</div>
        <div class="counterDivContainer">
            <div class="iconContainer"></div>
            <div clas="counterContainer"></div>
        </div>
</button>
<button type="button" class="menuButton" onclick="">
        <div class="addOnName">Add- On</div>
        <div class="counterDivContainer">
            <div class="iconContainer"></div>
            <div clas="counterContainer"></div>
        </div>
</button>
<button type="button" class="menuButton" onclick="">
        <div class="addOnName">Price List</div>
        <div class="counterDivContainer">
            <div class="iconContainer"></div>
            <div clas="counterContainer"></div>
        </div>
</button>
<button type="button" class="menuButton" onclick="">
        <div class="addOnName">Third Party Specifier</div>
        <div class="counterDivContainer">
            <div class="iconContainer"></div>
            <div clas="counterContainer"></div>
        </div>
</button>
<button type="button" class="menuButton" onclick="">
        <div class="addOnName">Minimum Order Quantity</div>
        <div class="counterDivContainer">
            <div class="iconContainer"></div>
            <div clas="counterContainer"></div>
        </div>
</button>
`

let userMaintenance = document.getElementById("userMaintenance")
userMaintenance.innerHTML = `
<div class="modalHeader">
<h1>User Maintenance</h1>
</div>
<div class="menuButtonContainer">
<button>Add New User</button>
<button>Update User</button>
<button>Delete User</button>
<div class="InputContainer">
<div><label for="employeeId">Employee ID:</label>
    <input type="text" id="employeeId" placeholder="Employee Id">
    <label for="firstName">First Name:</label>
    <input type="text" id="firstName" placeholder="First Name">
    <label for="lastName">Last Name:</label>
    <input type="text" id="lastName" placeholder="Last Name">
    <label for="position">Position:</label>
    <input type="text" id="position" placeholder="Position">
</div>
<div>
    <label for="eMail">E-Mail:</label>
    <input  type="text" id="eMail" placeholder="E-Mail">
    <label for="password">Password:</label>
    <input type="text" id="password" placeholder="Password">
    <label for="isActive">is Active:</label>
    <input type="checkbox" id="isActive">
    <label for="admin">Administator:</label>
    <input type="checkbox" id="admin">
    <label for="bydBusinessUsername">BYD Username</label>
    <input type="text" id="bydBusinessUsername" placeholder="BYD Username">
    <label for="bydPassword">BYD Password</label>
    <input type="text" id="bydPassword" placeholder="BYD Password">
</div>
<label for="addOnList">Add-On:</label>
<select name="addOnList" id="addOnList">
    <option value="Sales Order">Sales Order</option>
    <option value="Purchase Order">Purchase Order</option>
    <option value="Collection">Collection</option>
  </select>
  <label for="roleList">Role:</label>
  <select name="roleList" id="roleList">
      <option value="Approver">Approver</option>
      <option value="Requestor">Requestor</option>
    </select>

    <table id="addOn">
        <thead>
            <tr>
                <th>Add-On</th>
                <th>Role</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <td></td>
            <td></td>
            <td></td>
        </tbody>
    </table>
    <div class="modalFooter">&nbsp;<button id="modalClose">Close</button></div></div> 
</div>
`
let closeModal = document.getElementById('modalClose');
closeModal.onclick = () => {
    document.getElementById('userMaintenance').style.display = "none"
}

let openUserMaintenance = () => {
    document.getElementById('userMaintenance').style.display = "block"
}
