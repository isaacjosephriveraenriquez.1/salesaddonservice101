let params = new URLSearchParams(window.location.search)
let fromWhat = params.get('from')

let salesOrderHeader = document.getElementById("salesOrderHeader");
salesOrderHeader.innerHTML = 
`<div class="detailsHeader">
<h3>Sales Quote Details</h3>
<button type"button" id="hideSalesQuoteDetails">Hide</button>
</div>

<div  id="salesQuoteDetails">

<label for="status">Status:</label>
<input type="text" class="" id="status" name="status" value="New" disabled>

<label for="createdOn" ">Created On:</label>
<input type="text" id="createdOn" name="createdOn" placeholder="Creation Date" disabled>

<label for="salesOrderId">Sales Quote ID:</label>
<input type="text" class="salesOrderId" id="salesOrderId" name="salesOrderId" placeholder="Add-On Sales Quote ID" disabled>
</div>

<div class="detailsHeader">
    <h3>Customer Details</h3>
    <button type"button" id="hideCustomerDetails">Hide</button>
</div>
<div  id="customerDetails">
    <label for="AccountName">Account Name:</label>
    <input type="text" list="accounts" class="accountName" id="accountName" placeholder="Account Name" autocomplete="off">
    <datalist id="accounts"></datalist>

    <label for="currentSpendingLimit">CSL:</label>
    <input type="text" id="currentSpendingLimit" name="currentSpendingLimit" placeholder="Current Spending Limit" disabled>


    <label for="address" >Address:</label>
    <input type="text" list="locations" class="address" id="address" name="address" placeholder="Customer Address" disabled>


    <label for="creditLimit">Credit Limit:</label>
    <input type="text" id="creditLimit" name="creditLimit" placeholder="Credit Limit" disabled>

    <label for="custInformation" ">Customer Information:</label>
    <textarea id="custInformation" name="custInformation"  placeholder="Type in Customer Information here"></textarea>
</div>


<div class="detailsHeader">
    <h3>Delivery Details</h3>
    <button type"button" id="hideDeliveryDetails">Hide</button>
</div>
    <div Id="deliveryDetails">    
        <label for="receivedDate" ">Received Date:</label> <!-- The date when the company received the order -->
        <input type="date"  id="receivedDate" name="receivedDate"  required>

        <label for="shipTo" ">Ship-To:</label>
        <input type="text" list="locations"  id="shipTo" name="shipTo" placeholder="Ship To Location" disabled autocomplete="off">
        <datalist id="locations"></datalist> 

        <label for="externalReference" ">External Reference:</label>
        <input type="text"  id="externalReference" name="externalReference" placeholder="Enter External Reference" autocomplete="off" required>
    
        <label for="requestedDate" ">Customer Requested Date:</label><!-- The date when the customer wants to receive the order -->
        <input type="date"  id="requestedDate" name="requestedDate" required>
    
        <label for="tps" ">TPS:</label>
        <select id="tps" name="tps" required>
            <option value="" selected>Select Third Party Specifier...</option>
        </select>
    
        <label for="deliveryDate" ">Order Delivery Date:</label>
        <input type="text"  id="deliveryDate" name="deliveryDate" placeholder="Order Delivery Date" disabled >
    
        <label for="customerGroup" ">Customer Group</label>
        <select  id="customerGroup" name="customerGroup">
            <option value="" selected></option>
        </select>
    
        <label for="deliveryPriority"  ">Delivery Priority:</label>
        <select  aria-placeholder="Default select " id="deliveryPriority">
            <option value=""></option>
            <option value="1" selected>Immediate</option>
            <option value="2">Urgent</option>
            <option value="3" >Normal</option>
            <option value="7">Low</option>
        </select>
    
        <label for="distributionChannel"  ">Distribution Channel:</label>
    
        <select  aria-placeholder="Default select example" id="distributionChannel" >
            <option value="" selected></option>
        </select>
    </div>

<div class="detailsHeader">
        <h3>Payment Details</h3>
        <button type"button" id="hidePaymentDetails">Hide</button>
    </div>
    <div id="paymentDetails">

        <label for="paymentTerms" ">Payment Terms:</label>
        <input type="text" list="terms"  id="paymentTerms" name="paymentTerms" placeholder="Payment Terms" autocomplete="off">
        <datalist id="terms"></datalist>
    
        <label for="currency" ">Currency:</label>
        <input type="text" list="currencies"  id="currency" name="currency" placeholder="Currency" autocomplete="off">
        <datalist id="currencies"></datalist>
        <label for="salesGroup" ">Sales Group:</label>
        <input type="text"  id="salesGroup" name="salesGroup" placeholder="Sales Group" disabled>
    
        <label for="salesOrigin"  ">Sales Origin:</label>
    
        <select  id="salesOrigin" name="salesOrigin" required>
            <option value="" selected>.....</option>
            <option value="Fax">Fax</option>
            <option value="Email">Email</option>
            <option value="Phone">Phone</option>
            <option value="By Salesman">By Salesman</option>
            <option value="Online">Online</option>
        </select>
    
        <label for="completeDelivery">Complete Delivery:</label>
        <input type="checkbox" id="completeDelivery" disabled>
    
        <label for="mbAccountId" ">MB Account ID:</label>
        <input type="text"  id="mbAccountId" name="mbAccountId" placeholder="MB Account ID" disabled>
    
                <label for="overTolerance" ">Over Tolerance:</label>
                <input type="text"  id="overTolerance" name="overTolerance" placeholder="Over Tolerance" disabled>
    
            
                <label for="underTolerance" ">Under Tolerance:</label>
                <input type="text"  id="underTolerance" name="underTolerance" placeholder="Under Tolerance" disabled>
    </div>

    <div class="detailsHeader">
        <h3>Miscellaneous Details</h3>
        <button type"button" id="hideMiscellaneousDetails">Hide</button>
    </div>
<div id="miscellaneousDetails">    

    <label for="comment" ">Internal Comment:</label>
    <textarea  id="comment" name="comment"" placeholder="Type in your Internal Comment here"></textarea>

            <label for="requestor" ">Employee Name:</label>
            <input type="text" id="requestor" name="requestor" placeholder="Employee Name" disabled>
    
            <label for="employeeId" ">Employee ID:</label>
            <input type="text" id="employeeId" name="employeeId" placeholder="Employee ID" disabled>
</div>
`
let salesQuoteDetails =  document.getElementById('salesQuoteDetails')
let customerDetails =  document.getElementById('customerDetails')
let deliveryDetails = document.getElementById('deliveryDetails')
let paymentDetails = document.getElementById('paymentDetails')
let miscellaneousDetails = document.getElementById('miscellaneousDetails')

let hideSalesQuoteDetails = document.getElementById('hideSalesQuoteDetails')
let hideCustomerDetails = document.getElementById('hideCustomerDetails')
let hideDeliveryDetails = document.getElementById('hideDeliveryDetails')
let hidePaymentDetails = document.getElementById('hidePaymentDetails')
let hideMiscellaneousDetails = document.getElementById('hideMiscellaneousDetails')

hideSalesQuoteDetails.onclick = () => {
    if (salesQuoteDetails.style.display === "block" ){
		hideSalesQuoteDetails.innerText = "Show";
		salesQuoteDetails.style.display = "none";
} else {
		salesQuoteDetails.style.display = "block";
		hideSalesQuoteDetails.innerText = "Hide";
	}
 }

hideCustomerDetails.onclick = () => {
    if (customerDetails.style.display === "block" ){
		hideCustomerDetails.innerText = "Show";
		customerDetails.style.display = "none";
} else {
		customerDetails.style.display = "block";
		hideCustomerDetails.innerText = "Hide";
	}
 }

 hideDeliveryDetails.onclick = () => {
    if (deliveryDetails.style.display === "block" ){
		hideDeliveryDetails.innerText = "Show";
		deliveryDetails.style.display = "none";
} else {
		deliveryDetails.style.display = "block";
		hideDeliveryDetails.innerText = "Hide";
	}
 }

 hidePaymentDetails.onclick = () => {
    if (paymentDetails.style.display === "block" ){
		hidePaymentDetails.innerText = "Show";
		paymentDetails.style.display = "none";
} else {
		paymentDetails.style.display = "block";
		hidePaymentDetails.innerText = "Hide";
	}
 }

 hideMiscellaneousDetails.onclick = () => {
    if (miscellaneousDetails.style.display === "block" ){
		hideMiscellaneousDetails.innerText = "Show";
		miscellaneousDetails.style.display = "none";

} else {
		miscellaneousDetails.style.display = "block";
		hideMiscellaneousDetails.innerText = "Hide";
	}
 }

 let toggleCustomerHeaderButton = document.getElementById("toggleCustomerHeader");

toggleCustomerHeaderButton.onclick = () => {
	if (salesOrderHeader.style.display === "block" ){
		salesOrderHeader.style.display = "none"
		toggleCustomerHeaderButton.innerText = "Show"
} else {
		salesOrderHeader.style.display = "block"
		toggleCustomerHeaderButton.innerText = "Hide"
	}
};

// Global Variables
let token = localStorage.getItem("token");
let accountNameId = '';
let shipToId = '';
let termsId = '';
let taxIdentifier = '';
let employeePosition = '';
let leadTime = '';
let totalPerLineContainer = '';
let materialId = ''
let isExternalReferenceUnique = false;
let selectedProducts = []

let accountNameContainer = document.getElementById('accountName')
let cslContainer = document.getElementById('currentSpendingLimit')
let addressContainer = document.getElementById('address')
let creditLimitContainer = document.getElementById('creditLimit')
let creationDateContainer = document.getElementById('createdOn')
let shipToContainer = document.getElementById("shipTo")
let salesQuoteNoContainer = document.getElementById('salesOrderId')
let paymentTermsContainer = document.getElementById("paymentTerms")
let currencyContainer = document.getElementById("currency")
let receivedDateContainer = document.getElementById('receivedDate')
let externalReferenceContainer = document.getElementById('externalReference')
let requestedDateContainer = document.getElementById('requestedDate')
let tpsContainer = document.getElementById('tps')
let deliveryDateContainer = document.getElementById('deliveryDate')
let customerGroupContainer = document.getElementById('customerGroup')
let deliveryPriorityContainer = document.getElementById('deliveryPriority')
let distributionChannelContainer = document.getElementById('distributionChannel')
let salesGroupContainer = document.getElementById('salesGroup')
let salesOriginContainer = document.getElementById('salesOrigin')
let completeDeliveryContainer = document.getElementById('completeDelivery')
let mbAccountIdContainer = document.getElementById('mbAccountId')
let customerInformationContainer = document.getElementById('custInformation')
let overToleranceContainer = document.getElementById('overTolerance')
let underToleranceContainer = document.getElementById('underTolerance')
let internalCommentContainer = document.getElementById('comment')
let employeeNameContainer = document.getElementById('requestor');
let employeeIdContainer = document.getElementById('employeeId');

let rapidEntryButton = document.getElementById('rapidButton')
let productCategoryContainer = document.getElementById('productCategory')
let configContainer = document.getElementById('configId')
let productIdContainer = document.getElementById('productId')
let productDescriptionContainer = document.getElementById("productDescription")
let stockQuantityContainer = document.getElementById("stockQuantity")
let leadTimeContainer = document.getElementById('leadTime')
let partNumberContainer = document.getElementById('partNumber')
let quantityContainer = document.getElementById('quantity')
let uomContainer = document.getElementById("uom")
let listPriceContainer = document.getElementById("listPrice")
let discountContainer = document.getElementById('discount')
let netPriceContainer = document.getElementById('netPrice')
let taxAmountContainer = document.getElementById('taxAmount')
let netValueContainer = document.getElementById('netValue')
let itemsArray = [];

let totalNetValueContainer = document.getElementById('totalNetValue')
let totalTaxAmountContainer = document.getElementById('totalTaxAmount')
let itemstotalAmountContainer = document.getElementById('itemstotalAmount')

let addItemsButton = document.getElementById('addItems')

let headerDataLength = 0
let itemDataLength = 0

// Buttons
let salesOrderMenu = document.getElementById("buttonRow")

if(userData.isRequestor === true) {
	salesOrderMenu.innerHTML =
	`
	<button id="saveAndViewButton" class="functionalButtons">Save and View</button>
	<button id="saveAndNewButton" class="functionalButtons">Save and New</button>
    <button id="uploadButton" class="functionalButtons">Upload File</button>
	<button id="closeButton" class="functionalButtons">Close</button>
	<button>View Exhange Rates</button>
	`

	document.getElementById('saveAndViewButton').onclick = function(e) {
		e.preventDefault()
		generateSoIdSaveAndView()
	}

	document.getElementById('saveAndNewButton').onclick = function(e) {
		e.preventDefault()
		generateSoIdSaveAndNew()
	}

    document.getElementById('uploadButton').onclick = function(e) {
		e.preventDefault()
		uploadFileModal()
	}

	document.getElementById("closeButton").onclick = function(e) {
		e.preventDefault()
		closeFunction()
	}
}

// Generate Sales Quotes ID on Save and View 
function generateSoIdSaveAndView() {
				fetch(`http://localhost:4000/api/salesOrder/status/all`, {
					method: 'GET',
					headers: {
						'Content-Type': 'application/json'
					}
				}).then(res => res.json()).then(data => {
					if(data.length > 0) {
                        let myArray = []
                        data.forEach(res => {
                            myArray.push(res.salesQuoteNo)
                        })
                        
                        let currentId = Math.max.apply(null, myArray) + 1
                        
                        salesQuoteNoContainer.value = currentId
                    } else {
                        salesQuoteNoContainer.value = 1
                    }
				}).then(() => {
					generateCreationDate()
				}).then(() => {
					saveAndViewFunction()
				})
			}


// Save And View Function
function saveAndViewFunction() {
	let accountNameValue = accountNameContainer.value
	let addressValue = addressContainer.value
    let creditLimitValue = creditLimitContainer.value
	let creationDateValue = creationDateContainer.value
	let shipToValue = shipToContainer.value
	let salesQuoteNoValue = salesQuoteNoContainer.value
	let paymentTermsValue = paymentTermsContainer.value
	let currencyValue = currencyContainer.value
	let receivedDateValue = receivedDateContainer.value
	let externalReferenceValue = externalReferenceContainer.value
	let requestedDateValue = requestedDateContainer.value
	let tpsValue = tpsContainer.value
	let deliveryDateValue = deliveryDateContainer.value
	let customerGroup = customerGroupContainer.options[customerGroupContainer.selectedIndex].text
	let customerGroupCode = customerGroupContainer.value
	let deliveryPriority = deliveryPriorityContainer.options[deliveryPriorityContainer.selectedIndex].text
	let deliveryPriorityCode = deliveryPriorityContainer.value
	let distributionChannel = distributionChannelContainer.options[distributionChannelContainer.selectedIndex].text
	let distributionChannelCode = distributionChannelContainer.value
	let salesGroupValue = salesGroupContainer.value
	let salesOriginValue = salesOriginContainer.value
	let mbAccountIdValue = mbAccountIdContainer.value
	let customerInformationValue = customerInformationContainer.value
	let overToleranceValue = overToleranceContainer.value
	let underToleranceValue = underToleranceContainer.value
	let internalCommentValue = internalCommentContainer.value
	let employeeNameValue = employeeNameContainer.value
	let employeeIdValue = employeeIdContainer.value
    let totalSalesQuoteAmountValue = itemstotalAmountContainer.value
	let completeDeliveryValue = false;

	if(completeDeliveryContainer.checked) {
		completeDeliveryValue = true
	}

    let splitReceivedDate = receivedDateValue.split('-')
	let splitRequestedDate = requestedDateValue.split('-')
	
    let updatedReceivedDate = `${splitReceivedDate[1]}/${splitReceivedDate[2]}/${splitReceivedDate[0]}`
	let updatedRequestedDate = `${splitRequestedDate[1]}/${splitRequestedDate[2]}/${splitRequestedDate[0]}`
	
	if (accountNameContainer.value === ''){
		accountNameContainer.setAttribute("style", "border-color:red;")
	} else {
		accountNameContainer.setAttribute("style", "border-color:#ced4da;")
	}
	
	if (tpsContainer.value === ""){
		tpsContainer.setAttribute("style", "border-color:red;")
	} else {
		tpsContainer.setAttribute("style", "border-color:#ced4da;")
	}
	
	if (salesOriginContainer.value === ''){
		salesOriginContainer.setAttribute("style","border-color:red")
	} else {
		salesOriginContainer.setAttribute("style", "border-color:#ced4da;")
	}
	
	if (receivedDateContainer.value === ''){
		receivedDateContainer.setAttribute("style", "border-color:red;")
	} else {
		receivedDateContainer.setAttribute("style", "border-color:#ced4da;")
	} 
	if (requestedDateContainer.value === ''){
		requestedDateContainer.setAttribute("style", "border-color:red;")
    } else {
		requestedDateContainer.setAttribute("style", "border-color:#ced4da;")
	}

	if (externalReferenceContainer.value === ""){
		externalReferenceContainer.setAttribute("style", "border-color:red;")
	} else {
		externalReferenceContainer.setAttribute("style", "border-color: #ced4da;")
	}

	if (accountNameContainer.value === "" || tpsContainer.value === "" || salesOriginContainer.value === "" || receivedDateContainer === "" || requestedDateContainer.value === "" || externalReferenceContainer.value === "" ){
		alert("Please complete all the required fields!")
	} else {
	fetch('http://localhost:4000/api/salesOrder/add', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			accountId: accountNameId,
			accountName: accountNameValue,
			docStatus: 'In Preparation',
			address: addressValue,
            creditLimit: creditLimitValue,
            creationDate: creationDateValue,
			shipToPartyId: shipToId,
			shipToPartyDescription: shipToValue,
			salesQuoteNo: salesQuoteNoValue,
			paymentTermsId: termsId,
			paymentTerms: paymentTermsValue,
            currency: currencyValue,
            receivedDate: updatedReceivedDate,
			externalReference: externalReferenceValue, 
			requestedDate: updatedRequestedDate,
			tps: tpsValue,
            deliveryDate: deliveryDateValue,
			customerGroupCode: customerGroupCode,
			customerGroup: customerGroup,
            deliveryPriorityCode: deliveryPriorityCode, 
			deliveryPriority: deliveryPriority, 
            distributionChannelCode: distributionChannelCode,
			distributionChannel: distributionChannel,
			salesGroup: salesGroupValue,
            salesOrigin: salesOriginValue,
            completeDelivery: completeDeliveryValue,
            mbAccountId: mbAccountIdValue,
            customerInformation: customerInformationValue,
            overTolerance: overToleranceValue,
			underTolerance: underToleranceValue,
			internalComment: internalCommentValue,
			employeeName: employeeNameValue,
			employeeId: employeeIdValue,
            totalSalesQuoteAmount: totalSalesQuoteAmountValue,
			items: itemsArray
		})
	}).then(() => {
		alert('Successfully Created the Sales Quotes ')
		window.location.replace(`./salesOrderViewing.html?documentId=${salesQuoteNoValue}`)
	})
}
}

// Generate Sales Quotes ID on Save and New
function generateSoIdSaveAndNew() {
				fetch(`http://localhost:4000/api/salesOrder/status/all`, {
					method: 'GET',
					headers: {
						'Content-Type': 'application/json'
					}
				}).then(res => res.json()).then(data => {
					if(data.length > 0) {
                        let myArray = []
                        data.forEach(res => {
                            myArray.push(res.salesQuoteNo)
                        })
                        
                        let currentId = Math.max.apply(null, myArray) + 1
                        
                        salesQuoteNoContainer.value = currentId
                    } else {
                        salesQuoteNoContainer.value = 1
                    }
				}).then(() => {
					generateCreationDate()
				}).then(() => {
					saveAndNewFunction()
				})
			}

// Save and New Function
function saveAndNewFunction() {
    let accountNameValue = accountNameContainer.value
	let addressValue = addressContainer.value
    let creditLimitValue = creditLimitContainer.value
	let creationDateValue = creationDateContainer.value
	let shipToValue = shipToContainer.value
	let salesQuoteNoValue = salesQuoteNoContainer.value
	let paymentTermsValue = paymentTermsContainer.value
	let currencyValue = currencyContainer.value
	let receivedDateValue = receivedDateContainer.value
	let externalReferenceValue = externalReferenceContainer.value
	let requestedDateValue = requestedDateContainer.value
	let tpsValue = tpsContainer.value
	let deliveryDateValue = deliveryDateContainer.value
	let customerGroup = customerGroupContainer.options[customerGroupContainer.selectedIndex].text
	let customerGroupCode = customerGroupContainer.value
	let deliveryPriority = deliveryPriorityContainer.options[deliveryPriorityContainer.selectedIndex].text
	let deliveryPriorityCode = deliveryPriorityContainer.value
	let distributionChannel = distributionChannelContainer.options[distributionChannelContainer.selectedIndex].text
	let distributionChannelCode = distributionChannelContainer.value
	let salesGroupValue = salesGroupContainer.value
	let salesOriginValue = salesOriginContainer.value
	let mbAccountIdValue = mbAccountIdContainer.value
	let customerInformationValue = customerInformationContainer.value
	let overToleranceValue = overToleranceContainer.value
	let underToleranceValue = underToleranceContainer.value
	let internalCommentValue = internalCommentContainer.value
	let employeeNameValue = employeeNameContainer.value
	let employeeIdValue = employeeIdContainer.value
    let totalSalesQuoteAmountValue = itemstotalAmountContainer.value
	let completeDeliveryValue = false;

	if(completeDeliveryContainer.checked) {
		completeDeliveryValue = true
	}

    let splitReceivedDate = receivedDateValue.split('-')
	let splitRequestedDate = requestedDateValue.split('-')
	
    let updatedReceivedDate = `${splitReceivedDate[1]}-${splitReceivedDate[2]}-${splitReceivedDate[0]}`
	let updatedRequestedDate = `${splitRequestedDate[1]}-${splitRequestedDate[2]}-${splitRequestedDate[0]}`
	
	if (accountNameContainer.value === ''){
		accountNameContainer.setAttribute("style", "border-color:red;")
	} else {
		accountNameContainer.setAttribute("style", "border-color:#ced4da;")
	}
	
	if (tpsContainer.value === ""){
		tpsContainer.setAttribute("style", "border-color:red;")
	} else {
		tpsContainer.setAttribute("style", "border-color:#ced4da;")
	}
	
	if (salesOriginContainer.value === ''){
		salesOriginContainer.setAttribute("style","border-color:red")
	} else {
		salesOriginContainer.setAttribute("style", "border-color:#ced4da;")
	}
	
	if (receivedDateContainer.value === ''){
		receivedDateContainer.setAttribute("style", "border-color:red;")
	} else {
		receivedDateContainer.setAttribute("style", "border-color:#ced4da;")
	} 
	if (requestedDateContainer.value === ''){
		requestedDateContainer.setAttribute("style", "border-color:red;")
    } else {
		requestedDateContainer.setAttribute("style", "border-color:#ced4da;")
	}

	if (externalReferenceContainer.value === ""){
		externalReferenceContainer.setAttribute("style", "border-color:red;")
	} else {
		externalReferenceContainer.setAttribute("style", "border-color: #ced4da;")
	}

	if (accountNameContainer.value === "" || tpsContainer.value === "" || salesOriginContainer.value === "" || receivedDateContainer === "" || requestedDateContainer.value === "" || externalReferenceContainer.value === "" ){
		alert("Please complete all the required fields!")
	} else {
	fetch('http://localhost:4000/api/salesOrder/add', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			accountId: accountNameId,
			accountName: accountNameValue,
			docStatus: 'In Preparation',
			address: addressValue,
            creditLimit: creditLimitValue,
            creationDate: creationDateValue,
			shipToPartyId: shipToId,
			shipToPartyDescription: shipToValue,
			salesQuoteNo: salesQuoteNoValue,
			paymentTermsId: termsId,
			paymentTerms: paymentTermsValue,
            currency: currencyValue,
            receivedDate: updatedReceivedDate,
			externalReference: externalReferenceValue, 
			requestedDate: updatedRequestedDate,
			tps: tpsValue,
            deliveryDate: deliveryDateValue,
			customerGroupCode: customerGroupCode,
			customerGroup: customerGroup,
            deliveryPriorityCode: deliveryPriorityCode, 
			deliveryPriority: deliveryPriority, 
            distributionChannelCode: distributionChannelCode,
			distributionChannel: distributionChannel,
			salesGroup: salesGroupValue,
            salesOrigin: salesOriginValue,
            completeDelivery: completeDeliveryValue,
            mbAccountId: mbAccountIdValue,
            customerInformation: customerInformationValue,
            overTolerance: overToleranceValue,
			underTolerance: underToleranceValue,
			internalComment: internalCommentValue,
			employeeName: employeeNameValue,
			employeeId: employeeIdValue,
            totalSalesQuoteAmount: totalSalesQuoteAmountValue,
			items: itemsArray
		})
	}).then(() => {
		alert('Successfully Created the Sales Quotes ')
		window.location.reload()
	})
}}

// Upload Modal Function
let fileInput = document.getElementById('fileInput')
let fileName = document.getElementById('fileName')
let chooseFileButton = document.getElementById('chooseFileButton')
let selectedFile = null

function uploadFileModal() {
    let myModal = document.getElementById("uploadFileModal");
    $(myModal).modal("show")

    let closeButtonModal = document.getElementById('uploadFileModalCloseButton');
    closeButtonModal.onclick = function() {
        $(myModal).modal("hide")
    }

    window.onclick = function(event) {
        if (event.target === myModal) {
            $(myModal).modal("hide")
        }
    }

    chooseFileButton.onclick = function() {
        fileInput.click()
    }

    fileInput.onchange = function() {
        if(fileInput.value) {
            selectedFile = fileInput.files[0]
            fileName.innerHTML = fileInput.value.match(/[\/\\]([\w\d\s\.\-\(\)]+)$/)[1]
        } else {
            fileName.innerHTML = `No File Chosen`
        }
    }

    let uploadFileButton = document.getElementById('uploadFileButton')
    uploadFileButton.onclick = function() {
        uploadFunction()
        $(myModal).modal("hide")
    }
}

// Upload Function
function uploadFunction() {
    if(selectedFile) {
        document.getElementById('header').setAttribute('class', 'blur')
        document.getElementById('salesOrderCreationForm').setAttribute('class', 'blur')

        $("#myimgdivshowhide").show()

        let reader = new FileReader()
        reader.readAsBinaryString(selectedFile)
        reader.onload = (e) => {
            let data = e.target.result
            let workbook = XLSX.read(data, {type: "binary"})
            
            const headerSheet = workbook.Sheets['Entries']
            const headerData = XLSX.utils.sheet_to_json(headerSheet, { raw: false })
            headerDataLength = headerData.length

            for(let i = 0; i < headerData.length; i++) {
                if(i === 0 || headerData[i]['Entry'] !== headerData[i - 1]['Entry']) {
                    fetch(`http://localhost:4000/api/customer/view/specific/byAccountId/${headerData[i]['Account ID']}`, {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(res => {
                        return res.json()
                    }).then(result => {
                        fetch(`http://localhost:4000/api/header/upload`, {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                entryNo: headerData[i]['Entry'],
                                accountId: result.accountId,
                                accountName: result.accountName,
                                docStatus: "In Preparation",
                                address: result.address,
                                creditLimit: result.creditLimit,
                                creationDate: headerData[i]['Creation Date'],
                                shipToPartyId: result.shipToPartyId,
                                shipToPartyDescription: result.shipToPartyDescription,
                                paymentTermsId: result.paymentTermsId,
                                paymentTerms: result.paymentTerms,
                                currency: result.currency,
                                receivedDate: headerData[i]['Received Date'],
                                externalReference: headerData[i]['Reference'],
                                requestedDate: headerData[i]['Requested Date'],
                                tps: headerData[i]['TPS'],
                                deliveryDate: headerData[i]['Delivery Date'],
                                customerGroupCode: result.customerGroup,
                                customerGroup: result.customerGroupDesc,
                                deliveryPriorityCode: `${headerData[i]['Priority'] === "Immediate" ? "1" : (headerData[i]['Priority'] === "Urgent" ? "2" : (headerData[i]['Priority'] == "Normal" ? "3" : "7"))}`,
                                deliveryPriority: headerData[i]['Priority'],
                                distributionChannelCode: `${result.distributionChannel.length > 1 ? "02": (result.distributionChannel[0] == "Indirect" ? "02" : "01")}`,
                                distributionChannel: `${result.distributionChannel.length > 1 ? "Indirect Sales" : result.distributionChannel[0]}`,
                                salesGroup: result.salesGroup,
                                salesOrigin: headerData[i]['Origin'],
                                completeDelivery: result.exactDispatch,
                                mbAccountId: result.mbAccountId,
                                customerInformation: headerData[i]['Customer Information'],
                                overTolerance: result.overTolerance,
                                underTolerance: result.underTolerance,
                                internalComment: headerData[i]['Comment'],
                                employeeName: `${firstName} ${lastName}`,
                                employeeId: `${empID}`
                            })
                        })
                    }).then(() => {
                        fetch(`http://localhost:4000/api/products/view/specific/${headerData[i]['Product ID']}`, {
                            method: 'GET',
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        }).then(res => {
                            return res.json()
                        }).then(result => {
                            fetch(`http://localhost:4000/api/line/upload`, {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json'
                                },
                                body: JSON.stringify({
                                    entryNo: headerData[i]['Entry'],
                                    productCategoryId: result.productCategoryId,
                                    configId: result.configId,
                                    productId: result.materialId,
                                    productDescription: result.materialDescription,
                                    leadTime: result.leadTime,
                                    partNumber: result.partNumber,
                                    quantity: headerData[i]['Quantity'],
                                    unitOfMeasure: result.uomDescription,
                                    listPrice: result.price,
                                    discount: headerData[i]['Discount'],
                                    netPrice: (parseFloat(result.price) * ((100 - parseFloat(headerData[i]['Discount']))/100)).toFixed(2),
                                    netValue: (parseFloat(result.price) * ((100 - parseFloat(headerData[i]['Discount']))/100) * parseFloat(headerData[i]['Quantity'])).toFixed(2),
                                    taxAmount: (parseFloat(result.price) * ((100 - parseFloat(headerData[i]['Discount']))/100) * parseFloat(headerData[i]['Quantity']) * 0.12).toFixed(2),
                                    totalPerLine: ((parseFloat(result.price) * ((100 - parseFloat(headerData[i]['Discount']))/100) * parseFloat(headerData[i]['Quantity'])) + (parseFloat(result.price) * ((100 - parseFloat(headerData[i]['Discount']))/100) * parseFloat(headerData[i]['Quantity']) * 0.12)).toFixed(2)
                                })
                            })
                        })
                    })
                } else {
                    fetch(`http://localhost:4000/api/products/view/specific/${headerData[i]['Product ID']}`, {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(res => {
                        return res.json()
                    }).then(result => {
                        fetch(`http://localhost:4000/api/line/upload`, {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                entryNo: headerData[i]['Entry'],
                                productCategoryId: result.productCategoryId,
                                configId: result.configId,
                                productId: result.materialId,
                                productDescription: result.materialDescription,
                                leadTime: result.leadTime,
                                partNumber: result.partNumber,
                                quantity: headerData[i]['Quantity'],
                                unitOfMeasure: result.uomDescription,
                                listPrice: result.price,
                                discount: headerData[i]['Discount'],
                                netPrice: (parseFloat(result.price) * ((100 - parseFloat(headerData[i]['Discount']))/100)).toFixed(2),
                                netValue: (parseFloat(result.price) * ((100 - parseFloat(headerData[i]['Discount']))/100) * parseFloat(headerData[i]['Quantity'])).toFixed(2),
                                taxAmount: (parseFloat(result.price) * ((100 - parseFloat(headerData[i]['Discount']))/100) * parseFloat(headerData[i]['Quantity']) * 0.12).toFixed(2),
                                totalPerLine: ((parseFloat(result.price) * ((100 - parseFloat(headerData[i]['Discount']))/100) * parseFloat(headerData[i]['Quantity'])) + (parseFloat(result.price) * ((100 - parseFloat(headerData[i]['Discount']))/100) * parseFloat(headerData[i]['Quantity']) * 0.12)).toFixed(2)
                            })
                        })
                    })
                }
            }
        }

        setTimeout(() => {
            checkDatabaseVersusExcelLength()
        },5000)
    } else {
        alert('No File Selected')
    }
}

function checkDatabaseVersusExcelLength() {
    fetch(`http://localhost:4000/api/line/all`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => {
        return res.json()
    }).then(result => {
        if(result.length == headerDataLength) {
            fetch(`http://localhost:4000/api/salesOrder/createSalesQuoteFromUpload`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(() => {
                $("#myimgdivshowhide").hide()
                window.location.replace('./salesOrderMonitoring.html')
            })
        } else {
            checkDatabaseVersusExcelLength()
        }
    })
}

// Close Function
function closeFunction(){
	if(typeof(fromWhat) === 'string') {
		let confirm = window.confirm ("All unsaved data will be deleted. Do you want to close?");
		if (confirm === true){
			window.location.replace('./allSalesOrder.html')
		}	
	} else {
		let confirm = window.confirm ("All unsaved data will be deleted. Do you want to close?");
		if (confirm === true){
			window.location.replace('./salesOrderMonitoring.html')
		}			
	}
}

// Populate Fields - Employee Name and Employee ID
fetch('http://localhost:4000/api/users/details', {
method: 'GET',
headers: {
	'Content-Type': 'application/json',
	Authorization: `Bearer ${token}`
}
}).then(res => res.json()).then(result => {
	employeePosition = result.position
	employeeNameContainer.value = `${result.firstName} ${result.lastName}`;
	employeeIdContainer.value = result.employeeId;
})

//Look Up - Account Name
let accountsDatalist = document.getElementById("accounts")
let accounts;

fetch('http://localhost:4000/api/customer/view/all', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
}).then(res => res.json()).then(data => {
	accounts = data.map(result => {
		return  (
			`
			<option value="${result.accountName}">
			${result.accountId} - ${result.searchName}
			</option>
			`
		)
	})
	accountsDatalist.innerHTML = accounts;
}) 

// Clear Populated Fields if Account Name is Changed
function clearCustomerFields() {
	accountNameId = ''
	addressContainer.value = ''
    creditLimitContainer.value = ''
	shipToId = ''
	shipToContainer.value = ''
	termsId = ''
	paymentTermsContainer.value = ''
	currencyContainer.value = ''
	customerGroupContainer.options[customerGroupContainer.selectedIndex].text = ''
	salesGroupContainer.value = ''
	mbAccountIdContainer.value = ''
	overToleranceContainer.value = ''
	underToleranceContainer.value = ''
	taxIdentifier = ''
	distributionChannelContainer.options[distributionChannelContainer.selectedIndex].text = ''
	completeDeliveryContainer.checked = false
}

//Populate Fields - Address, Credit Limit, Ship To, Payment Terms, Currency, Customer Group, Sales Group, MB Account ID, Over Tolerance, Under Tolerance, Tax Identifier
function populateCustomerFields() {
	fetch(`http://localhost:4000/api/customer/view/specific/${accountNameContainer.value}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		accountNameId = data.accountId
		addressContainer.value = data.address
        creditLimitContainer.value = data.creditLimit.toLocaleString('en-US', { minimumFractionDigits: 2 })
		shipToId = data.shipToPartyId
		shipToContainer.value = data.shipToPartyDescription
		termsId = data.paymentTermsId
		paymentTermsContainer.value = data.paymentTerms
		currencyContainer.value = data.currency
		customerGroupContainer.value = data.customerGroup
		customerGroupContainer.options[customerGroupContainer.selectedIndex].text = data.customerGroupDesc
		salesGroupContainer.value = data.salesGroup
		mbAccountIdContainer.value = data.mbAccountId
		overToleranceContainer.value = data.overTolerance
		underToleranceContainer.value = data.underTolerance
		taxIdentifier = data.taxStatus

		if(data.distributionChannel.length > 1) {
			distributionChannelContainer.value = '02'
			distributionChannelContainer.options[distributionChannelContainer.selectedIndex].text = 'Indirect Sales'
		} else {
			distributionChannelContainer.value = '02'
			distributionChannelContainer.options[distributionChannelContainer.selectedIndex].text = 'Indirect Sales'
		}

		if(data.exactDispatch === true) {
			completeDeliveryContainer.checked = true
		} else {
			completeDeliveryContainer.checked = false
		}

	}).then(() => {
		fetchBydDetails()
	})
}

// Retrieve Current Spending Limit from BYD
function fetchBydDetails() {
	fetch(`http://localhost:4000/getCurrentSpendingLimit/${accountNameId}`, {
		method: 'POST'
	}).then(res => res.json()).then(data => {
        csl = data['AccountOpenAmounts']['0']['CurrentSpendingLimitAmount']['0']['_']
		updateCSL(csl);
	})
}

//Update Current Spending Limit - CSL from BYD minus In Preparation and For Approval on SO Add-On
function updateCSL(csl){
	fetch(`http://localhost:4000/api/salesOrder/allSalesOrderinDatabase/${accountNameId}`,{
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(result => {
		let totalValueForSpecificCustomer = 0

        for(a = 0; a < result.length; a++) {
            let totalAmountPerItem = 0
            totalAmountPerItem = parseFloat(result[a].taxAmount) + parseFloat(result[a].netValue)
            totalValueForSpecificCustomer += totalAmountPerItem
        }

		cslContainer.value = (parseFloat(csl) - parseFloat(totalValueForSpecificCustomer)).toLocaleString('en-US', { minimumFractionDigits: 2 })
	})
}

// Look Up - Ship To Location Customer
// function shipToLocationCustomerLookUp() {
// 	let locationDatalist = document.getElementById('locations')
// 	let location;

// 	fetch('http://localhost:4000/api/shipToLocationCustomer/view/all', {
// 		method: 'GET',
// 		headers: {
// 			'Content-Type': 'application/json'
// 		}
// 	}).then(res => res.json()).then(data => {
// 		location = data.map(result => {
// 			return  (
// 				`
// 				<option value="${result.shipToPartyDescription}">
// 				${result.shipToPartyId}
// 				</option>
// 				`
// 			)
// 		})
// 		locationDatalist.innerHTML = location;
// 	}) 
// }

// Look Up - Payment Terms
function paymentTermsLookUp() {
	let termsDatalist = document.getElementById('terms')
	let terms;

	fetch('http://localhost:4000/api/terms/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		terms = data.map(result => {
			return  (
				`
				<option value="${result.paymentTermsDescription}">
				${result.paymentTermsId}
				</option>
				`
			)
		})
		termsDatalist.innerHTML = terms;
	}) 
}

//Look Up - Currency
function currencyLookUp() {
	let currencyDatalist = document.getElementById('currencies')
	let curr;
	fetch('http://localhost:4000/api/currency/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		curr = data.map(result => {
			return  (
				curr =
				`
				<option value="${result.currencyTicker.toUpperCase()}">
				${result.currencyName}
				</option>
				`
			)
		})
		currencyDatalist.innerHTML = curr;
	}) 
}

accountNameContainer.oninput = function() {
	clearCustomerFields()
	populateCustomerFields()
	// shipToLocationCustomerLookUp()
	paymentTermsLookUp()
	currencyLookUp()
	productCategoryContainer.removeAttribute('readonly')
	addItemsButton.removeAttribute('disabled')
};

// Look Up - TPS
let tps;
fetch('http://localhost:4000/api/tps/query', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
}).then(res => res.json()).then(data => {
	tps = data.map(result => {
		return  (
			`
			<option value="${result.code} - ${result.vendorAccount}">${result.code} - ${result.vendorAccount}</option>
			`
		)
	})
	tpsContainer.innerHTML += tps;      
})

//Look Up - Customer Group
let grp;
fetch('http://localhost:4000/api/customerGroup/view/all', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
}).then(res => res.json()).then(data => {
	grp = data.map(result => {
		return  (
			`
			<option value="${result.code}">${result.description}</option>
			`
		)
	})
	customerGroupContainer.innerHTML += grp;      
})

//Look Up - Distribution Channel
let chnl;
fetch('http://localhost:4000/api/distributionChannel/view/all', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
}).then(res => res.json()).then(data => {
	chnl = data.map(result => {
		return  (
			`
			<option value="${result.code}">${result.description}</option>
			`
		)
	})
	distributionChannelContainer.innerHTML += chnl;      
})

//Generating Created On Date
function generateCreationDate() {
	let month = new Date().toLocaleString('default', { month: 'long' })

	if(month === 'January'){
		month = '01'
	} else if(month === 'February') {
		month = '02'
	} else if(month === 'March') {
		month = '03'
	} else if(month === 'April') {
		month = '04'
	} else if(month === 'May') {
		month = '05'
	} else if(month === 'June') {
		month = '06'
	} else if(month === 'July') {
		month = '07'
	} else if(month === 'August') {
		month = '08'
	} else if(month === 'September') {
		month = '09'
	} else if(month === 'October') {
		month = '10'
	} else if(month === 'November') {
		month = '11'
	} else if(month === 'December') {
		month = '12'
	}

	let day = new Date().getDate();
	let year = new Date().getFullYear();

	creationDateContainer.value = `${month}/${day}/${year}`
}

// Look Up - Product Categories
let productDatalist = document.getElementById('productCategories')
let product;
fetch('http://localhost:4000/api/productCategory/view/all', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
}).then(res => res.json()).then(data => {
	data.sort()
    product = data.map(result => {
        return  (
            `
            <option value="${result.categoryId}"></option>
            `
        )
    })
    productDatalist.innerHTML = product;
})

// To clear Config ID Datalist
function clearConfigIdDatalist() {
	let configDatalist = document.getElementById('configIds')
	configDatalist.innerHTML = ''
	configContainer.value = ''
}

// Look Up - Materials with Product Category indicated
function configIdLookUp() {
	let configDatalist = document.getElementById('configIds')
	let configs;
	fetch(`http://localhost:4000/api/products/view/byProductCategory/${productCategoryContainer.value}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		data.sort()
		configs = data.map(result => {
			return  (
				`
				<option value="${result.configId}">${result.materialDescription}</option>
				`
			)
		})
		configDatalist.innerHTML = configs;
	})
}

productCategoryContainer.oninput = function() {
	configContainer.removeAttribute('readonly')
	
	configIdLookUp()
};

function clearLineItemFromProductCategory() {
    productIdContainer.value = ""
    productDescriptionContainer.value = ""
    stockQuantityContainer.value = ""
    leadTimeContainer.value = ""
    partNumberContainer.value = ""
    quantityContainer.value = ""
    uomContainer.value = ""
    listPriceContainer.value = ""
    discountContainer.value = ""
    netPriceContainer.value = ""
    taxAmountContainer.value = ""
    netValueContainer.value = ""
}

productCategoryContainer.onkeydown = function(e) {
    if(e.code == "Backspace") {
        clearLineItemFromProductCategory()
        clearConfigIdDatalist()
    }
}

// To Concatenate Product Category ID and Config ID
function concatenateIds() {
    if(configContainer.value.length > 4) {
        productIdContainer.value = `${productCategoryContainer.value}-${configId.value}`
    } else {
        productIdContainer.value = ""
    }
    getStockCounts()
}

// To Retrieve Stock Quantity of the Item
function getStockCounts() {
    let materialId = productIdContainer.value
    
    fetch(`http://localhost:4000/getCurrentStockQuantity/${materialId}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => {
        return res.json()
    }).then(result => {
        stockQuantityContainer.value = result.toLocaleString('en-US', { minimumFractionDigits: 0 })
    })
}

//Populate Fields - Product Description, Lead Time, Part Number, Stocks, MOQ, UoM, List Price, Net Price
function populateProductFields() {
	fetch(`http://localhost:4000/api/products/view/specific/${productIdContainer.value}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		productDescriptionContainer.value = data.materialDescription
		leadTimeContainer.value = data.leadTime
		partNumberContainer.value = data.partNumber
		uomContainer.value = data.uomDescription
		listPriceContainer.value = data.price
		netPriceContainer.value = data.price

		if(leadTime === '') {
			leadTime = data.leadTime
		} else if(leadTime < parseInt(data.leadTime)) {
			leadTime = data.leadTime
		}
	})
}

// Look-Up and Validation of Minimum Order Quantity
function enableFields() {
	uomContainer.removeAttribute('readonly')
	quantityContainer.removeAttribute('readonly')
	discountContainer.removeAttribute('readonly')
	partNumberContainer.removeAttribute('readonly')

	if(employeePosition === 'Customer Service Supervisor') {
		listPriceContainer.removeAttribute('readonly')
	}
}

configContainer.oninput = function() {
    concatenateIds()
    populateProductFields()
    enableFields()
}

function clearLineItemFromConfig() {
    productIdContainer.value = ""
    productDescriptionContainer.value = ""
    stockQuantityContainer.value = ""
    leadTimeContainer.value = ""
    partNumberContainer.value = ""
    quantityContainer.value = ""
    uomContainer.value = ""
    listPriceContainer.value = ""
    discountContainer.value = ""
    netPriceContainer.value = ""
    taxAmountContainer.value = ""
    netValueContainer.value = ""
}

configContainer.onkeydown = function(e) {
    if(e.code == "Backspace") {
        clearLineItemFromConfig()
    }
}

// Generating Net Value
function generateNetValue() {
	let quantityValue = document.getElementById('quantity').value
	let netValueContainer = document.getElementById('netValue')
	let listPriceValue = document.getElementById('listPrice').value

	netValueContainer.value = quantityValue * listPriceValue
};

quantityContainer.onchange = function() {
	fetch(`http://localhost:4000/api/moq/view/specific?productCategoryId=${productCategoryContainer.value}&minimumOrder=${quantityContainer.value}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		if (confirm("The Minimum Order Quantity for this item is " + `${data.minimumOrder}` +" Would you like to change your order to "+ `${data.minimumOrder}` +"?") == true){
			quantityContainer.value = data.minimumOrder
			generateNetValue()
		} else {
			generateNetValue()
		}
	})
};


// Retrieving Discount Amount to Change Net Price
function applyDiscount(discountValue) {
	let netPriceValue = netPriceContainer.value
	let listPriceValue = listPriceContainer.value
	netPriceValue = listPriceValue;
	discountValue = discountValue/100
	netPriceValue = (netPriceValue * (1 - discountValue)).toFixed(2)

	changeNetPrice(netPriceValue)
}

// Changing the Net Price based on Discount Field
function changeNetPrice(netPriceValue) {
	let netPriceContainer = document.getElementById("netPrice")
	let netValueContainer = document.getElementById("netValue")
	let quantityValue = document.getElementById("quantity").value

	netPriceContainer.value = netPriceValue
	netValueContainer.value = (parseFloat(quantityValue) * parseFloat(netPriceContainer.value)).toFixed(2)

	calculateTaxAmount();
}

// Calculating Tax
function calculateTaxAmount() {
	let netValueValue = document.getElementById("netValue").value
	let taxAmountContainer = document.getElementById("taxAmount")

	if(taxIdentifier === 'Vatable') {
		taxAmountContainer.value = (netValueValue * 0.12).toFixed(2)
	} else {
		taxAmountContainer.value = netValueValue * 0
	}
	
	itemsPush();
	itemsDisplay();
	clearFunction();
}


//Array for Products/Items
function itemsPush() {	
	perLineTotal = (parseFloat(taxAmountContainer.value) + parseFloat(netValueContainer.value)).toFixed(2)
	itemsArray.push({
		productCategoryId: productCategoryContainer.value,
		configId: configContainer.value,
        productId: productIdContainer.value,
		productDescription: productDescriptionContainer.value,
        stock: stockQuantityContainer.value,
		leadTime: leadTimeContainer.value,
		partNumber: partNumberContainer.value,
		quantity: quantityContainer.value,
		unitOfMeasure: uomContainer.value,
		listPrice: listPriceContainer.value,
		discount: discountContainer.value,
		netPrice: netPriceContainer.value,
		netValue: netValueContainer.value,
        taxAmount: taxAmountContainer.value,
		totalPerLine: perLineTotal
	})
}

// Add Row
let productTableContainer = document.getElementById("productTableContainer");
let items;

function itemsDisplay() {
	let a = 0;
	items = itemsArray.map(itemsList => {
        return(
			a++,
            `
            <tr>
			<td class="text-center"><div> ${a} </div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${itemsList.productCategoryId}" disabled></div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${itemsList.configId}" disabled></div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${itemsList.productId}" disabled></div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${itemsList.productDescription}" disabled></div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${itemsList.stock}" disabled></div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${itemsList.leadTime}" disabled></div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${itemsList.partNumber}" disabled></div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${itemsList.moq}" disabled></div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${parseFloat(itemsList.quantity).toLocaleString('en-US', { minimumFractionDigits: 0 })}" disabled></div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${itemsList.unitOfMeasure}" disabled></div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${parseFloat(itemsList.listPrice).toLocaleString('en-US', { minimumFractionDigits: 2 })}" disabled></div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${itemsList.discount}" disabled></div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${parseFloat(itemsList.netPrice).toLocaleString('en-US', { minimumFractionDigits: 2 })}" disabled></div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${parseFloat(itemsList.netValue).toLocaleString('en-US', { minimumFractionDigits: 2 })}" disabled></div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${parseFloat(itemsList.taxAmount).toLocaleString('en-US', { minimumFractionDigits: 2 })}" disabled></div></td>
			<td><div><input type="text" style="width: 100%; text-align: center;" value="${parseFloat(itemsList.totalPerLine).toLocaleString('en-US', { minimumFractionDigits: 2 })}" disabled></div></td>
			<td>
				<div class="d-flex justify-content-center">
					<button class="crudButton button btn ms-1" type="button" onclick="remove_tr(this)">
						<i class="far fa-trash-alt"></i>
					</button>
				</div>
				</td>
            </tr>
            `
        )
	}).join("");
	productTableContainer.innerHTML = items
}

// To Clear Look Ups
function clearFunction() {
	productCategoryContainer.value = ''
	configContainer.value = ''
    productIdContainer.value = ''
	productDescriptionContainer.value = ''
    stockQuantityContainer.value = ''
	leadTimeContainer.value = ''
	partNumberContainer.value = ''
	quantityContainer.value = ''
	uomContainer.value = ''
	listPriceContainer.value = ''
	discountContainer.value = ''
	netPriceContainer.value = ''
	taxAmountContainer.value = ''
	netValueContainer.value = ''
}

function disableFields() {
	configContainer.setAttribute('readonly', true)
	uomContainer.setAttribute('readonly', true)
	quantityContainer.setAttribute('readonly', true)
	discountContainer.setAttribute('readonly', true)
	uomContainer.setAttribute('readonly', true)
	listPriceContainer.setAttribute('readonly', true)
	partNumberContainer.setAttribute('readonly', true)
}

// Generate Order Delivery Date
function generateOrderDeliveryDate() {
	let deliveryDate = document.getElementById('deliveryDate')
	let today = new Date()
	let newDate = new Date()
	newDate.setDate(today.getDate() + parseInt(leadTime))

	newMonth = newDate.toLocaleString('default', {month: 'long'})

	if(newMonth === 'January'){
		newMonth = '01'
	} else if(newMonth === 'February') {
		newMonth = '02'
	} else if(newMonth === 'March') {
		newMonth = '03'
	} else if(newMonth === 'April') {
		newMonth = '04'
	} else if(newMonth === 'May') {
		newMonth = '05'
	} else if(newMonth === 'June') {
		newMonth = '06'
	} else if(newMonth === 'July') {
		newMonth = '07'
	} else if(newMonth === 'August') {
		newMonth = '08'
	} else if(newMonth === 'September') {
		newMonth = '09'
	} else if(newMonth === 'October') {
		newMonth = '10'
	} else if(newMonth === 'November') {
		newMonth = '11'
	} else if(newMonth === 'December') {
		newMonth = '12'
	}

	newDay = newDate.getDate()
	
	if(newDay.toString().length === 1) {
		newDay = `0${newDay}`
	}
	
	newYear = newDate.getFullYear()
	
	deliveryDate.value = `${newMonth}/${newDay}/${newYear}`
}

// Calculating Total Net Value of Sales Quote
function calculateTotalNetValue() {
	let sumOfAllNetValue = 0
	itemsArray.forEach(item => {
		sumOfAllNetValue += parseFloat(item.netValue)
	})
	totalNetValueContainer.value = sumOfAllNetValue.toLocaleString('en-US', { minimumFractionDigits: 2 })
}

// Calculating Total Tax Amount of Sales Quote
function calculateTotalTaxAmount() {
	let sumOfAllTaxAmount = 0
	itemsArray.forEach(item => {
		sumOfAllTaxAmount += parseFloat(item.taxAmount)
	})
	totalTaxAmountContainer.value = sumOfAllTaxAmount.toLocaleString('en-US', { minimumFractionDigits: 2 })
}

// Calculating Total Gross Amount of Sales Quote
function calculateTotalAmount() {
	let sumOfAllItemsTotalAmount = 0
	itemsArray.forEach(item => {
		sumOfAllItemsTotalAmount += parseFloat(item.taxAmount) + parseFloat(item.netValue)
	})
	itemstotalAmountContainer.value = sumOfAllItemsTotalAmount.toLocaleString('en-US', { minimumFractionDigits: 2 })
}

document.getElementById("addItems").onclick = function(e) {
	e.preventDefault();
		let discountValue = discountContainer.value
		applyDiscount(discountValue);
		disableFields();
		generateOrderDeliveryDate();
		calculateTotalNetValue();
		calculateTotalTaxAmount();
		calculateTotalAmount();

};

// Accept Function
function acceptFunction() {
	let discountValue = discountContainer.value
	applyDiscount(discountValue);
	disableFields();
	generateOrderDeliveryDate();
	calculateTotalNetValue();
	calculateTotalTaxAmount();
	calculateTotalAmount();
}

// Reject Function
function rejectFunction() {
	let discountValue = discountContainer.value
	applyDiscount(discountValue);
	disableFields();
	generateOrderDeliveryDate();
	calculateTotalNetValue();
	calculateTotalTaxAmount();
	calculateTotalAmount();
}

// Remove Functions
function remove_tr(This) {
	itemsArray.splice(This.closest('tr').rowIndex - 1, 1);
	This.closest('tr').remove();

	calculateTotalNetValue();
	calculateTotalTaxAmount();
	calculateTotalAmount();
}

// Rapid Entry
rapidEntryButton.onclick = function(e) {
    e.preventDefault()

    let myModal = document.getElementById("rapidEntryModal");
    $(myModal).modal("show")

    let closeButtonModal = document.getElementById('rapidEntryModalCloseButton');
    closeButtonModal.onclick = function() {
        $(myModal).modal("hide")
    }

    window.onclick = function(event) {
        if (event.target === myModal) {
            $(myModal).modal("hide")
        }
    }

    let categoryIdModalContainer = document.getElementById('categoryIdModal')
    let configIdTable = document.getElementById('configIdTable')

    // Look Up - Product Categories
    let productDatalistModal = document.getElementById('productCategoriesModal')
    let mats
    fetch('http://localhost:4000/api/productCategory/view/all', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json()).then(data => {
        data.sort()
        mats = data.map(result => {
            return  (
                `
                <option value="${result.categoryId}"></option>
                `
            )
        })
        productDatalistModal.innerHTML = mats;
    })

    categoryIdModalContainer.onchange = async function() {
        let configs
        fetch(`http://localhost:4000/api/products/rapidEntry/${categoryIdModalContainer.value}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            return res.json()
        }).then(data => {
            configs = data.map(result => {
                return (
                    `
                    <tr>
                        <td class="modalTableHeader text-center">${result.product}</td>
                        <td class="modalTableHeader text-center">${result.stock}</td>
                        <td class="modalTableHeader text-center">
                            <input type="number" style="width: 8rem;" class="text-center my-2" />
                        </td>
                        <td class="modalTableHeader text-center">
                            <input type="checkbox" class="mt-2" />
                        </td>
                    </tr>
                    `
                )
            }).join("")
        
            configIdTable.innerHTML += configs
        })
    }

    let addItemsModal = document.getElementById('addItemsModal')
    addItemsModal.onclick = function() {
        getValuesOfCheckedBox()
    }

    // Select a Checkbox
    function getValuesOfCheckedBox() {
        //Reference the Table.
        let grid = document.getElementById("configIdTable");

        //Reference the CheckBoxes in Table.
        let checkBoxes = grid.getElementsByTagName("INPUT");

        //Loop through the CheckBoxes.
        for (let i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].checked) {
                let row = checkBoxes[i].parentNode.parentNode
                let data = {
                    productCategoryId: categoryIdModalContainer.value,
                    productId: row.cells[0].innerHTML,
                    quantity: row.cells[2].children[0].value
                }
                selectedProducts.push(data)
            }
        }
        $(myModal).modal("hide")
        categoryIdModalContainer.value = ""
        while(grid.hasChildNodes()) {
            grid.removeChild(grid.firstChild)
        }
        pushRapidEntry()
    }
}

function pushRapidEntry() {
    console.log(selectedProducts);
}
function getAllMaterial(){
	fetch('http://localhost:4000/api/salesOrder/queryMaterials', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
}).then(res => res.json()).then(data => {
	 data.map(result => {
		return  (
			result
		)
	})    
})
}

//external reference
externalReferenceContainer.onchange = function() {
	
	let first = accountName.value
	let second = externalReferenceContainer.value
	console.log(first)
	console.log(second)

		fetch(`http://localhost:4000/api/salesOrder/checkExternalReference/accountName/${first}/externalReference/${second}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			console.log(data)
				if (data == false){
					externalReferenceContainer.setAttribute("style", "border-color:red;")
					confirm("External Reference Already used by this Customer. Please used another code")

					if(confirm("External Reference Already used by this Customer. Please used another code") == true ){
						externalReferenceContainer.value = ""
						console.log(externalReferenceContainer)
					} 
				} else {
					externalReferenceContainer.setAttribute("style", "border-color:#ced4da;")
				}
			})
	}

