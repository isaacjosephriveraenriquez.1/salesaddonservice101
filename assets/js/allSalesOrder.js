// Global Variables
let token = localStorage.getItem("token") 
let employeeId =localStorage.getItem("employeeId")
let isApprover = localStorage.getItem("isApprover")
let firstName = localStorage.getItem('firstName')
let lastName = localStorage.getItem('lastName')
let selectedSalesOrders = []

// Welcome Banner
let userLoggedIn = document.getElementById('userLoggedIn')
userLoggedIn.innerHTML = `${firstName} ${lastName}`

//Logout Function 
function logoutFunction() {
    localStorage.clear();
    window.location.replace('./logout.html')
}

document.getElementById('logoutButton').onclick = function() {
    logoutFunction();
}

// Filter Status
let filterStatusContainer = document.getElementById("filterStatus");

filterStatus.innerHTML = 
`
<option value="allStatus">All Status</option>
<option value="inPreparationStatus">In Preparation</option>
<option value="forApprovalStatus">For Approval</option>
<option value="postedStatus">Posted</option>
<option value="approvedStatus">Approved</option>
<option value="cancelledStatus">Cancelled</option>
<option value="rejectedStatus">Rejected</option>
<option value="postingErrorStatus">Posting Error</option>
`

// Change of Filter by Status
function statusFilterCallback() {
    let statusFilterValue = document.getElementById('filterStatus').value

    if(statusFilterValue === 'allStatus') {
        fetch(`http://localhost:4000/api/salesOrder/status/all`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Sales Quotes Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}&from="allSalesOrder"'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div>${result.externalReference}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div>${totalAmountPerSo.toLocaleString('en-US', {minimumFractionDigits: 2})}</div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("")
                salesOrderTable.innerHTML = salesOrder;              
            }
        })
    } else if(statusFilterValue === 'inPreparationStatus') {
        fetch(`http://localhost:4000/api/salesOrder/status/all/inPreparation`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No In Preparation Sales Quotes Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}&from="allSalesOrder"'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div>${result.externalReference}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div>${totalAmountPerSo.toLocaleString('en-US', {minimumFractionDigits: 2})}</div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'forApprovalStatus') {
        fetch(`http://localhost:4000/api/salesOrder/status/all/forApproval`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No For Approval Sales Quotes Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}&from="allSalesOrder"'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div>${result.externalReference}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div>${totalAmountPerSo.toLocaleString('en-US', {minimumFractionDigits: 2})}</div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'postedStatus') {
        fetch(`http://localhost:4000/api/salesOrder/status/all/posted`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Posted Sales Quotes Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}&from="allSalesOrder"'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div>${result.externalReference}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div>${totalAmountPerSo.toLocaleString('en-US', {minimumFractionDigits: 2})}</div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'approvedStatus') {
        fetch(`http://localhost:4000/api/salesOrder/status/all/approved`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Posted Sales Quotes Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}&from="allSalesOrder"'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div>${result.externalReference}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div>${totalAmountPerSo.toLocaleString('en-US', {minimumFractionDigits: 2})}</div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'rejectedStatus') {
        fetch(`http://localhost:4000/api/salesOrder/status/all/rejected`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Rejected Sales Quotes Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}&from="allSalesOrder"'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div>${result.externalReference}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div>${totalAmountPerSo.toLocaleString('en-US', {minimumFractionDigits: 2})}</div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'cancelledStatus') {
        fetch(`http://localhost:4000/api/salesOrder/status/all/cancelled`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Cancelled Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}&from="allSalesOrder"'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div>${result.externalReference}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div>${totalAmountPerSo.toLocaleString('en-US', {minimumFractionDigits: 2})}</div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'postingErrorStatus') {
        fetch(`http://localhost:4000/api/salesOrder/status/all/postingError`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Posting Error Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}&from="allSalesOrder"'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div>${result.externalReference}</div></td>
                        <td style="text-align: center; width: 4rem;"><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div>${totalAmountPerSo.toLocaleString('en-US', {minimumFractionDigits: 2})}</div></td>
                        <td style="text-align: center; width: 7rem;"><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    }
}

document.getElementById('filterStatus').onchange = function() {
	statusFilterCallback()
};

// Buttons
let buttonContainerList = document.getElementById("buttonRow")

if(localStorage.getItem("isRequestor") === "true") {
	buttonContainerList.innerHTML =
	`
    <button id="newButton" class="functionalButtons">New</button>
    <button id="viewButton" class="functionalButtons">View</button>
    <button id="postButton" class="functionalButtons">Post</button>
    <button id="cancelButton" class="functionalButtons">Cancel</button>
    <button id="closeButton" class="functionalButtons">Close</button>
	`

    document.getElementById("newButton").onclick = function(e) {
        e.preventDefault()
        newFunction()
    }

    document.getElementById("viewButton").onclick = function(e) {
        e.preventDefault()
        getValuesOfCheckedBox()

        if(selectedSalesOrders.length < 1) {
            alert('Please Select a Sales Order')
        } else if(selectedSalesOrders.length > 1) {
            selectedSalesOrders.length = 0
            alert('Action cannot be performed on multiple line items. Please select a single line item')
        } else {
            viewFunction()
        }
    }

    document.getElementById("postButton").onclick = function(e) {
        e.preventDefault()
        getValuesOfCheckedBox()
        postFunction()
    }

    document.getElementById('cancelButton').onclick = function(e){
		e.preventDefault()
		getValuesOfCheckedBox()

        if(selectedSalesOrders.length < 1) {
            alert('Please select a Sales Quote')
        } else if(selectedSalesOrders.length > 1) {
            selectedSalesOrders.length = 0
            alert('Action cannot be performed on multiple line items. Please select a single line item')
        } else {
            if(selectedSalesOrders[0].docStatus !== 'In Preparation') {
                alert(`The selected Sales Quote is already ${selectedSalesOrders[0].docStatus}`)
                selectedSalesOrders.length = 0
            } else {
                cancelModalFunction()
            }
        }
	}

    document.getElementById("closeButton").onclick = function(e) {
        e.preventDefault()
        closeFunction()
    }
}

if(localStorage.getItem("isRequestor") === "false" && localStorage.getItem("isApprover") === "false") {
    buttonContainerList.innerHTML =
	`
    <button id="viewButton">View</button>
    <button id="closeButton">Close</button>
	`

    document.getElementById("viewButton").onclick = function(e) {
        e.preventDefault()
        getValuesOfCheckedBox()

        if(selectedSalesOrders.length < 1) {
            alert('Please Select a Sales Order')
        } else if(selectedSalesOrders.length > 1) {
            selectedSalesOrders.length = 0
            alert('Action cannot be performed on multiple line items. Please select a single line item')
        } else {
            viewFunction()
        }
    }

    document.getElementById("closeButton").onclick = function(e) {
        e.preventDefault()
        closeFunction()
    }
}

// New Function
function newFunction(){
    location.replace("./salesOrderCreation.html?from=allSalesOrder")
}
        
// View Function
function viewFunction() {
    let docId = selectedSalesOrders[0].salesQuoteNo
    window.location.replace(`./salesOrderViewing.html?documentId=${docId}&from=allSalesOrder`)
}

// Cancel Modal Function
function cancelModalFunction() {
    let documentId = selectedSalesOrders[0].salesQuoteNo

    let myModal = document.getElementById("cancelModal");
    $(myModal).modal("show")

    let closeButtonModal = document.getElementById('closeCancelModalButton')
    closeButtonModal.onclick = function() {
        $(myModal).modal("hide")
    }

    window.onclick = function(event) {
        if (event.target === myModal) {
            $(myModal).modal("hide")
        }
    }

    let cancelButton = document.getElementById('cancelModalButton')
    cancelButton.onclick = function() {
        let explanation = document.getElementById('explanation').value
        cancelFunction(documentId, explanation)
        $(myModal).modal("hide")
    }
}

// Cancel Function
function cancelFunction(documentId, explanation) {
    fetch(`http://localhost:4000/api/salesOrder/function/cancel/${documentId}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            cancelReason: explanation
        })
    }).then(() => {
        selectedSalesOrders.length = 0
        alert('Successfully Cancelled the Sales Quote')
        window.location.reload();
    })
}

// Close Function
function closeFunction(){
	window.location.replace('./launchpad.html')
}

// Select All Checkbox
function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}

// Select a Checkbox
function getValuesOfCheckedBox() {
    //Reference the Table.
    let grid = document.getElementById("salesOrderTable");

    //Reference the CheckBoxes in Table.
    let checkBoxes = grid.getElementsByTagName("INPUT");

    //Loop through the CheckBoxes.
    for (let i = 0; i < checkBoxes.length; i++) {
        if (checkBoxes[i].checked) {
            let row = checkBoxes[i].parentNode.parentNode
            let data = {
                salesOrderId: row.cells[1].innerHTML,
                salesQuoteNo: row.cells[2].innerText,
                docStatus: row.cells[3].innerHTML,
                accountId: row.cells[4].innerHTML,
                accountName:  row.cells[5].innerHTML,
                salesGroup: row.cells[6].innerHTML,
                customerGroup: row.cells[7].innerHTML,
                externalReference: row.cells[8].innerHTML,
                totalAmount:  row.cells[9].innerHTML,
                creationDate: row.cells[10].innerHTML,
                createdBy:  row.cells[11].innerHTML,
                validator:  row.cells[12].innerHTML,
                validationDate: row.cells[13].innerHTML,
                deliveryBlockReason: row.cells[14].innerHTML,
                cancelReason: row.cells[15].innerHTML
            }
            selectedSalesOrders.push(data)
        }
    }
}

// Post Function
function postFunction() {
    let withIssue = []
    let withoutIssue = []


    for(let i = 0; i < selectedSalesOrders.length; i++) {
        if(selectedSalesOrders[i].docStatus !== "In Preparation") {
            continue
        } else {
            fetch(`http://localhost:4000/getCurrentSpendingLimit/${selectedSalesOrders[i].accountId}`, {
                method: 'POST'
            }).then(response => {
                return response.json()
            }).then(result => {
                let csl = parseFloat(result['AccountOpenAmounts']['0']['CurrentSpendingLimitAmount']['0']['_'])

                fetch(`http://localhost:4000/api/salesOrder/allSalesOrderinDatabase/${selectedSalesOrders[i].accountId}`,{
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(response => {
                    return response.json()
                }).then(result => {
                    let totalValueForSpecificCustomer = 0

                    for(a = 0; a < result.length; a++) {
                        let totalAmountPerItem = 0
                        totalAmountPerItem = parseFloat(result[a].taxAmount) + parseFloat(result[a].netValue)
                        totalValueForSpecificCustomer += totalAmountPerItem
                    }

                    let updatedCSL = csl - totalValueForSpecificCustomer

                    return updatedCSL.toFixed(2)
                }).then((updatedCSL) => {
                    if(updatedCSL < 0) {
                        withIssue.push(selectedSalesOrders[i])

                        fetch(`http://localhost:4000/api/salesOrder/function/setDeliveryBlock/${selectedSalesOrders[i].salesQuoteNo}`, {
                            method: 'PUT',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                deliveryBlockReasonCode: '01',
                                deliveryBlockReason: 'Credit Limit',
                                docStatus: 'For Approval'
                            })
                        })
                    } else if(parseFloat(selectedSalesOrders[i].totalAmount) > updatedCSL) {
                        withIssue.push(selectedSalesOrders[i])

                        fetch(`http://localhost:4000/api/salesOrder/function/setDeliveryBlock/${selectedSalesOrders[i].salesQuoteNo}`, {
                            method: 'PUT',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                deliveryBlockReasonCode: '01',
                                deliveryBlockReason: 'Credit Limit',
                                docStatus: 'For Approval'
                            })
                        })
                    } else {
                        withoutIssue.push(selectedSalesOrders[i])

                        let validatorValue = `${firstName} ${lastName}`
                        fetch(`http://localhost:4000/api/salesOrder/function/post/${selectedSalesOrders[i].salesQuoteNo}`, {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                validator: validatorValue
                            })
                            
                        })
                    }
                })
            })
        }
    } 
}

// Populate Sales Order Table
function fetchSalesOrder() {
    fetch(`http://localhost:4000/api/salesOrder/status/all`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json()).then(data => {
        if (data.length < 1){
            salesOrder = `<h1 style="text-align: center;">No Sales Quotes Available</h1>`
            salesOrderTable.innerHTML = salesOrder;
        } else {
            data.reverse()
            salesOrder = data.map(result=> {
                let totalAmountPerSo = 0
                result.items.forEach(item => {
                    let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                    totalAmountPerSo += amountPerLine
                })
                return(
                    `
                    <tr>
                    <td style="width: 2rem"><div><input type="checkbox" id="flexCheckDefault"></div></td>
                    <td style="text-align: center; width: 5rem;"><div>${result.salesOrderNo}</div></td>
                    <td style="text-align: center; width: 5rem;"><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}&from="allSalesOrder"'>${result.salesQuoteNo}</a></div></td>
                    <td style="text-align: center; width: 7rem;"><div>${result.docStatus}</div></td>
                    <td style="text-align: center; width: 5rem;"><div>${result.accountId}</div></td>
                    <td style="text-align: center; width: 24rem;"><div>${result.accountName}</div></td>
                    <td style="text-align: center; width: 4rem;"><div>${result.salesGroup}</div></td>
                    <td style="text-align: center; width: 6.5rem;"><div>${result.customerGroup}</div></td>
                    <td style="text-align: center; width: 10rem;"><div>${result.externalReference}</div></td>
                    <td style="text-align: center; width: 4rem;"><div>${result.currency}</div></td>
                    <td style="text-align: center; width: 7.5rem;"><div>${totalAmountPerSo.toLocaleString('en-US', {minimumFractionDigits: 2})}</div></td>
                    <td style="text-align: center; width: 7rem;"><div>${result.creationDate}</div></td>
                    <td style="text-align: center; width: 8rem;"><div>${result.employeeName}</div></td>
                    <td style="text-align: center; width: 13rem;"><div>${result.validator}</div></td>
                    <td style="text-align: center; width: 12rem;"><div>${result.validationDate}</div></td>
                    <td style="text-align: center; width: 8rem;"><div>${result.deliveryBlockReason}</div></td>
                    <td style="text-align: center; width: 9rem;"><div>${result.cancelReason}</div></td>
                    </tr>
                    `
                )
            }).join("")
            salesOrderTable.innerHTML = salesOrder;                       
        }
    })
}

fetchSalesOrder()