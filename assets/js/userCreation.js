// Global Variables
let empFirstName = localStorage.getItem('firstName')
let empLastName = localStorage.getItem('lastName')

let employeeIdContainer = document.getElementById('employeeId')
let accountTypeContainer = document.getElementById('accountType')
let firstNameContainer = document.getElementById('firstName')
let lastNameContainer = document.getElementById('lastName')
let usernameContainer = document.getElementById('username')
let passwordContainer = document.getElementById('password')
let positionContainer = document.getElementById('position')
let addOnContainer = document.getElementById('addOn')

if(localStorage.getItem('token') === null) {
	window.location.replace('../index.html')
}

// Welcome Banner
let userLoggedIn = document.getElementById('userLoggedIn')
userLoggedIn.innerHTML = `${empFirstName} ${empLastName}`

// Logout Function
function logoutFunction() {
	localStorage.clear();
	window.location.replace('./logout.html')
}

document.getElementById("logoutButton").onclick = function() {
	logoutFunction();
}

// Buttons
let saveAndViewButton = document.getElementById('saveAndViewButton')
saveAndViewButton.onclick = function(e) {
    e.preventDefault();
    saveAndViewFunction()
}

let saveAndNewButton = document.getElementById('saveAndNewButton')
saveAndNewButton.onclick = function(e) {
    e.preventDefault();
    saveAndNewFunction()
}

let closeButton = document.getElementById('closeButton')
closeButton.onclick = function(e) {
    e.preventDefault();
    closeFunction()
}

// Save and View Function
function saveAndViewFunction() {
    let employeeIdValue = employeeIdContainer.value
    let accountTypeValue = accountTypeContainer.value
    let firstNameValue = firstNameContainer.value
    let lastNameValue = lastNameContainer.value
    let usernameValue = usernameContainer.value
    let passwordValue = passwordContainer.value
    let positionValue = positionContainer.value
    let addOnValue = addOn.value
    let isRequestor, isApprover, isAdmin

    if(accountTypeValue === 'Requestor') {
        isRequestor = true
        isApprover = false
        isAdmin = false
    } else if(accountTypeValue === 'Approver') {
        isRequestor = false
        isApprover = true
        isAdmin = false
    } else {
        isRequestor = false
        isApprover = false
        isAdmin = true
    }

    fetch('http://localhost:4000/api/users/register', {
        method: 'POST',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
            employeeId: employeeIdValue,
            firstName: firstNameValue,
            lastName: lastNameValue,
            userName: usernameValue.toUpperCase(),
            password: passwordValue,
            isRequestor: isRequestor,
            isApprover: isApprover,
            isAdmin: isAdmin,
            position: positionValue,
            addOn: addOnValue
        })
    }).then(res => res.json()).then(() => {
        alert('Successfully Created the User Account')
        window.location.replace(`./userViewing.html?documentId=${employeeIdValue}`)
    })
}

// Save and New Function
function saveAndNewFunction() {
    let employeeIdValue = employeeIdContainer.value
    let accountTypeValue = accountTypeContainer.value
    let firstNameValue = firstNameContainer.value
    let lastNameValue = lastNameContainer.value
    let usernameValue = usernameContainer.value
    let passwordValue = passwordContainer.value
    let positionValue = positionContainer.value
    let addOnValue = addOn.value
    let isRequestor, isApprover, isAdmin

    if(accountTypeValue === 'Requestor') {
        isRequestor = true
        isApprover = false
        isAdmin = false
    } else if(accountTypeValue === 'Approver') {
        isRequestor = false
        isApprover = true
        isAdmin = false
    } else {
        isRequestor = false
        isApprover = false
        isAdmin = true
    }

    fetch('http://localhost:4000/api/users/register', {
        method: 'POST',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
            employeeId: employeeIdValue,
            firstName: firstNameValue,
            lastName: lastNameValue,
            userName: usernameValue.toUpperCase(),
            password: passwordValue,
            isRequestor: isRequestor,
            isApprover: isApprover,
            isAdmin: isAdmin,
            position: positionValue,
            addOn: addOnValue
        })
    }).then(res => res.json()).then(() => {
        alert('Successfully Created the User Account')
        window.location.reload()
    })
}

// Close Function
function closeFunction() {
    window.location.replace('./userMonitoring.html')
}