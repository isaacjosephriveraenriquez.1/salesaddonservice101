// Retrieving Employee ID
let params = new URLSearchParams(window.location.search)
let docId = params.get('documentId')

// Global Variables
let empFirstName = localStorage.getItem('firstName')
let empLastName = localStorage.getItem('lastName')

let employeeIdContainer = document.getElementById('employeeId')
let accountTypeContainer = document.getElementById('accountType')
let firstNameContainer = document.getElementById('firstName')
let lastNameContainer = document.getElementById('lastName')
let usernameContainer = document.getElementById('username')
let passwordContainer = document.getElementById('password')
let positionContainer = document.getElementById('position')
let addOnContainer = document.getElementById('addOn')

let initialAccountTypeOption = document.getElementById('initialAccountTypeOption')
let initialPositionOption = document.getElementById('initialPositionOption')

if(localStorage.getItem('token') === null) {
	window.location.replace('../index.html')
}

// Welcome Banner
let userLoggedIn = document.getElementById('userLoggedIn')
userLoggedIn.innerHTML = `${empFirstName} ${empLastName}`

// Logout Function
function logoutFunction() {
	localStorage.clear();
	window.location.replace('./logout.html')
}

document.getElementById("logoutButton").onclick = function() {
	logoutFunction();
}

// Buttons
let newButton = document.getElementById('newButton')
newButton.onclick = function(e) {
    e.preventDefault();
    newFunction()
}

let saveButton = document.getElementById('saveButton')
saveButton.onclick = function(e) {
    e.preventDefault();
    saveFunction()
}

let editButton = document.getElementById('editButton')
editButton.onclick = function(e) {
    e.preventDefault();
    editFunction()
}

let closeButton = document.getElementById('closeButton')
closeButton.onclick = function(e) {
    e.preventDefault();
    closeFunction()
}

// New Function
function newFunction() {
    window.location.replace('./userCreation.html')
}

// Save Function
function saveFunction() {
    employeeIdContainer.setAttribute('disabled', true)
    accountTypeContainer.setAttribute('disabled', true)
    firstNameContainer.setAttribute('disabled', true)
    lastNameContainer.setAttribute('disabled', true)
    usernameContainer .setAttribute('disabled', true)
    positionContainer.setAttribute('disabled', true)

    let employeeIdValue = employeeIdContainer.value
    let accountTypeValue = accountTypeContainer.value
    let firstNameValue = firstNameContainer.value
    let lastNameValue = lastNameContainer.value
    let usernameValue = usernameContainer.value
    let positionValue = positionContainer.value
    let isRequestor, isApprover, isAdmin

    if(accountTypeValue === 'Requestor') {
        isRequestor = true
        isApprover = false
        isAdmin = false
    } else if(accountTypeValue === 'Approver') {
        isRequestor = false
        isApprover = true
        isAdmin = false
    } else {
        isRequestor = false
        isApprover = false
        isAdmin = true
    }

    

    fetch(`http://localhost:4000/api/users/function/edit/${docId}`, {
        method: 'PUT',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
            employeeId: employeeIdValue,
            isRequestor: isRequestor,
            isApprover: isApprover,
            isAdmin: isAdmin,
            firstName: firstNameValue,
            lastName: lastNameValue,
            userName: usernameValue.toUpperCase(),
            position: positionValue
        })
    }).then(res => res.json()).then(() => {
        alert('Successfully Updated the User Account')
        window.location.reload()
    })
}

// Edit Function
function editFunction() {
    employeeIdContainer.removeAttribute('disabled')
    accountTypeContainer.removeAttribute('disabled')
    firstNameContainer.removeAttribute('disabled')
    lastNameContainer.removeAttribute('disabled')
    usernameContainer.removeAttribute('disabled')
    positionContainer.removeAttribute('disabled')
}

// Close Function
function closeFunction() {
    window.location.replace('./userMonitoring.html')
}

// Populate Fields
fetch(`http://localhost:4000/api/users/view/specific/${docId}`, {
    method: 'GET',
    headers: {
        'Content-Type' : 'application/json'
    }
}).then(res => res.json()).then(result => {
    employeeIdContainer.value = result.employeeId
    firstNameContainer.value = result.firstName
    lastNameContainer.value = result.lastName
    usernameContainer.value = result.userName
    passwordContainer.value = result.passwords
    initialPositionOption.value = result.position
    initialPositionOption.innerText = result.position
    addOnContainer.value = result.addOn

    if(result.isRequestor) {
        initialAccountTypeOption.innerText = 'Requestor'
        initialAccountTypeOption.value = 'Requestor'
    } else if(result.isApprover) {
        initialAccountTypeOption.innerText = 'Approver'
        initialAccountTypeOption.value = 'Approver'
    } else {
        initialAccountTypeOption.innerText = 'Administrator'
        initialAccountTypeOption.value = 'Administrator'
    }
})