// Change Password Function
function changePasswordFunction() {
	let username = document.getElementById('username').value
    let oldPassword = document.getElementById('oldPassword').value
	let newPassword = document.getElementById('newPassword').value
	let confirmPassword = document.getElementById('confirmPassword').value

	username = username.toUpperCase()
	
    if(username === '') {
        alert('Please input your Username')
    } else if(oldPassword === '') {
        alert('Please input your Old Password')
    } else if(newPassword === '') {
		alert('Please input your New Password')
	} else if(confirmPassword === '') {
        alert('Please Confirm your New Password')
    } else if(newPassword !== confirmPassword && newPassword !== '' && confirmPassword !== '') {
		alert('New Password and Confirm Password did not matched!')
	} else if(oldPassword == newPassword) {
        alert('Old Password and New Password must be different')
	} else {
        fetch('http://localhost:4000/api/users/validate', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				userName: username.toUpperCase()
			})
		}).then(res => res.json()).then(result => {
            if(result !== true) {
                alert('Incorrect Username and/or Password')
				window.location.reload()
            } else {
                fetch('http://localhost:4000/api/users/login', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						userName: username.toUpperCase(),
						password: oldPassword
					})
				}).then(res => res.json()).then(result => {
                    if(!result.accessToken) {
                        alert('Incorrect Username and/or Password')
				        window.location.reload()
                    } else {
                        fetch(`http://localhost:4000/api/users/changePassword/${username}`, {
                            method: 'PUT',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                password: newPassword
                            })
                        }).then(() => {
                            fetch(`http://localhost:4000/api/users/function/email/${username}`, {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json'
                                },
                                body: JSON.stringify({
                                    newPassword: newPassword
                                })
                            }).then(() => {
                                alert('Successfully changed your password!')
                                window.location.replace('../index.html')
                            })
                        })
                    }
                })
            }
        })
	}
}

document.getElementById('changePasswordButton').onclick = function(e) {
	e.preventDefault()
	changePasswordFunction()
}