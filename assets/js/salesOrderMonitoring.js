// Global Variables
let token = localStorage.getItem("token") 
let selectedSalesOrders = []


// Filter Status
if(userData.isApprover === true){
    filterStatus.innerHTML = 
    `
    <option value="forApprovalStatusApprover">For Approval</option>
    <option value="allStatusApprover">All Status</option> 
    <option value="approvedStatusApprover">Approved</option>
    <option value="rejectedStatusApprover">Rejected</option>
    <option value="postingErrorStatusApprover">Posting Error</option>
   `
}

if(userData.isRequestor === true){
    filterStatus.innerHTML = 
    `
    <option value="allStatusRequestor">All Status</option>
    <option value="inPreparationStatusRequestor">In Preparation</option>
    <option value="forApprovalStatusRequestor">For Approval</option>
    <option value="postedStatusRequestor">Posted</option>
    <option value="approvedStatusRequestor">Approved</option>
    <option value="cancelStatusRequestor">Cancelled</option>
    <option value="rejectedStatusRequestor">Rejected</option>
    <option value="postingErrorStatusRequestor">Posting Error</option>
   `
}

// Change of Filter by Status
function statusFilterCallback() {
    let statusFilterValue = document.getElementById('filterStatus').value

    if(statusFilterValue === 'allStatusRequestor') {
        fetch(`http://localhost:4000/api/salesOrder/status/all/requestor/${employeeId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    return(
                        `
                        <tr>
                            <td style="width: 2rem;"><div><div><input type="checkbox" id="flexCheckDefault"></div></td>
                            <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                            <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                            <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                            <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                            <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                            <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                            <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                            <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                            <td tyle="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                            <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                            <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                            <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                            <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                            <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                            <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                            <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("")
                salesOrderTable.innerHTML = salesOrder;              
            }
        })
    } else if(statusFilterValue === 'inPreparationStatusRequestor') {
        fetch(`http://localhost:4000/api/salesOrder/status/inPreparation/${employeeId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No In Preparation Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                        <td tyle="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'forApprovalStatusRequestor') {
        fetch(`http://localhost:4000/api/salesOrder/status/forApproval/${employeeId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No For Approval Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                        <td tyle="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'postedStatusRequestor') {
        fetch(`http://localhost:4000/api/salesOrder/status/posted/${employeeId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Posted Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><div><input type="checkbox" id="flexCheckDefault"></div></td>
                            <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                            <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                            <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                            <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                            <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                            <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                            <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                            <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                            <td tyle="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                            <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                            <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                            <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                            <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                            <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                            <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                            <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'approvedStatusRequestor') {
        fetch(`http://localhost:4000/api/salesOrder/status/approved/${employeeId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Approved Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                        <td tyle="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'cancelStatusRequestor') {
        fetch(`http://localhost:4000/api/salesOrder/status/cancelled/${employeeId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Cancelled Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                        <td tyle="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'rejectedStatusRequestor') {
        fetch(`http://localhost:4000/api/salesOrder/status/rejected/${employeeId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Rejected Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                        <td tyle="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'postingErrorStatusRequestor') {
        fetch(`http://localhost:4000/api/salesOrder/status/postingError/${employeeId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Posting Error Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                        <td tyle="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'forApprovalStatusApprover') {
        fetch(`http://localhost:4000/api/salesOrder/status/all/forApproval`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No For Approval Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                        <td tyle="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'allStatusApprover') {
        fetch(`http://localhost:4000/api/salesOrder/status/all/approver`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                        <td tyle="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    // } else if(statusFilterValue === 'postedStatusApprover') {
    //     fetch(`http://localhost:4000/api/salesOrder/status/all/posted`, {
    //         method: 'GET',
    //         headers: {
    //             'Content-Type': 'application/json'
    //         }
    //     }).then(res => res.json()).then(data => {
    //         if (data.length < 1){
    //             salesOrder = `<h1 style="text-align: center;">No Posted Sales Quote Available</h1>`
    //             salesOrderTable.innerHTML = salesOrder;
    //         } else {
    //             data.reverse()
    //             salesOrder = data.map(result=> {
    //                 return(
    //                     `
    //                     <tr>
    //                         <td style="width: 2rem"><input type="checkbox" id="flexCheckDefault"></div></td>
    //                         <td style="text-align: center; width: 5rem;"><div>${result.salesOrderNo}</div></td>
    //                         <td style="text-align: center; width: 5rem;"><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
    //                         <td style="text-align: center; width: 7rem;"><div>${result.docStatus}</div></td>
    //                         <td style="text-align: center; width: 5rem;"><div>${result.accountId}</div></td>
    //                         <td style="text-align: center; width: 24rem;"><div>${result.accountName}</div></td>
    //                         <td style="text-align: center; width: 6.5rem;"><div>${parseFloat(result.creditLimit).toLocaleString('en-US', { minimumFractionDigits: 0 })}</div></td>
    //                         <td style="text-align: center; width: 6.5rem;"><div>${parseFloat(result.accountsReceivable.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
    //                         <td style="text-align: center; width: 4rem;"><div>${result.salesGroup}</div></td>
    //                         <td style="text-align: center; width: 6.5rem;"><div>${result.customerGroup}</div></td>
    //                         <td style="text-align: center; width: 10rem;"><div>${result.externalReference}</div></td>
    //                         <td style="text-align: center; width: 7.5rem;"><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
    //                         <td style="text-align: center; width: 7rem;"><div>${result.creationDate}</div></td>
    //                         <td style="text-align: center; width: 8rem;"><div>${result.employeeName}</div></td>
    //                         <td style="text-align: center; width: 13rem;"><div>${result.validator}</div></td>
    //                         <td style="text-align: center; width: 12rem;"><div>${result.validationDate}</div></td>
    //                         <td style="text-align: center; width: 8rem;"><div>${result.deliveryBlockReason}</div></td>
    //                         <td style="text-align: center; width: 9rem;"><div>${result.cancelReason}</div></td>
    //                     </tr>
    //                     `
    //                 )
    //             }).join("");
    //             salesOrderTable.innerHTML = salesOrder;                       
    //         }
    //     })
    } else if(statusFilterValue === 'approvedStatusApprover') {
        fetch(`http://localhost:4000/api/salesOrder/status/all/approved`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Approved Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    return(
                        `
                        <tr>
                            <td style="width: 2rem;"><div><div><input type="checkbox" id="flexCheckDefault"></div></td>
                            <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                            <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                            <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                            <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                            <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                            <td style="text-align: center; width: 6.5rem;"><div><div>${parseFloat(result.creditLimit).toLocaleString('en-US', { minimumFractionDigits: 0 })}</div></td>
                            <td style="text-align: center; width: 6.5rem;"><div><div>${parseFloat(result.accountsReceivable.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                            <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                            <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                            <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                            <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                            <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                            <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                            <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                            <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                            <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                            <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'rejectedStatusApprover') {
        fetch(`http://localhost:4000/api/salesOrder/status/all/rejected`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Rejected Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    return(
                        `
                        <tr>
                            <td style="width: 2rem;"><div><div><input type="checkbox" id="flexCheckDefault"></div></td>
                            <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                            <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                            <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                            <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                            <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                            <td style="text-align: center; width: 6.5rem;"><div><div>${parseFloat(result.creditLimit).toLocaleString('en-US', { minimumFractionDigits: 0 })}</div></td>
                            <td style="text-align: center; width: 6.5rem;"><div><div>${parseFloat(result.accountsReceivable.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                            <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                            <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                            <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                            <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                            <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                            <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                            <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                            <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                            <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                            <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'postingErrorStatusApprover') {
        fetch(`http://localhost:4000/api/salesOrder/status/all/postingError`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Posting Error Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                data.reverse()
                salesOrder = data.map(result=> {
                    return(
                        `
                        <tr>
                        <td style="width: 2rem;"><div><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                        <td tyle="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    }
}

document.getElementById('filterStatus').onchange = function() {
	statusFilterCallback()
};

// Buttons - Approver
let buttonContainerList = document.getElementById('buttonRow')


if(userData.isApprover === true) {
    buttonContainerList.innerHTML = 
	`
	<button id="viewButtonApprover" class="functionalButtons">View</button>
    <button id="approveButton" class="functionalButtons">Approve</button>
    <button id="rejectButton" class="functionalButtons">Reject</button>
	<button id="closeButton" class="functionalButtons">Close</button>
	`

    document.getElementById('viewButtonApprover').onclick = function(e) {
        e.preventDefault()
        getValuesOfCheckedBox()
        
        if(selectedSalesOrders.length < 1) {
            alert('Please Select a Sales Order')
        } else if(selectedSalesOrders.length > 1) {
            selectedSalesOrders.length = 0
            alert('Action cannot be performed on multiple line items. Please select a single line item')
        } else {
            viewFunction()
        }
    }

    document.getElementById('approveButton').onclick = function(e) {
        e.preventDefault()
        getValuesOfCheckedBox()

        if(selectedSalesOrders.length < 1) {
            alert('Please Select a Sales Order')
        } else if(selectedSalesOrders.length > 1) {
            selectedSalesOrders.length = 0
            alert('Action cannot be performed on multiple line items. Please select a single line item')
        } else {
            if(selectedSalesOrders[0].docStatus !== 'For Approval') {
                alert(`The selected Sales Quote is already ${selectedSalesOrders[0].docStatus}`)
                selectedSalesOrders.length = 0
            } else {
                askInvoiceBlockModalFunction()
            }
        }
    }

    document.getElementById('rejectButton').onclick = function(e) {
        e.preventDefault()
        getValuesOfCheckedBox()
        rejectFunction()
    }

    document.getElementById("closeButton").onclick = function(e) {
        e.preventDefault()
        closeFunction()
    }
}

// Ask if the Approver wants to set Invoice Block or Not
function askInvoiceBlockModalFunction() {
	let myModal = document.getElementById("yesOrNoModal");
	$(myModal).modal("show")

	let closeButtonModal = document.getElementById('closeYesOrNoModalButton');
	closeButtonModal.onclick = function() {
        selectedSalesOrders.length = 0
		$(myModal).modal("hide")
	}

	window.onclick = function(event) {
		if (event.target === myModal) {
            selectedSalesOrders.length = 0
			$(myModal).modal("hide")
		}
	}

	let yesButton = document.getElementById('yesButton')
	yesButton.onclick = function() {
		setInvoiceBlockModalFunction()
		$(myModal).modal("hide")
	}

	let noButton = document.getElementById('noButton')
	noButton.onclick = function() {
		approveFunction()
		$(myModal).modal("hide")
	}
}

// To select Invoice Block if Approver selected Yes Button
function setInvoiceBlockModalFunction() {
	let myModal = document.getElementById("invoiceBlockModal");
	$(myModal).modal("show")

	let closeButtonModal = document.getElementById('closeInvoiceBlockModalButton');
	closeButtonModal.onclick = function() {
        selectedSalesOrders.length = 0
		$(myModal).modal("hide")
	}

	window.onclick = function(event) {
		if (event.target === myModal) {
            selectedSalesOrders.length = 0
			$(myModal).modal("hide")
		}
	}

	let invoiceBlockModalButton = document.getElementById('invoiceBlockModalButton')
	invoiceBlockModalButton.onclick = function() {
		let reasonCode = document.getElementById('invoiceBlockSelection').value
		let reasonDescription = document.getElementById('invoiceBlockSelection').options[invoiceBlockSelection.selectedIndex].text
		setInvoiceBlock(reasonCode, reasonDescription)
		$(myModal).modal("hide")
	}
}

// To set Invoice Block Reason
function setInvoiceBlock(reasonCode, reasonDescription) {
	fetch(`http://localhost:4000/api/salesOrder/function/setInvoiceBlock/${selectedSalesOrders[0].salesQuoteNo}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			invoiceBlockReasonCode: reasonCode,
			invoiceBlockReason: reasonDescription
		})
	}).then(() => {
		approveFunctionWithInvoiceBlock()
	})
}

// Approve Function - with Invoice Block
function approveFunction() {
    fetch(`http://localhost:4000/api/salesOrder/function/approve/${selectedSalesOrders[0].salesQuoteNo}`, {
		method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            validator: `${firstName} ${lastName}`
        })
	}).then(() => {
        document.getElementById('header').setAttribute('class', 'blur')
        document.getElementById('salesOrderMonitoringForm').setAttribute('class', 'blur')

        $("#myimgdivshowhide").show()

        let docId = selectedSalesOrders[0].salesQuoteNo
		statusChecker(docId)
	})
}

// Approve Function - with Invoice Block
function approveFunctionWithInvoiceBlock() {
    fetch(`http://localhost:4000/api/salesOrder/function/approve/${selectedSalesOrders[0].salesQuoteNo}`, {
		method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            validator: `${firstName} ${lastName}`
        })
	}).then(() => {
        document.getElementById('header').setAttribute('class', 'blur')
        document.getElementById('salesOrderMonitoringForm').setAttribute('class', 'blur')

        $("#myimgdivshowhide").show()

        let docId = selectedSalesOrders[0].salesQuoteNo
		statusChecker(docId)
	})
}

async function statusChecker(docId) {
    let response = await fetch(`http://localhost:4000/api/salesOrder/function/viewStatus/${docId}`)
    let updatedResponse = await response.json()

    if(updatedResponse.docStatus === "In Preparation" || updatedResponse.docStatus === "For Approval") {
        statusChecker(docId)
    } else {
        selectedSalesOrders.length = 0
        $("#myimgdivshowhide").hide()
        window.location.reload()
    }
}

// Reject Function
function rejectFunction() {
    let wrongStatus = [];
    for(let i = 0; i < selectedSalesOrders.length; i++) {
        if(selectedSalesOrders[i].docStatus !== 'For Approval') {
            wrongStatus.push({
                salesQuoteNo: selectedSalesOrders[i].salesQuoteNo,
                docStatus: selectedSalesOrders[i].docStatus
            })
            continue;
        } else {
            fetch(`http://localhost:4000/api/salesOrder/function/reject/${selectedSalesOrders[i].salesQuoteNo}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
        }
    }

    if(wrongStatus.length < 1) {
        alert('Successfully Rejected Sales Quotes')	
    } else {
        alert(`Some Sales Quote/s failed to Reject.`)
    }

    selectedSalesOrders.length = 0
    window.location.reload();
}

// Buttons - Requestor
if(userData.isRequestor === true) {
	buttonContainerList.innerHTML =
	`
    <button id="newButton" class="functionalButtons">New</button>
    <button id="viewButtonRequestor" class="functionalButtons">View</button>
    <button id="postButton" class="functionalButtons">Post</button>
    <button id="cancelButton" class="functionalButtons">Cancel</button>
    <button id="closeButton" class="functionalButtons">Close</button>
	`

    document.getElementById("newButton").onclick = function(e) {
        e.preventDefault()
        newFunction()
    }

    document.getElementById("viewButtonRequestor").onclick = function(e) {
        e.preventDefault()
        getValuesOfCheckedBox()

        if(selectedSalesOrders.length < 1) {
            alert('Please Select a Sales Order')
        } else if(selectedSalesOrders.length > 1) {
            selectedSalesOrders.length = 0
            alert('Action cannot be performed on multiple line items. Please select a single line item')
        } else {
            viewFunction()
        }
    }

    document.getElementById("postButton").onclick = function(e) {
        e.preventDefault()
        getValuesOfCheckedBox()
        postFunction()
    }

    document.getElementById('cancelButton').onclick = function(e){
		e.preventDefault()
		getValuesOfCheckedBox()

        if(selectedSalesOrders.length < 1) {
            alert('Please select a Sales Quote')
        } else if(selectedSalesOrders.length > 1) {
            selectedSalesOrders.length = 0
            alert('Action cannot be performed on multiple line items. Please select a single line item')
        } else {
            if(selectedSalesOrders[0].docStatus !== 'In Preparation') {
                alert(`The selected Sales Quote is already ${selectedSalesOrders[0].docStatus}`)
                selectedSalesOrders.length = 0
            } else {
                cancelModalFunction()
            }
        }
	}

    document.getElementById("closeButton").onclick = function(e) {
        e.preventDefault()
        closeFunction()
    }
}

// New Function
function newFunction(){
    location.replace("./salesOrderCreation.html")
}
        
// View Function
function viewFunction() {
    let salesQuoteNumber = selectedSalesOrders[0].salesQuoteNo
    selectedSalesOrders.length = 0
    window.location.replace(`./salesOrderViewing.html?documentId=${salesQuoteNumber}`)
}

// Post Function
function postFunction() {
    let withIssue = []
    let withoutIssue = []


    for(let i = 0; i < selectedSalesOrders.length; i++) {
        if(selectedSalesOrders[i].docStatus !== "In Preparation") {
            continue
        } else {
            fetch(`http://localhost:4000/getCurrentSpendingLimit/${selectedSalesOrders[i].accountId}`, {
                method: 'POST'
            }).then(response => {
                return response.json()
            }).then(result => {
                let csl = parseFloat(result['AccountOpenAmounts']['0']['CurrentSpendingLimitAmount']['0']['_'])

                fetch(`http://localhost:4000/api/salesOrder/allSalesOrderinDatabase/${selectedSalesOrders[i].accountId}`,{
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(response => {
                    return response.json()
                }).then(result => {
                    let totalValueForSpecificCustomer = 0

                    for(a = 0; a < result.length; a++) {
                        let totalAmountPerItem = 0
                        totalAmountPerItem = parseFloat(result[a].taxAmount) + parseFloat(result[a].netValue)
                        totalValueForSpecificCustomer += totalAmountPerItem
                    }

                    let updatedCSL = csl - totalValueForSpecificCustomer

                    return updatedCSL.toFixed(2)
                }).then((updatedCSL) => {
                    if(updatedCSL < 0) {
                        withIssue.push(selectedSalesOrders[i])

                        fetch(`http://localhost:4000/api/salesOrder/function/setDeliveryBlock/${selectedSalesOrders[i].salesQuoteNo}`, {
                            method: 'PUT',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                deliveryBlockReasonCode: '01',
                                deliveryBlockReason: 'Credit Limit',
                                docStatus: 'For Approval'
                            })
                        })
                    } else if(parseFloat(selectedSalesOrders[i].totalAmount) > updatedCSL) {
                        withIssue.push(selectedSalesOrders[i])

                        fetch(`http://localhost:4000/api/salesOrder/function/setDeliveryBlock/${selectedSalesOrders[i].salesQuoteNo}`, {
                            method: 'PUT',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                deliveryBlockReasonCode: '01',
                                deliveryBlockReason: 'Credit Limit',
                                docStatus: 'For Approval'
                            })
                        })
                    } else {
                        withoutIssue.push(selectedSalesOrders[i])

                        let validatorValue = `${firstName} ${lastName}`
                        fetch(`http://localhost:4000/api/salesOrder/function/post/${selectedSalesOrders[i].salesQuoteNo}`, {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                validator: validatorValue
                            })
                            
                        })
                    }
                })
            })
        }
    } 
}
                
// Cancel Modal Function
function cancelModalFunction() {
    let documentId = selectedSalesOrders[0].salesQuoteNo

    let myModal = document.getElementById("cancelModal");
    $(myModal).modal("show")

    let closeButtonModal = document.getElementById('closeCancelModalButton')
    closeButtonModal.onclick = function() {
        $(myModal).modal("hide")
    }

    window.onclick = function(event) {
        if (event.target === myModal) {
            $(myModal).modal("hide")
        }
    }

    let cancelButton = document.getElementById('cancelModalButton')
    cancelButton.onclick = function() {
        let explanation = document.getElementById('explanation').value
        cancelFunction(documentId, explanation)
        $(myModal).modal("hide")
    }
}

// Cancel Function
function cancelFunction(documentId, explanation) {
    let validatorName = `${firstName} ${lastName}`
    fetch(`http://localhost:4000/api/salesOrder/function/cancel/${documentId}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            cancelReason: explanation,
            validator: validatorName
        })
    }).then(() => {
        selectedSalesOrders.length = 0
        alert('Successfully Cancelled the Sales Quote')
        window.location.reload();
    })
}

// Close Function
function closeFunction(){
	window.location.replace('./launchpad.html')
}

// Select All Checkbox
function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}

// Select a Checkbox
function getValuesOfCheckedBox() {
    //Reference the Table.
    let grid = document.getElementById("salesOrderTable");

    //Reference the CheckBoxes in Table.
    let checkBoxes = grid.getElementsByTagName("INPUT");

    //Loop through the CheckBoxes.
    for (let i = 0; i < checkBoxes.length; i++) {
        if (checkBoxes[i].checked) {
            let row = checkBoxes[i].parentNode.parentNode
            let data = {
                salesOrderId: row.cells[1].innerHTML,
                salesQuoteNo: row.cells[2].innerText,
                docStatus: row.cells[3].innerHTML,
                accountId: row.cells[4].innerHTML,
                accountName:  row.cells[5].innerHTML,
                salesGroup: row.cells[6].innerHTML,
                customerGroup: row.cells[7].innerHTML,
                externalReference: row.cells[8].innerHTML,
                totalAmount:  row.cells[9].innerHTML,
                creationDate: row.cells[10].innerHTML,
                createdBy:  row.cells[11].innerHTML,
                validator:  row.cells[12].innerHTML,
                validationDate: row.cells[13].innerHTML,
                deliveryBlockReason: row.cells[14].innerHTML,
                cancelReason: row.cells[15].innerHTML
            }
            selectedSalesOrders.push(data)
            console.log(selectedSalesOrders)
        }
    }
}

// Adding Credit Limit Column if Approver 
if(isApprover === 'true') {
    let creditLimitColumn = document.createElement('th')
    let creditLimitColumnName = 'Credit Limit'
    creditLimitColumn.style.width = '6.5rem'
    creditLimitColumn.setAttribute('id', "creditLimitColumn")
    creditLimitColumn.innerText = creditLimitColumnName

    let accountNameColumn = document.getElementById('accountNameColumn')
    accountNameColumn.insertAdjacentElement('afterend', creditLimitColumn)

    let accountReceivableColumn = document.createElement('th')
    let accountReceivableColumnName = 'Receivable'
    accountReceivableColumn.style.width = '6.5rem'
    accountReceivableColumn.innerText = accountReceivableColumnName

    let creditLimitCol = document.getElementById('creditLimitColumn')
    creditLimitCol.insertAdjacentElement('afterend', accountReceivableColumn)
}

// Populate Sales Order Table
function fetchSalesOrder() {
    if (localStorage.getItem("isApprover") === "true") {
        fetchSalesOrderApprover()
    } else {
        fetchSalesOrderRequestor()
    }
}

function fetchSalesOrderApprover() {
    fetch(`http://localhost:4000/api/salesOrder/status/all/forApproval`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        } 
    }).then(res => res.json()).then(data => {
        if (data.length < 1){
            salesOrder = `<h1 style="text-align: center;">No For Approval Sales Quote Available</h1>`
            salesOrderTable.innerHTML = salesOrder;
        } else {
            data.reverse()
            salesOrder = data.map(result=> {
                return(
                    `
                    <tr>
                        <td style="width: 2rem"><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div><div>${parseFloat(result.creditLimit).toLocaleString('en-US', { minimumFractionDigits: 0 })}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div><div>${parseFloat(result.accountsReceivable.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                        <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                        <td style="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                    </tr>
                    `
                )
            }).join("")
            salesOrderTable.innerHTML = salesOrder;                       
        }
    })
}
    
function fetchSalesOrderRequestor() {
    fetch(`http://localhost:4000/api/salesOrder/status/all/requestor/${employeeId}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json()).then(data => {
        if (data.length < 1){
            salesOrder = `<h1 style="text-align: center;">No Sales Quote Available</h1>`
            salesOrderTable.innerHTML = salesOrder;
        } else {
            data.reverse()
            salesOrder = data.map(result=> {
                return(
                    `
                    <tr>
                        <td style="width: 2rem"><div><input type="checkbox" id="flexCheckDefault"></div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                        <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                        <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                        <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                        <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                        <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                        <td style="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                        <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                        <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                        <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                        <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                        <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                        <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                    </tr>
                    `
                )
            }).join("")
            salesOrderTable.innerHTML = salesOrder
        }
    })
}

fetchSalesOrder();

// Sort by Account Name
let accountNameColumn = document.getElementById('accountNameColumn')
let accountNameColumnAscending = false

function sortAccountNameAscending() {
    fetch(`http://localhost:4000/api/salesOrder/sort/ascending/accountName`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json()).then(data => {
        salesOrder = data.map(result=> {
            return(
                `
                <tr>
                    <td style="width: 2rem"><div><input type="checkbox" id="flexCheckDefault"></div></td>
                    <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                    <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                    <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                    <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                    <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                    <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                    <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                    <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                    <td style="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                    <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                    <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                    <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                    <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                    <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                    <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                    <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                </tr>
                `
            )
        }).join("")
        accountNameColumnAscending = true
        salesOrderTable.innerHTML = salesOrder
    })
}

function sortAccountNameDescending() {
    fetch(`http://localhost:4000/api/salesOrder/sort/descending/accountName`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json()).then(data => {
        salesOrder = data.map(result=> {
            return(
                `
                <tr>
                    <td style="width: 2rem"><div><input type="checkbox" id="flexCheckDefault"></div></td>
                    <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                    <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                    <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                    <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                    <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                    <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                    <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                    <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                    <td style="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                    <td style="text-align: center; width: 7.5rem;"><div><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                    <td style="text-align: center; width: 7rem;"><div><div>${result.creationDate}</div></td>
                    <td style="text-align: center; width: 8rem;"><div><div>${result.employeeName}</div></td>
                    <td style="text-align: center; width: 13rem;"><div><div>${result.validator}</div></td>
                    <td style="text-align: center; width: 12rem;"><div><div>${result.validationDate}</div></td>
                    <td style="text-align: center; width: 8rem;"><div><div>${result.deliveryBlockReason}</div></td>
                    <td style="text-align: center; width: 9rem;"><div><div>${result.cancelReason}</div></td>
                </tr>
                `
            )
        }).join("")
        accountNameColumnAscending = false
        salesOrderTable.innerHTML = salesOrder
    })
}

accountNameColumn.onclick = function() {
    let myModal = document.getElementById("ascendDescendModal");
	$(myModal).modal("show")

	window.onclick = function(event) {
		if (event.target === myModal) {
            selectedSalesOrders.length = 0
			$(myModal).modal("hide")
		}
	}

    let ascendModalButton = document.getElementById('ascendModalButton')
    ascendModalButton.onclick = function() {
        sortAccountNameAscending()
        $(myModal).modal("hide")
    }

    let descendModalButton = document.getElementById('descendModalButton')
    descendModalButton.onclick = function() {
        sortAccountNameDescending()
        $(myModal).modal("hide")
    }

    let removeSortModalButton = document.getElementById('removeSortModalButton')
    removeSortModalButton.onclick = function() {
        if(isApprover === 'true') {
            fetchSalesOrderApprover()
        } else {
            fetchSalesOrderRequestor()
        }
        $(myModal).modal("hide")
    }
}

// Sort by Account ID
let accountIdColumn = document.getElementById('accountIdColumn')
let accountIdColumnAscending = false

function sortAccountIdAscending() {
    fetch(`http://localhost:4000/api/salesOrder/sort/ascending/accountId`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json()).then(data => {
        salesOrder = data.map(result=> {
            return(
                `
                <tr>
                    <td style="width: 2rem"><div><input type="checkbox" id="flexCheckDefault"></div></td>
                    <td style="text-align: center; width: 5rem;"><div><div>${result.salesOrderNo}</div></td>
                    <td style="text-align: center; width: 5rem;"><div><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                    <td style="text-align: center; width: 7rem;"><div><div>${result.docStatus}</div></td>
                    <td style="text-align: center; width: 5rem;"><div><div>${result.accountId}</div></td>
                    <td style="text-align: center; width: 24rem;"><div><div>${result.accountName}</div></td>
                    <td style="text-align: center; width: 4rem;"><div><div>${result.salesGroup}</div></td>
                    <td style="text-align: center; width: 6.5rem;"><div><div>${result.customerGroup}</div></td>
                    <td style="text-align: center; width: 10rem;"><div><div>${result.externalReference}</div></td>
                    <td style="text-align: center; width: 4rem;"><div><div>${result.currency}</div></td>
                    <td style="text-align: center; width: 7.5rem;"><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                    <td style="text-align: center; width: 7rem;"><div>${result.creationDate}</div></td>
                    <td style="text-align: center; width: 8rem;"><div>${result.employeeName}</div></td>
                    <td style="text-align: center; width: 13rem;"><div>${result.validator}</div></td>
                    <td style="text-align: center; width: 12rem;"><div>${result.validationDate}</div></td>
                    <td style="text-align: center; width: 8rem;"><div>${result.deliveryBlockReason}</div></td>
                    <td style="text-align: center; width: 9rem;"><div>${result.cancelReason}</div></td>
                </tr>
                `
            )
        }).join("")
        accountNameColumnAscending = true
        salesOrderTable.innerHTML = salesOrder
    })
}

function sortAccountIdDescending() {
    fetch(`http://localhost:4000/api/salesOrder/sort/descending/accountId`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json()).then(data => {
        salesOrder = data.map(result=> {
            return(
                `
                <tr>
                    <td style="width: 2rem"><input type="checkbox" id="flexCheckDefault"></div></td>
                    <td style="text-align: center; width: 5rem;"><div>${result.salesOrderNo}</div></td>
                    <td style="text-align: center; width: 5rem;"><div><a href='salesOrderViewing.html?documentId=${result.salesQuoteNo}'>${result.salesQuoteNo}</a></div></td>
                    <td style="text-align: center; width: 7rem;"><div>${result.docStatus}</div></td>
                    <td style="text-align: center; width: 5rem;"><div>${result.accountId}</div></td>
                    <td style="text-align: center; width: 24rem;"><div>${result.accountName}</div></td>
                    <td style="text-align: center; width: 4rem;"><div>${result.salesGroup}</div></td>
                    <td style="text-align: center; width: 6.5rem;"><div>${result.customerGroup}</div></td>
                    <td style="text-align: center; width: 10rem;"><div>${result.externalReference}</div></td>
                    <td style="text-align: center; width: 4rem;"><div>${result.currency}</div></td>
                    <td style="text-align: center; width: 7.5rem;"><div>${parseFloat(result.totalSalesQuoteAmount.replace(/,/g, "")).toLocaleString('en-US', { minimumFractionDigits: 2 })}</div></td>
                    <td style="text-align: center; width: 7rem;"><div>${result.creationDate}</div></td>
                    <td style="text-align: center; width: 8rem;"><div>${result.employeeName}</div></td>
                    <td style="text-align: center; width: 13rem;"><div>${result.validator}</div></td>
                    <td style="text-align: center; width: 12rem;"><div>${result.validationDate}</div></td>
                    <td style="text-align: center; width: 8rem;"><div>${result.deliveryBlockReason}</div></td>
                    <td style="text-align: center; width: 9rem;"><div>${result.cancelReason}</div></td>
                </tr>
                `
            )
        }).join("")
        accountNameColumnAscending = false
        salesOrderTable.innerHTML = salesOrder
    })
}

accountIdColumn.onclick = function() {
    let myModal = document.getElementById("ascendDescendModal");
	$(myModal).modal("show")

	window.onclick = function(event) {
		if (event.target === myModal) {
            selectedSalesOrders.length = 0
			$(myModal).modal("hide")
		}
	}

    let ascendModalButton = document.getElementById('ascendModalButton')
    ascendModalButton.onclick = function() {
        sortAccountIdAscending()
        $(myModal).modal("hide")
    }

    let descendModalButton = document.getElementById('descendModalButton')
    descendModalButton.onclick = function() {
        sortAccountIdDescending()
        $(myModal).modal("hide")
    }

    let removeSortModalButton = document.getElementById('removeSortModalButton')
    removeSortModalButton.onclick = function() {
        if(isApprover === 'true') {
            fetchSalesOrderApprover()
        } else {
            fetchSalesOrderRequestor()
        }
        $(myModal).modal("hide")
    }
}