let userData = JSON.parse(localStorage.getItem('userData'))
console.log(userData)
let userMenuNav = document.getElementById('userMenuNav');

if (userData == null){
    console.log("null")
    // window.location.replace('/index.html');
} else {
    if(window.location.href.indexOf("/pages/launchPad.html") != -1){
        userMenuNav.innerHTML = 
    `
    <div class="dropdown">
      <button onclick="openDropDownList()" class="dropDown">User Menu</button>
        <div id="userDropDown" class="dropDownContent">
        <a href="#">User Details</a>
        <a href="#">Change Password</a>
        <a href="#" onclick="logOut()">Logout</a>
        </div>
    </div>
    `
    } else {
        userMenuNav.innerHTML = 
    `
    <div class="dropdown">
      <button onclick="openDropDownList()" class="dropDown">User Menu</button>
        <div id="userDropDown" class="dropDownContent">
        <a href="#" onclick="home()">Home</a>
        <a href="#">Change Password</a>
        <a href="#" onclick="logOut()">Logout</a>
        </div>
    </div>
    `
    }

    
    openDropDownList = () => {
        document.querySelector('.dropDownContent').classList.toggle('show');
    }

    home = () => {
        window.location.replace('/pages/launchpad.html')
    }

    logOut = () => {
        localStorage.clear();
        userData = null;
        if (confirm("Are you sure you want to Log Out?") == true) {
            window.location.replace('/pages/logout.html');
          } 
    }
}