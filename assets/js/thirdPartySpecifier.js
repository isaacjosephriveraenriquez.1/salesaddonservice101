//Global Variable
let thirdPartySpecifierButton = document.querySelector("#addThirdPartySpecifierButton");
let codeInput = document.querySelector("#code");
let vendorAccountInput = document.querySelector("#vendorAccount");
let tbody = document.querySelector("#tbody");
let back = document.getElementById('Back');
let tHeadRowCell = document.getElementsByTagNameNS('th');
let codeQuery = document.getElementById("codeQuery");
let vendorAccountQuery = document.getElementById("vendorAccountQuery");
let sortQuery = document.getElementById("sortQuery");

tHeadRowCell.onclick = () => {
    codeQuery.style.display = 'block';
    vendorAccountQuery.display = 'block';
    sortQuery.display = 'block';
    console.log(help)
};

console.log(tHeadRowCell)





back.onclick = () => window.location.replace("/pages/launchpad.html")

fetch('http://localhost:4000/api/tps/view', {
    method: 'Get',
    headers: {
        'Content-Type' : 'application/json'
    }
}).then(res => res.json()).then(result => {
    if (result.length < 1){
        thirdPartySpecifierList = `<h1 style="text-align: center;">No Third Party Specifier Available</h1>`
        tbody.innerHTML = salesOrder;
    } else {
    thirdPartySpecifierList = result.map(result=> {
    return(
        `
        <tr>
        <td>${result.code}</td>
        <td>${result.vendorAccount}</td>
        <td><button>Edit => Update</button>
            <button onclick="deleteThirdPartySpecifier">Delete</button></td>
        </tr>
        `
    )
}).join(""),
tbody.innerHTML = thirdPartySpecifierList    
}});

// Add Third Party Specifier
thirdPartySpecifierButton.onclick = function(){
    fetch('http://localhost:4000/api/tps/add', {
        method: 'POST',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
            code: codeInput.value,
            vendorAccount: vendorAccountInput.value
        })
    }).then(res => res.json()).then(() => {
        alert('Successfully Created Third Party Specifier')
    })  
}