let loginForm = document.querySelector("#loginForm");

loginForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let userName = document.getElementById("username").value;
	let password = document.getElementById("password").value;

	if (userName == "" || password == "") {
		alert("Please input your username and/or password.")
	} else { 
		fetch('http://localhost:4000/api/users/validate', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				userName: userName.toUpperCase()
			})
		}).then(res => res.json()).then(result => {
			if(result !== true) {
				alert('Incorrect Username')
			} else {
				fetch('http://localhost:4000/api/users/login', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						userName: userName.toUpperCase(),
						password: password
					})
				}).then(res => res.json()).then(data => {
					console.log(data)
					if(data.accessToken) {
						localStorage.setItem('token', data.accessToken);
						fetch(`http://localhost:4000/api/users/details`, {
							headers: {
								'Content-Type': 'application/json',
								Authorization: `Bearer ${data.accessToken}`
							}
						}).then(res => res.json()).then(data => {
							localStorage.setItem("userData", JSON.stringify(data));
							window.location.replace('/pages/launchpad.html');
						})
					} else {
						alert("Incorrect Password");
					}
				})
			}
		})
	}
})