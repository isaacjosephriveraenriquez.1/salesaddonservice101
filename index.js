const express = require('express')
const mongoose = require('mongoose')
const app = express()
const cors = require('cors')
const request = require('request')
const parseString = require('xml2js').parseString
const convert = require('xml-js');

app.use(cors())

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://ortegsssss:spci@cluster0.uswqp.mongodb.net/Dev_Database?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const currencyRoutes = require('./routes/rout_currency')
const customerRoutes = require('./routes/rout_customer')
const customerGroupRoutes = require('./routes/rout_customerGroup')
const distributionChannelRoutes = require('./routes/rout_distributionChannel')
const headerRoutes = require('./routes/rout_header')
const lineRoutes = require('./routes/rout_line')
const moqRoutes = require('./routes/rout_minimumOrderQuantity')
const paymentTermRoutes = require('./routes/rout_paymentTerm')
const productRoutes = require('./routes/rout_product')
const categoryRoutes = require('./routes/rout_productCategory')
const salesOrderRoutes = require('./routes/rout_salesOrder')
const tpsRoutes = require('./routes/rout_thirdPartySpecifier')
const uomRoutes = require('./routes/rout_unitOfMeasure')
const userRoutes = require('./routes/rout_user')
const salesOrderHistoryTrailRoutes = require('./routes/rout_salesOrderHistoryTrail')

app.use('/api/currency', currencyRoutes)
app.use('/api/customer', customerRoutes)
app.use('/api/customerGroup', customerGroupRoutes)
app.use('/api/distributionChannel', distributionChannelRoutes)
app.use('/api/header', headerRoutes)
app.use('/api/line', lineRoutes)
app.use('/api/moq', moqRoutes)
app.use('/api/terms', paymentTermRoutes)
app.use('/api/products', productRoutes)
app.use('/api/productCategory', categoryRoutes)
app.use('/api/salesOrder', salesOrderRoutes)
app.use('/api/tps', tpsRoutes)
app.use('/api/uom', uomRoutes)
app.use('/api/users', userRoutes)
app.use('/api/History', salesOrderHistoryTrailRoutes)

app.post('/getCurrentSpendingLimit/:accountId', (req,res) => {
    let accountId = req.params.accountId

	let xml = `
	<?xml version="1.0" encoding="utf-8"?>
	<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global">
		<soapenv:Header/>
		<soapenv:Body>
			<glob:AccountsOpenAmountsQueryRequest_sync>
					<AccountOpenAmountsSelection>
						<SelectionByAccountID>
                            <InclusionExclusionCode>I</InclusionExclusionCode>
                            <IntervalBoundaryTypeCode>1</IntervalBoundaryTypeCode>
                            <LowerBoundaryIdentifier>${accountId}</LowerBoundaryIdentifier>
					    </SelectionByAccountID>
					</AccountOpenAmountsSelection>
					<ProcessingConditions>
						<QueryHitsMaximumNumberValue>100</QueryHitsMaximumNumberValue>
						<QueryHitsUnlimitedIndicator>false</QueryHitsUnlimitedIndicator>
					</ProcessingConditions>
					<IncludeOpenAndInPreparationOrders>true</IncludeOpenAndInPreparationOrders>
			</glob:AccountsOpenAmountsQueryRequest_sync>
		</soapenv:Body>
	</soapenv:Envelope>
	`

	request({
		method: 'POST',
		uri: 'https://my355904.sapbydesign.com/sap/bc/srt/scs/sap/queryaccountopenamountsin',
		auth: {
			'user': '_AOS0001',
			'pass': '@dd0nP@$$',
			'sendImmediately': false
		},
		headers: {
			'Content-Type': 'text/xml'
		  },
		body: xml,
	},
	function (error, response, body) {
		if (error) {
			res.send (error)
		} else {
			parseString(body, function(err, result) {
				let x = result['soap-env:Envelope']['soap-env:Body']['0']['n0:AccountOpenAmountsQueryResponse_sync']['0']
				res.send(x)
			})
		}
	})
})

app.post('/getCurrentStockQuantity/:productId', (req,res) => {
    let productId = req.params.productId

    let xml = `
	<?xml version="1.0" encoding="utf-8"?>
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global">
        <soapenv:Header/>
        <soapenv:Body>
            <n0:ProductAvailabilityDeterminationQuery_sync xmlns:n0="http://sap.com/xi/SAPGlobal20/Global">
                <ProductAvailabilityDeterminationQuery>
                    <ConsiderScopeOfCheckIndicator>false</ConsiderScopeOfCheckIndicator>
                    <ProductAndSupplyPlanningArea>
                        <ProductInternalID>${productId.replace(/-/g, "/")}</ProductInternalID>
                        <ProductTypeCode>1</ProductTypeCode>
                        <SupplyPlanningAreaID>M001000111</SupplyPlanningAreaID>
                    </ProductAndSupplyPlanningArea>
                    <ProductAvailabilityDeterminationHorizonDuration>P1D</ProductAvailabilityDeterminationHorizonDuration>
                </ProductAvailabilityDeterminationQuery>
            </n0:ProductAvailabilityDeterminationQuery_sync>
		</soapenv:Body>
	</soapenv:Envelope>
	`

    request({
		method: 'POST',
		uri: 'https://my355904.sapbydesign.com/sap/bc/srt/scs/sap/productavailabilitydeterminati',
		auth: {
			'user': '_AOS0001',
			'pass': '@dd0nP@$$',
			'sendImmediately': false
		},
		headers: {
			'Content-Type': 'text/xml'
		  },
		body: xml,
	},
	function (error, response, body) {
		if(error) {
		    res.send (error)
		} else {
			parseString(body, function(err, result) {
                if(result['soap-env:Envelope']['soap-env:Body']['0'].hasOwnProperty("soap-env:Fault")) {
                    let data = result['soap-env:Envelope']['soap-env:Body']['0']['soap-env:Fault']['0']['faultstring']['0']
                    res.send(data)
                } else {
                    // if(result['soap-env:Envelope']['soap-env:Body']['0'].hasOwnProperty("n0:ProductAvailabilityDeterminationResponse_sync")) {
                    //     let data = result['soap-env:Envelope']['soap-env:Body']['0']['n0:ProductAvailabilityDeterminationResponse_sync']['0']['ProductAvailabilityDeterminationResponse']['0']['AvailableQuantity']['0']['_']
                    //     res.send(data)
                    // } 
					return "n/a"
                }
			})
		}
	})
})
// get customer from byd response time 1.11 minutes (1632 Customers)
app.post('/getCustomersByd', (req, res) => {
	let xml = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global">
	<soapenv:Header />
	<soapenv:Body>
	<n0:CustomerByCommunicationDataQuery_sync xmlns:n0="http://sap.com/xi/SAPGlobal20/Global">
		<CustomerSelectionByCommunicationData>
			<SelectionByEmailURI>
				<InclusionExclusionCode>I</InclusionExclusionCode>
				<IntervalBoundaryTypeCode>1</IntervalBoundaryTypeCode>
			</SelectionByEmailURI>
		</CustomerSelectionByCommunicationData>
		<ProcessingConditions>
		  <!--<QueryHitsMaximumNumberValue>10</QueryHitsMaximumNumberValue>-->
			<QueryHitsUnlimitedIndicator>true</QueryHitsUnlimitedIndicator>
		</ProcessingConditions>
	</n0:CustomerByCommunicationDataQuery_sync>
	</soapenv:Body>
	</soapenv:Envelope>`

	request({
		method: 'POST',
		uri: 'https://my355904.sapbydesign.com/sap/bc/srt/scs/sap/querycustomerin1',
		auth: {
			'user': '_AOS0001',
			'pass': '@dd0nP@$$',
			'sendImmediately': false
		},
		headers: {
			'Content-Type': 'text/xml'
		},
		body: xml,
	},
		function (error, response, body) {
			if (error) {
				res.send (error)
			} else {
				const result = convert.xml2json(body, {compact: true, spaces: 4});
				res.send (result)
			}
		}
	)})
//get Material/Products from BYD response time 5.26 minutes (10000+ Products)
app.post('/getMaterialBYD', (req, res) => {
	let xml = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global">
	<soapenv:Header />
	<soapenv:Body>
	<n0:MaterialByElementsQuery_sync xmlns:n0="http://sap.com/xi/SAPGlobal20/Global">
		<ProcessingConditions>
			<QueryHitsMaximumNumberValue>20000</QueryHitsMaximumNumberValue>
			<QueryHitsUnlimitedIndicator>false</QueryHitsUnlimitedIndicator>
		</ProcessingConditions>
	</n0:MaterialByElementsQuery_sync>
	</soapenv:Body>
	</soapenv:Envelope>`

	request({
		method: 'POST',
		uri: 'https://my355904.sapbydesign.com/sap/bc/srt/scs/sap/querymaterialin',
		auth: {
			'user': '_AOS0001',
			'pass': '@dd0nP@$$',
			'sendImmediately': false
		},
		headers: {
			'Content-Type': 'text/xml'
		},
		body: xml,
	},
		function (error, response, body) {
			if (error) {
				res.send (error)
			} else {
				const result = convert.xml2json(body, {compact: true, spaces: 4});
				res.send (result)
			}
		}
	)})

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
})