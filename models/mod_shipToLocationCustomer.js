const mongoose = require('mongoose')

const shipToLocationCustomerSchema = new mongoose.Schema({
    shipToPartyId: {
        type: String
    },
    shipToPartyDescription:{
        type: String
    }
})

module.exports = mongoose.model('shipToLocationCustomer', shipToLocationCustomerSchema);