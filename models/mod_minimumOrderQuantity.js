const mongoose = require('mongoose')

const moqSchema = new mongoose.Schema({
    productCategoryId: {
        type: String
    },
    ktiOfASubstrate: {
      type: String  
    },
    ktiOfAItemGtCode: {
        type: String
    },
    ktiOfADtsCode:{
        type: String
    },
    ktiOfAKgPkgConv: {
        type: Number
    },
    ktiOfASluWtConv: {
        type: Number
    },
    minimumOrderQuantity: [
        new mongoose.Schema ({
            Cheeses : { type: Number },
            minimumOrder : { type : Number}
        })]
    });
    
module.exports = mongoose.model('MOQ', moqSchema);