const mongoose = require('mongoose')

const addOnSchema = new mongoose.Schema({
    addOnName: {
        type: String
    },
    addOnUrl: {
        type: String
    }
})

module.exports = mongoose.model('addOn', addOnSchema);