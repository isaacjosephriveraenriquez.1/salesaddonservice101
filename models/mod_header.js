const mongoose = require('mongoose')

const headerSchema = new mongoose.Schema({
    entryNo: {
        type: String
    },
    accountId: {
        type: String
    },
    accountName: {
        type: String
    },
    docStatus: {
        type: String
    },
    address: {
        type: String
    },
    creditLimit: {
        type: String
    },
    creationDate: {
        type: String
    },
    shipToPartyId: {
        type: String
    },
    shipToPartyDescription: {
        type: String
    },
    salesQuoteNo: {
        type: String
    },
    paymentTermsId: {
        type: String
    },
    paymentTerms: {
        type: String
    },
    salesOrderNo: {
        type: String
    },
    currency: {
        type: String
    },
    receivedDate: {
        type: String
    },
    externalReference: {
        type: String
    },
    requestedDate: {
        type: String
    },
    tps: {
        type: String
    },
    deliveryDate: {
        type: String
    },
    customerGroupCode: {
        type: String
    },
    customerGroup: {
        type: String
    },
    deliveryPriorityCode: {
        type: String
    },
    deliveryPriority: {
        type: String
    },
    distributionChannelCode: {
        type: String
    },
    distributionChannel: {
        type: String
    },
    salesGroup: {
        type: String
    },
    salesOrigin: {
        type: String
    },
    completeDelivery: {
        type: String
    },
    mbAccountId: {
        type: String
    },
    customerInformation: {
        type: String
    },
    overTolerance: {
        type: String
    },
    underTolerance: {
        type: String
    },
    internalComment: {
        type: String
    },
    employeeName: {
        type: String
    },
    employeeId: {
        type: String
    },
    deliveryBlockReasonCode: {
        type: String
    },
    deliveryBlockReason: {
        type: String
    },
    invoiceBlockReasonCode: {
        type: String
    },
    invoiceBlockReason: {
        type: String
    },
    validator: {
        type: String
    },
    validationDate: {
        type: String
    },
    cancelReason: {
        type: String
    },
    postingError: {
        type: String
    },
    accountsReceivable: {
        type: String
    },
    totalSalesQuoteAmount: {
        type: String
    },
})

module.exports = mongoose.model('Header', headerSchema);