const mongoose = require('mongoose')

const customerGroupSchema = new mongoose.Schema({
    code: {
        type: String
    },
    description: {
        type: String
    }
})

module.exports = mongoose.model('CustomerGroup', customerGroupSchema);