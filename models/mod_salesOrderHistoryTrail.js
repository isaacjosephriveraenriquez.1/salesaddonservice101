const mongoose = require('mongoose')

const salesOrderHistoryTrailSchema = new mongoose.Schema({
    _referenceId: {
        type: String
    },
    accountId: {
        type: String
    },
    accountName: {
        type: String
    },
    docStatus: {
        type: String
    },
    address: {
        type: String
    },
    creditLimit: {
        type: String
    },
    creationDate: {
        type: Date
    },
    shipToPartyId: {
        type: String
    },
    shipToPartyDescription: {
        type: String
    },
    salesQuoteNo: {
        type: String
    },
    paymentTermsId: {
        type: String
    },
    paymentTerms: {
        type: String
    },
    salesOrderNo: {
        type: String
    },
    currency: {
        type: String
    },
    receivedDate: {
        type: Date
    },
    externalReference:{
        type: String
    },
    requestedDate: {
        type: Date
    },
    tps: {
        type: String
    },
    deliveryDate: {
        type: Date
    },
    customerGroupCode: {
        type: String
    },
    customerGroup: {
        type: String
    },
    deliveryPriorityCode: {
        type: String
    },
    deliveryPriority: {
        type: String
    },
    distributionChannelCode: {
        type: String
    },
    distributionChannel: {
        type: String
    },
    salesGroup: {
        type: String
    },
    salesOrigin: {
        type: String
    },
    completeDelivery: {
        type: String
    },
    mbAccountId: {
        type: String
    },
    customerInformation: {
        type: String
    },
    overTolerance: {
        type: String
    },
    underTolerance: {
        type: String
    },
    internalComment: {
        type: String
    },
    employeeName: {
        type: String
    },
    employeeId: {
        type: String
    },
    deliveryBlockReasonCode: {
        type: String
    },
    deliveryBlockReason: {
        type: String
    },
    invoiceBlockReasonCode: {
        type: String
    },
    invoiceBlockReason: {
        type: String
    },
    validator: {
        type: String
    },
    validationDate: {
        type: String
    },
    cancelReason: {
        type: String
    },
    postingError: {
        type: String
    },
    accountsReceivable: {
        type: String
    },
    totalSalesQuoteAmount: {
        type: String
    },
    items: [],
    dateChange: {
        type: Date
    },
    changeBy: {
        type: String
    }
})

module.exports = mongoose.model('SalesOrderHistoryTrail', salesOrderHistoryTrailSchema);