const mongoose = require('mongoose')

const CustomerSchema = new mongoose.Schema({
    accountId: {
        type: String
    },
    accountName: {
        type: String
    },
    searchName: {
        type: String
    },
    address: {
        type: String
    },
    taxStatus: {
        type: String
    },
    currency: {
        type: String
    },
    paymentTerms: {
        type: String
    },
    employeeResponsible: {
        type: String
    },
    salesGroup: {
        type: String
    },
    customerGroup: {
        type: String
    },
    underTolerance: {
        type: String
    },
    overTolerance: {
        type: String
    },
    mbAccountId: {
        type: String
    }
})

module.exports = mongoose.model('Customer', CustomerSchema);
