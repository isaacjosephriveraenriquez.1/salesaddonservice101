const mongoose = require('mongoose')

const tpsSchema = new mongoose.Schema({
    code: {
        type: String
    },
    vendorAccount: {
        type: String
    }
})

module.exports = mongoose.model('TPS', tpsSchema);