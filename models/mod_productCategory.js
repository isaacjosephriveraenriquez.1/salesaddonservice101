const mongoose = require('mongoose')

const salesCategorySchema = new mongoose.Schema({
    categoryId: {
        type: String
    }
})

module.exports = mongoose.model('salesCategory', salesCategorySchema);