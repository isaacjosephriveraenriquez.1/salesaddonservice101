const mongoose = require('mongoose')

const distributionChannelSchema = new mongoose.Schema({
    code: {
        type: String
    },
    description: {
        type: String
    }
})

module.exports = mongoose.model('DistributionChannel', distributionChannelSchema);