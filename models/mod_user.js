const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    employeeId:{
        type: String
    },
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    userName: {
        type: String
    },
    password: {
        type: String
    },
    isRequestor: {
        type: Boolean
    },
    isApprover: {
        type: Boolean
    },
    isAdmin: {
        type: Boolean
    },
    position: {
        type: String
    },
    addOn: {
        type: String
    },
    isActive: {
        type: Boolean
    },
    mailUsername: {
        type: String
    }
})

module.exports = mongoose.model('user', userSchema);