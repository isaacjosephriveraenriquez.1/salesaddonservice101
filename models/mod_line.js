const mongoose = require('mongoose')

const lineSchema = new mongoose.Schema({
    entryNo: {
        type: String
    },
    productCategoryId: {
        type: String
    },
    configId: {
        type: String
    },
    productId: {
        type: String
    },
    productDescription: {
        type: String
    },
    leadTime: {
        type: String
    },
    partNumber: {
        type: String
    },
    quantity: {
        type: String
    },
    unitOfMeasure: {
        type: String
    },
    listPrice: {
        type: String
    },
    discount: {
        type: String
    },
    netPrice: {
        type: String
    },
    netValue: {
        type: String
    },
    taxAmount: {
        type: String
    },
    totalPerLine: {
        type: String
    }
})

module.exports = mongoose.model('Line', lineSchema);