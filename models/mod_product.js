const mongoose = require('mongoose')

const productsSchema = new mongoose.Schema({
    materialId: {
        type: String
    },
    materialDescription:{
        type: String
    },
    productCategoryId:{
        type: String
    },
    productCategoryDescription:{
        type: String
    },
    purchasingUom:{
        type: String
    },
    sellingUom:{
        type: String
    },
    price: {
        type: String
    },
    saleGroupPrice: [
        new mongoose.Schema ({
            salesgroupName: {type: String},
            salesGroupPrice: {type: Number},
            validityStartdate: {type: Date},
            validityEndDate: { type : Date}
        })
    ], 
    customerSpecificPriceList: [
        new mongoose.Schema ({
        customer : { type: String },
        specificPrice : { type : Number},
        validityStartdate: {type: Date},
        validityEndDate: { type : Date}
    })]
})

module.exports = mongoose.model('products', productsSchema);