const CustomerGroup = require('../models/mod_customerGroup');

/* Add Customer Group to the Database */
module.exports.addCustomerGroup = (reqBody) => {
	let newCustomerGroup = new CustomerGroup ({
		code: reqBody.code,
		description : reqBody.description
	})

	return newCustomerGroup.save().then((createdCustomerGroup, error) => {
		if(error){
			return error
		} else {
			return createdCustomerGroup
		}
	})
}

/* View All Customer Groups from the Database */
module.exports.queryCustomerGroup = (reqParams) => {
	let sort;

	if(reqParams.sort == "Ascending")
	{
		sort = 1;
	} else {
		sort = -1;
	}

	return CustomerGroup.find(reqParams).sort({$natural:`${sort}`}).then(result => {
		return result;
	})
}

/* View Customer Group from the Database */
module.exports.viewCustomerGroup = (reqParams) => {
	return CustomerGroup.find({code: reqParams.code}).then(result => {
		if(Object.keys(result).length < 1) {
			return false
		} else {
			return result
		}
	})
}