const salesOrderHistoryTrail = require('../models/mod_salesOrderHistoryTrail')

//add history
module.exports.addNewHistoryEntry = (reqBody) => {
    let newHistoryTrail = new salesOrderHistoryTrail ({
        _referenceId: reqBody._referenceId,
        accountId: reqBody.accountId,
        accountName: reqBody.accountName,
        docStatus: reqBody.docStatus,
        address: reqBody.address,
        creditLimit: reqBody.creditLimit,
        creationDate: reqBody.creationDate,
        shipToPartyId: reqBody.shipToPartyId,
        shipToPartyDescription: reqBody.shipToPartyDescription,
        salesQuoteNo: reqBody.salesQuoteNo,
        paymentTermsId: reqBody.paymentTermsId,
        paymentTerms: reqBody.paymentTerms,
        salesOrderNo: reqBody.salesOrderNo,
        currency: reqBody.currency,
        receivedDate: reqBody.receivedDate,
        externalReference: reqBody.externalReference,
        requestedDate: reqBody.requestedDate,
        tps: reqBody.tps,
        deliveryDate: reqBody.deliveryDate,
        customerGroupCode: reqBody.customerGroupCode,
        customerGroup: reqBody.customerGroup,
        deliveryPriorityCode: reqBody.deliveryPriorityCode,
        deliveryPriority: reqBody.deliveryPriority,
        distributionChannelCode: reqBody.distributionChannelCode,
        distributionChannel: reqBody.distributionChannel,
        salesGroup: reqBody.salesGroup,
        salesOrigin: reqBody.salesOrigin,
        completeDelivery: reqBody.completeDelivery,
        mbAccountId: reqBody.mbAccountId,
        customerInformation: reqBody.customerInformation,
        overTolerance: reqBody.overTolerance,
        underTolerance: reqBody.underTolerance,
        internalComment: reqBody.internalComment,
        employeeName: reqBody.employeeName,
        employeeId: reqBody.employeeId,
        deliveryBlockReasonCode: reqBody.deliveryBlockReasonCode,
        deliveryBlockReason: reqBody.deliveryBlockReason,
        invoiceBlockReasonCode: reqBody.invoiceBlockReasonCode,
        invoiceBlockReason: reqBody.invoiceBlockReasonCode,
        validator: reqBody.validator,
        validationDate: reqBody.validationDate,
        cancelReason: reqBody.cancelReason,
        postingError: reqBody.postingError,
        accountsReceivable: reqBody.accountsReceivable,
        totalSalesQuoteAmount: reqBody.totalSalesQuoteAmount,
        items: reqBody.items,
        dateChange: reqBody.dateChange,
        changeBy: reqBody.changeBy
    })

    return newHistoryTrail.save().then((addHistory, error) => {
		if(error){
			return error
		} else {
			return addHistory
		}
	})
}

 