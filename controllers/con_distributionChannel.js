const DistributionChannel = require('../models/mod_distributionChannel');

/* Add Distribution Channel to the Database */
module.exports.addDistributionChannel = (reqBody) => {
	let newDistributionChannel = new DistributionChannel ({
		code: reqBody.code,
		description: reqBody.description
	})

	return newDistributionChannel.save().then((createdDistributionChannel, error) => {
		if(error){
			return error
		} else {
			return createdDistributionChannel
		}
	})
}

/* View All Distribution Channels from the Database */
module.exports.viewAllDistributionChannels = () => {
	return DistributionChannel.find().then(result => {
		return result;
	})
}

/* View Distribution Channel from the Database */
module.exports.viewDistributionChannel = (reqParams) => {
	return DistributionChannel.find({code: reqParams.code}).then(result => {
		if(Object.keys(result).length < 1) {
			return false
		} else {
			return result
		}
	})
}