const SalesOrder = require('../models/mod_salesOrder');
const Header = require('../models/mod_header')
const Line = require('../models/mod_line')
const request = require('request');
const parseString = require('xml2js').parseString


/*Create Sales Order*/
module.exports.addSalesOrder = (reqBody) => {

	let newSalesOrder = new SalesOrder({
        accountId: reqBody.accountId,
        accountName: reqBody.accountName,
        docStatus: reqBody.docStatus,
        address: reqBody.address,
        creditLimit: reqBody.creditLimit,
        creationDate: reqBody.creationDate,
        shipToPartyId: reqBody.shipToPartyId,
        shipToPartyDescription: reqBody.shipToPartyDescription,
        salesQuoteNo: reqBody.salesQuoteNo,
        paymentTermsId: reqBody.paymentTermsId,
        paymentTerms: reqBody.paymentTerms,
        salesOrderNo: "",
        currency: reqBody.currency,
        receivedDate:  reqBody.receivedDate,
        externalReference: reqBody.externalReference,
        requestedDate: reqBody.requestedDate,
        tps: reqBody.tps,
        deliveryDate: reqBody.deliveryDate,
        customerGroupCode: reqBody.customerGroupCode,
        customerGroup: reqBody.customerGroup,
        deliveryPriorityCode: reqBody.deliveryPriorityCode,
        deliveryPriority: reqBody.deliveryPriority, 
        distributionChannelCode: reqBody.distributionChannelCode,
        distributionChannel: reqBody.distributionChannel,
        salesGroup: reqBody.salesGroup,
        salesOrigin: reqBody.salesOrigin,
        completeDelivery: reqBody.completeDelivery,
        mbAccountId: reqBody.mbAccountId,
        customerInformation: reqBody.customerInformation,
        overTolerance: reqBody.overTolerance,
        underTolerance: reqBody.underTolerance,
        internalComment: reqBody.internalComment,
        employeeName: reqBody.employeeName,
        employeeId: reqBody.employeeId,
        deliveryBlockReasonCode: "",
        deliveryBlockReason: "",
        invoiceBlockReasonCode: "",
        invoiceBlockReason: "",
        validator: "",
        validationDate: "",
        cancelReason: "",
        postingError: "",
        accountsReceivable: "",
        totalSalesQuoteAmount: reqBody.totalSalesQuoteAmount,
        items: reqBody.items
	})

	return newSalesOrder.save().then((createdSalesOrder, error) => {
		if(error){
			return error
		} else {
			return createdSalesOrder
		}
	})
};

/*Create Sales Quote from Uploaded File */
module.exports.addSalesQuoteFromUpload = async () => {
    let lastSalesQuoteNo

    await SalesOrder.find({}, {_id: 1, salesQuoteNo: 1}).sort({_id: -1}).then(result => {
        if(result.length < 1) {
            return lastSalesQuoteNo = 0
        } else {
            return lastSalesQuoteNo = result[0].salesQuoteNo
        }
    })

    return Header.aggregate([
        { 
            $lookup: {
                from: 'lines',
                localField: 'entryNo',
                foreignField: 'entryNo',
                as: 'items'
            }
        }
    ]).then(result => {
        for(let i = 0; i < result.length; i++) {
            
            let totalAmountOfSalesQuote = 0;
            for(let a = 0; a < result[i].items.length; a++) {
                totalAmountOfSalesQuote += parseFloat(result[i].items[a].totalPerLine)
            }
            
            let newSalesOrder = new SalesOrder({
                accountId: result[i].accountId,
                accountName: result[i].accountName,
                docStatus: result[i].docStatus,
                address: result[i].address,
                creditLimit: result[i].creditLimit,
                creationDate: result[i].creationDate,
                shipToPartyId: result[i].shipToPartyId,
                shipToPartyDescription: result[i].shipToPartyDescription,
                salesQuoteNo: (parseInt(lastSalesQuoteNo) + (i + 1)),
                paymentTermsId: result[i].paymentTermsId,
                paymentTerms: result[i].paymentTerms,
                salesOrderNo: "",
                currency: result[i].currency,
                receivedDate:  result[i].receivedDate,
                externalReference: result[i].externalReference,
                requestedDate: result[i].requestedDate,
                tps: result[i].tps,
                deliveryDate: result[i].deliveryDate,
                customerGroupCode: result[i].customerGroupCode,
                customerGroup: result[i].customerGroup,
                deliveryPriorityCode: result[i].deliveryPriorityCode,
                deliveryPriority: result[i].deliveryPriority, 
                distributionChannelCode: result[i].distributionChannelCode,
                distributionChannel: result[i].distributionChannel,
                salesGroup: result[i].salesGroup,
                salesOrigin: result[i].salesOrigin,
                completeDelivery: result[i].completeDelivery,
                mbAccountId: result[i].mbAccountId,
                customerInformation: result[i].customerInformation,
                overTolerance: result[i].overTolerance,
                underTolerance: result[i].underTolerance,
                internalComment: result[i].internalComment,
                employeeName: result[i].employeeName,
                employeeId: result[i].employeeId,
                deliveryBlockReasonCode: "",
                deliveryBlockReason: "",
                invoiceBlockReasonCode: "",
                invoiceBlockReason: "",
                validator: "",
                validationDate: "",
                cancelReason: "",
                postingError: "",
                accountsReceivable: "",
                totalSalesQuoteAmount: totalAmountOfSalesQuote,
                items: result[i].items
            })

            newSalesOrder.save()
        }
    }).then(() => {
        return Header.deleteMany({}).then(() => {
            Line.deleteMany({}).then(() => {
                return true
            })
        })
    })
}

/*Retrieve All Sales Order in the Database*/
module.exports.getAllSalesOrder = () => {
    return SalesOrder.find().then( result => {
        return result
    })
}

/*Retrieve All Status Sales Order - Requestor*/
module.exports.getAllSalesOrderRequestor = (reqParams) => {
    return SalesOrder.find({employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve All Status Sales Order - Approver*/
module.exports.getAllSalesOrderApprover = () => {
    return SalesOrder.find().then(result => {
        let resultArray = []
        result.forEach(data => {
            if(data.docStatus === 'For Approval') {
                resultArray.push(data)
            } else if(data.docStatus === 'Approved') {
                resultArray.push(data)
            } else if(data.docStatus === 'Rejected') {
                resultArray.push(data)
            } else if(data.docStatus === 'Posting Error') {
                resultArray.push(data)
            }
        })

        return resultArray
    })
}

/*Retrieve All In Preparation Sales Order*/
module.exports.getAllInPreparation = () => {
    return SalesOrder.find({docStatus: 'In Preparation'}).then( result => {
        return result
    })
}

/*Retrieve All For Approval Sales Order*/
module.exports.getAllForApproval = () => {
    return SalesOrder.find({docStatus: 'For Approval'}).then( result => {
        return result
    })
}

/*Retrieve All Posted Sales Order*/
module.exports.getAllPosted = () => {
    return SalesOrder.find({docStatus: 'Posted'}).then( result => {
        return result
    })
}

/*Retrieve All Approved Sales Order*/
module.exports.getAllApproved = () => {
    return SalesOrder.find({docStatus: 'Approved'}).then( result => {
        return result
    })
}

/*Retrieve All Cancelled Sales Order*/
module.exports.getAllCancelled = () => {
    return SalesOrder.find({docStatus: 'Cancelled'}).then( result => {
        return result
    })
}

/*Retrieve All Rejected Sales Order*/
module.exports.getAllRejected = () => {
    return SalesOrder.find({docStatus: 'Rejected'}).then( result => {
        return result
    })
}

/*Retrieve All Posting Error Sales Order*/
module.exports.getAllPostingError = () => {
    return SalesOrder.find({docStatus: 'Posting Error'}).then( result => {
        return result
    })
}

/*Retrieve In Preparation Sales Order - Per Requestor*/
module.exports.inPreparationSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'In Preparation', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve For Approval Sales Order - Per Requestor*/
module.exports.forApprovalSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'For Approval', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Posted Sales Order - Per Requestor*/
module.exports.postedSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'Posted', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Approved Sales Order - Per Requestor*/
module.exports.approvedSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'Approved', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Cancelled Sales Order - Per Requestor*/
module.exports.cancelledSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'Cancelled', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Rejected Sales Order - Per Requestor*/
module.exports.rejectedSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'Rejected', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Posting Error Sales Order - Per Requestor*/
module.exports.postingErrorSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'Posting Error', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Add Accounts Receivable*/
module.exports.addAccountsReceivable = (reqBody, reqParams) => {
    return SalesOrder.findOne({salesQuoteNo: reqParams.salesQuoteNo}).then(result => {
        result.accountsReceivable = reqBody.accountsReceivable

        return result.save().then((updatedSalesOrder, error) => {
            if(error){
                return error
            } else {
                return updatedSalesOrder
            }
        })
    })
} 

/*View Function*/
module.exports.getSalesOrder = (reqParams) => {
    return SalesOrder.findOne({salesQuoteNo: reqParams.salesQuoteNo}).then(result => {
        return result
    })
} 

/*View Status Function*/
module.exports.getSalesOrderStatus = (reqParams) => {
    return SalesOrder.findOne({salesQuoteNo: reqParams.salesQuoteNo}, {docStatus: 1, _id: 0}).then(result => {
        return result
    })
} 

/*Edit Function*/
module.exports.editSalesOrder = (reqBody, reqParams) => {
    return SalesOrder.findOne({salesQuoteNo: reqParams.salesQuoteNo}).then(result => {
        result.accountId = reqBody.accountId
        result.accountName = reqBody.accountName
        result.address = reqBody.address
        result.shipToPartyId = reqBody.shipToPartyId
        result.shipToPartyDescription = reqBody.shipToPartyDescription
        result.paymentTermsId = reqBody.paymentTermsId
        result.paymentTerms = reqBody.paymentTerms
        result.currency = reqBody.currency
        result.receivedDate = reqBody.receivedDate
        result.externalReference = reqBody.externalReference
        result.requestedDate = reqBody.requestedDate
        result.tps = reqBody.tps
        result.deliveryDate = reqBody.deliveryDate
        result.customerGroup = reqBody.customerGroup
        result.customerGroupCode = reqBody.customerGroupCode
        result.deliveryPriority = reqBody.deliveryPriority
        result.deliveryPriorityCode = reqBody.deliveryPriorityCode
        result.distributionChannel = reqBody.distributionChannel
        result.distributionChannelCode = reqBody.distributionChannelCode
        result.salesGroup = reqBody.salesGroup
        result.salesOrigin = reqBody.salesOrigin
        result.completeDelivery = reqBody.completeDelivery
        result.mbAccountId = reqBody.mbAccountId
        result.customerInformation = reqBody.customerInformation
        result.overTolerance = reqBody.overTolerance
        result.underTolerance = reqBody.underTolerance
        result.internalComment = reqBody.internalComment
        result.items = reqBody.items

        return result.save().then((updatedResult, error) => {
            if(error){
                return error
            } else {
                return updatedResult
            }
        })
    })
}

/*Post Function*/
module.exports.postSalesOrder = (reqParams, reqBody) => {
    return SalesOrder.findOne({salesQuoteNo: reqParams.salesQuoteNo}).then(result => {
        let year = result.requestedDate.slice(6,10)
        let month = result.requestedDate.slice(0,2)
        let day = result.requestedDate.slice(3,5)
        let updatedDateTime = `${year}-${month}-${day}T12:00:00.1234567Z`
        
        let xmlItems = ''
        for(let i = 0; i < result.items.length; i++) { 
            xmlItems += `
            <Item>
                <ID>${i+1}</ID>
                <ItemProduct>
                    <ProductInternalID>${result.items[i].productId.replace(/-/g, "/")}</ProductInternalID>
                </ItemProduct>

                <ItemScheduleLine>
                    <Quantity>${result.items[i].quantity}</Quantity>
                </ItemScheduleLine>

                <a47:CustomerExternalCode>${result.items[i].partNumber}</a47:CustomerExternalCode>

                <PriceAndTaxCalculationItem>
                    <CountryCode>PH</CountryCode>
                    <ItemMainDiscount>
                        <Rate>
                            <DecimalValue>${(result.items[i].discount !== '') ? result.items[i].discount : 0}</DecimalValue>
                            <CurrencyCode>PHP</CurrencyCode>
                            <BaseDecimalValue>1</BaseDecimalValue>
                        </Rate>
                    </ItemMainDiscount>
                    <ItemMainPrice>
                        <Rate>
                            <DecimalValue>${(result.items[i].listPrice !== '') ? result.items[i].listPrice : 0}</DecimalValue>
                            <CurrencyCode>PHP</CurrencyCode>
                            <BaseDecimalValue>1</BaseDecimalValue>
                        </Rate>
                    </ItemMainPrice>
                </PriceAndTaxCalculationItem>
            </Item>
            `
        }
        
        let xml = `
        <?xml version="1.0" encoding="utf-8"?>
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global" xmlns:a47="http://sap.com/xi/AP/CustomerExtension/BYD/A475U">
            <soapenv:Header/>
            <soapenv:Body>
                <n0:SalesOrderBundleMaintainRequest_sync xmlns:n0="http://sap.com/xi/SAPGlobal20/Global">
                    <SalesOrder>
                        <BuyerID>${result.externalReference}</BuyerID>

                        <AccountParty>
                            <PartyID>${result.accountId}</PartyID>
                        </AccountParty>

                        <BillToParty>
                            <PartyID>${result.accountId}</PartyID>
                        </BillToParty>

                        <EmployeeResponsibleParty>
                            <PartyID>${result.employeeId}</PartyID>
                        </EmployeeResponsibleParty>

                        <SalesAndServiceBusinessArea actionCode="01">
                            <DistributionChannelCode>${result.distributionChannelCode}</DistributionChannelCode>
                        </SalesAndServiceBusinessArea>

                        <RequestedFulfillmentPeriodPeriodTerms> 
                            <StartDateTime timeZoneCode="UTC">${updatedDateTime}</StartDateTime> 
                        </RequestedFulfillmentPeriodPeriodTerms>

                        <TextCollection>

                            <Text>

                                <TypeCode>10011</TypeCode>

                                <ContentText>${(result.internalComment !== '') ? result.internalComment : ' '}</ContentText>

                            </Text>



                            <Text>

                                <TypeCode>10024</TypeCode>

                                <ContentText>${(result.customerInformation !== '') ? result.customerInformation : ' '}</ContentText>

                            </Text>

                        </TextCollection>

                        <DataOriginTypeCode>4</DataOriginTypeCode>
                        
                        <FulfillmentBlockingReasonCode>${(result.deliveryBlockReasonCode !== '') ? result.deliveryBlockReasonCode : ` `}</FulfillmentBlockingReasonCode>

                        <InvoiceTerms>
                            <InvoicingBlockingReasonCode>${(result.invoiceBlockReasonCode !== '') ? result.invoiceBlockReasonCode : ` `}</InvoicingBlockingReasonCode>
                        </InvoiceTerms>

                        <DeliveryTerms>
                            <DeliveryPriorityCode>${result.deliveryPriorityCode}</DeliveryPriorityCode>
                            <CompleteDeliveryRequestedIndicator>${result.completeDelivery}</CompleteDeliveryRequestedIndicator>
                        </DeliveryTerms>

                        <a47:TPS>${result.tps}</a47:TPS>

                        <a47:CustomerGroup>${result.customerGroup}</a47:CustomerGroup>

                        <a47:SalesGroup1>${result.salesGroup}</a47:SalesGroup1>

                        <a47:SalesOrigin>${result.salesOrigin}</a47:SalesOrigin>

                        ${xmlItems}
                        
                    </SalesOrder>
                </n0:SalesOrderBundleMaintainRequest_sync>
            </soapenv:Body>
        </soapenv:Envelope>
        `

        request({
            method: 'POST',
            uri: 'https://my355904.sapbydesign.com/sap/bc/srt/scs/sap/managesalesorderin5',
            auth: {
                'user': '_AOS0001',
                'pass': '@dd0nP@$$',
                'sendImmediately': false
            },
            headers: {
                'Content-Type': 'text/xml'
                },
            body: xml,
        }, (error, response, body) => {
            if(error) {
                return error
            } else {
                let soid = ''
                let errorLogs = ''
                parseString(body, (error, data) => {
                    if(data['soap-env:Envelope']['soap-env:Body']['0'].hasOwnProperty('soap-env:Fault')) {
                        errorLogs = data['soap-env:Envelope']['soap-env:Body']['0']['soap-env:Fault']['0']['faultstring']['0']['_']
                    } else {
                        if(data['soap-env:Envelope']['soap-env:Body']['0']['n0:SalesOrderBundleMaintainConfirmation_sync']['0'].hasOwnProperty('SalesOrder')) {
                            soid = data['soap-env:Envelope']['soap-env:Body']['0']['n0:SalesOrderBundleMaintainConfirmation_sync']['0']['SalesOrder']['0']['ID']['0']
                        } else {
                            errorLogs = data['soap-env:Envelope']['soap-env:Body']['0']['n0:SalesOrderBundleMaintainConfirmation_sync']['0']['Log']['0']['Item']    
                        }
                    }
                })
                
                if(soid.length > 0) {
                    result.salesOrderNo = soid
                    result.docStatus = 'Posted'
                    result.validator = reqBody.validator
                }

                if(errorLogs.length > 0) {
                    result.salesOrderNo = ''
                    result.docStatus = 'Posting Error'
                    result.postingError = errorLogs
                }

                let month = new Date().toLocaleString('default', { month: 'long' })

                if(month === 'January'){
                    month = '01'
                } else if(month === 'February') {
                    month = '02'
                } else if(month === 'March') {
                    month = '03'
                } else if(month === 'April') {
                    month = '04'
                } else if(month === 'May') {
                    month = '05'
                } else if(month === 'June') {
                    month = '06'
                } else if(month === 'July') {
                    month = '07'
                } else if(month === 'August') {
                    month = '08'
                } else if(month === 'September') {
                    month = '09'
                } else if(month === 'October') {
                    month = '10'
                } else if(month === 'November') {
                    month = '11'
                } else if(month === 'December') {
                    month = '12'
                }

                let day = new Date().getDate();
                let year = new Date().getFullYear()
                let time = new Date().toLocaleTimeString()

                if(time.length === 10) {
                    time = `0${time}`
                } else {
                    time = time
                }

                if(day.toString().length === 1) {
                    day = `0${day}`
                }
                
                result.validationDate = `${month}/${day}/${year} - ${time}`

                result.save().then((updatedResult, error) => {
                    if(error) {
                        return false
                    } else {
                        return true
                    }
                })
            }
        })
    })
}

/*Approve Function*/
module.exports.approveSalesOrder = (reqParams, reqBody) => {
    return SalesOrder.findOne({salesQuoteNo: reqParams.salesQuoteNo}).then(result => {
        let year = result.requestedDate.slice(6,10)
        let month = result.requestedDate.slice(0,2)
        let day = result.requestedDate.slice(3,5)
        let updatedDateTime = `${year}-${month}-${day}T12:00:00.1234567Z`
        
        let xmlItems = ''
        for(let i = 0; i < result.items.length; i++) { 
            xmlItems += `
            <Item>
                <ID>${i+1}</ID>
                <ItemProduct>
                    <ProductInternalID>${result.items[i].productId.replace(/-/g, "/")}</ProductInternalID>
                </ItemProduct>

                <ItemScheduleLine>
                    <Quantity>${result.items[i].quantity}</Quantity>
                </ItemScheduleLine>

                <a47:CustomerExternalCode>${result.items[i].partNumber}</a47:CustomerExternalCode>

                <PriceAndTaxCalculationItem>
                    <CountryCode>PH</CountryCode>
                    <ItemMainDiscount>
                        <Rate>
                            <DecimalValue>${(result.items[i].discount !== '') ? result.items[i].discount : 0}</DecimalValue>
                            <CurrencyCode>PHP</CurrencyCode>
                            <BaseDecimalValue>1</BaseDecimalValue>
                        </Rate>
                    </ItemMainDiscount>
                    <ItemMainPrice>
                        <Rate>
                            <DecimalValue>${(result.items[i].listPrice !== '') ? result.items[i].listPrice : 0}</DecimalValue>
                            <CurrencyCode>PHP</CurrencyCode>
                            <BaseDecimalValue>1</BaseDecimalValue>
                        </Rate>
                    </ItemMainPrice>
                </PriceAndTaxCalculationItem>
            </Item>
            `
        }
        
        let xml = `
        <?xml version="1.0" encoding="utf-8"?>
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global" xmlns:a47="http://sap.com/xi/AP/CustomerExtension/BYD/A475U">
            <soapenv:Header/>
            <soapenv:Body>
                <n0:SalesOrderBundleMaintainRequest_sync xmlns:n0="http://sap.com/xi/SAPGlobal20/Global">
                    <SalesOrder>
                        <BuyerID>${result.externalReference}</BuyerID>

                        <AccountParty>
                            <PartyID>${result.accountId}</PartyID>
                        </AccountParty>

                        <BillToParty>
                            <PartyID>${result.accountId}</PartyID>
                        </BillToParty>

                        <EmployeeResponsibleParty>
                            <PartyID>${result.employeeId}</PartyID>
                        </EmployeeResponsibleParty>

                        <SalesAndServiceBusinessArea actionCode="01">
                            <DistributionChannelCode>${result.distributionChannelCode}</DistributionChannelCode>
                        </SalesAndServiceBusinessArea>

                        <RequestedFulfillmentPeriodPeriodTerms> 
                            <StartDateTime timeZoneCode="UTC">${updatedDateTime}</StartDateTime> 
                        </RequestedFulfillmentPeriodPeriodTerms>

                        <TextCollection> 
                            <Text>
                                <TypeCode>10011</TypeCode>
                                <ContentText>${result.internalComment ? result.internalComment : ' '}</ContentText> 
                            </Text> 

                            <Text>
                                <TypeCode>10024</TypeCode>
                                <ContentText>${result.customerInformation ? result.customerInformation : ' '}</ContentText>
                            </Text>
                        </TextCollection>
                        
                        <DataOriginTypeCode>4</DataOriginTypeCode>

                        <FulfillmentBlockingReasonCode>${(result.deliveryBlockReasonCode !== '') ? result.deliveryBlockReasonCode : ` `}</FulfillmentBlockingReasonCode>

                        <InvoiceTerms>
                            <InvoicingBlockingReasonCode>${(result.invoiceBlockReasonCode !== '') ? result.invoiceBlockReasonCode : ` `}</InvoicingBlockingReasonCode>
                        </InvoiceTerms>

                        <DeliveryTerms>
                            <DeliveryPriorityCode>${result.deliveryPriorityCode}</DeliveryPriorityCode>
                            <CompleteDeliveryRequestedIndicator>${result.completeDelivery}</CompleteDeliveryRequestedIndicator>
                        </DeliveryTerms>

                        <a47:TPS>${result.tps}</a47:TPS>

                        <a47:CustomerGroup>${result.customerGroup}</a47:CustomerGroup>

                        <a47:SalesGroup1>${result.salesGroup}</a47:SalesGroup1>

                        <a47:SalesOrigin>${result.salesOrigin}</a47:SalesOrigin>

                        ${xmlItems}
                        
                    </SalesOrder>
                </n0:SalesOrderBundleMaintainRequest_sync>
            </soapenv:Body>
        </soapenv:Envelope>
        `

        request({
            method: 'POST',
            uri: 'https://my355904.sapbydesign.com/sap/bc/srt/scs/sap/managesalesorderin5',
            auth: {
                'user': '_AOS0001',
                'pass': '@dd0nP@$$',
                'sendImmediately': false
            },
            headers: {
                'Content-Type': 'text/xml'
                },
            body: xml,
        }, (error, response, body) => {
            if(error) {
                return error
            } else {
                let soid = ''
                let errorLogs = ''
                parseString(body, (error, data) => {
                    if(data['soap-env:Envelope']['soap-env:Body']['0'].hasOwnProperty('soap-env:Fault')) {
                        errorLogs = data['soap-env:Envelope']['soap-env:Body']['0']['soap-env:Fault']['0']['faultstring']['0']['_']
                    } else {
                        if(data['soap-env:Envelope']['soap-env:Body']['0']['n0:SalesOrderBundleMaintainConfirmation_sync']['0'].hasOwnProperty('SalesOrder')) {
                            soid = data['soap-env:Envelope']['soap-env:Body']['0']['n0:SalesOrderBundleMaintainConfirmation_sync']['0']['SalesOrder']['0']['ID']['0']
                        } else {
                            errorLogs = data['soap-env:Envelope']['soap-env:Body']['0']['n0:SalesOrderBundleMaintainConfirmation_sync']['0']['Log']['0']['Item']    
                        }
                    }
                })

                if(soid.length > 0) {
                    result.salesOrderNo = soid
                    result.docStatus = 'Approved'
                    result.validator = reqBody.validator
                }

                if(errorLogs.length > 0) {
                    result.salesOrderNo = ''
                    result.docStatus = 'Posting Error'
                    result.postingError = errorLogs
                }

                let month = new Date().toLocaleString('default', { month: 'long' })

                if(month === 'January'){
                    month = '01'
                } else if(month === 'February') {
                    month = '02'
                } else if(month === 'March') {
                    month = '03'
                } else if(month === 'April') {
                    month = '04'
                } else if(month === 'May') {
                    month = '05'
                } else if(month === 'June') {
                    month = '06'
                } else if(month === 'July') {
                    month = '07'
                } else if(month === 'August') {
                    month = '08'
                } else if(month === 'September') {
                    month = '09'
                } else if(month === 'October') {
                    month = '10'
                } else if(month === 'November') {
                    month = '11'
                } else if(month === 'December') {
                    month = '12'
                }

                let day = new Date().getDate();
                let year = new Date().getFullYear()
                let time = new Date().toLocaleTimeString()

                if(time.length === 10) {
                    time = `0${time}`
                } else {
                    time = time
                }

                if(day.toString().length === 1) {
                    day = `0${day}`
                }
                
                result.validationDate = `${month}/${day}/${year} - ${time}`

                result.save().then((updatedResult, error) => {
                    if(error) {
                        return false
                    } else {
                        return true
                    }
                })
            }
        })
    })
}

/*Reject Function*/
module.exports.rejectSalesOrder = (reqParams) => {
    return SalesOrder.findOne({salesQuoteNo: reqParams.salesQuoteNo}).then(result => {
        result.docStatus = 'Rejected'

        return result.save().then((updatedResult, error) => {
            if(error) {
                return error
            } else {
                return updatedResult
            }
        })
    }
)}
    
/*Cancel Function*/
module.exports.cancelSalesOrder = (reqBody, reqParams) => {
    return SalesOrder.findOne({salesQuoteNo: reqParams.salesQuoteNo}).then(result => {
        result.docStatus = 'Cancelled'
        result.validator = reqBody.validator
        result.cancelReason = reqBody.cancelReason

        let month = new Date().toLocaleString('default', { month: 'long' })

        if(month === 'January'){
            month = '01'
        } else if(month === 'February') {
            month = '02'
        } else if(month === 'March') {
            month = '03'
        } else if(month === 'April') {
            month = '04'
        } else if(month === 'May') {
            month = '05'
        } else if(month === 'June') {
            month = '06'
        } else if(month === 'July') {
            month = '07'
        } else if(month === 'August') {
            month = '08'
        } else if(month === 'September') {
            month = '09'
        } else if(month === 'October') {
            month = '10'
        } else if(month === 'November') {
            month = '11'
        } else if(month === 'December') {
            month = '12'
        }

        let day = new Date().getDate();
        let year = new Date().getFullYear()
        let time = new Date().toLocaleTimeString()

        if(time.length === 10) {
            time = `0${time}`
        } else {
            time = time
        }

        if(day.toString().length === 1) {
            day = `0${day}`
        }
        
        result.validationDate = `${month}/${day}/${year} - ${time}`
    
        return result.save().then((updatedResult, error) => {
            if(error) {
                return error
            } else {
                return updatedResult
            }
        })
    })
}
//External Reference
module.exports.externalReference = (params) => {
    return SalesOrder.find({accountName: params.accountName, externalReference: params.externalReference}).then(result => {

        console.log(params)
        console.log(result)

        if(result.length != 0){
            return false
        } else {
            return true
        }
    })
}

/*Set Delivery Block Function*/
module.exports.setDeliveryBlock = (reqBody, reqParams) => {
    return SalesOrder.findOne({salesQuoteNo: reqParams.salesQuoteNo}).then(result => {
        result.deliveryBlockReasonCode = reqBody.deliveryBlockReasonCode
        result.deliveryBlockReason = reqBody.deliveryBlockReason
        result.docStatus = reqBody.docStatus
    
        return result.save().then((updatedResult, error) => {
            if(error) {
                return error
            } else {
                return updatedResult
            }
        })
    })
}

/*Set Invoice Block Function*/
module.exports.setInvoiceBlock = (reqBody, reqParams) => {
    return SalesOrder.findOne({salesQuoteNo: reqParams.salesQuoteNo}).then(result => {
        result.invoiceBlockReasonCode = reqBody.invoiceBlockReasonCode
        result.invoiceBlockReason = reqBody.invoiceBlockReason
    
        return result.save().then((updatedResult, error) => {
            if(error) {
                return error
            } else {
                return updatedResult
            }
        })
    })
}

/*For Credit Limit*/
module.exports.getAllSpecificCustomer = async (reqParams) => {
    return await SalesOrder.find({accountId: reqParams.accountId}).then(result =>{
        let selectedArray = []
        for(let i = 0; i < result.length; i++) {
            if(result[i].docStatus === 'In Preparation') {
                selectedArray.push(result[i])
            } else if(result[i].docStatus === 'For Approval') {
                selectedArray.push(result[i])
            }
        }
        let allItemsArray = []
        for(let x = 0; x < selectedArray.length; x++) {
            for(let y = 0; y < selectedArray[x].items.length; y++) {
                allItemsArray.push(selectedArray[x].items[y])
            }
        }

        return allItemsArray
    })
}

/* Sort Sales Order in Ascending Order */
module.exports.sortSalesOrderAscending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({salesOrderNo: 1}).then(result => {
        let postedSalesOrder = []
        let unpostedSalesOrder = []

        for(let i = 0; i < result.length; i++) {
            if(result[i].salesOrderNo === "") {
                unpostedSalesOrder.push(result[i])
            } else {
                postedSalesOrder.push(result[i])
            }
        }

        postedSalesOrder.sort((a, b) => {
            return parseInt(a.salesOrderNo) - parseInt(b.salesOrderNo)
        })

        unpostedSalesOrder.sort((a, b) => {
            return parseInt(b.salesOrderNo) - parseInt(a.salesOrderNo)
        })

        return postedSalesOrder.concat(unpostedSalesOrder)
    })
}

/* Sort Sales Order in Descending Order */
module.exports.sortSalesOrderDescending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({salesOrderNo: -1}).then(result => {
        let postedSalesOrder = []
        let unpostedSalesOrder = []

        for(let i = 0; i < result.length; i++) {
            if(result[i].salesOrderNo === "") {
                unpostedSalesOrder.push(result[i])
            } else {
                postedSalesOrder.push(result[i])
            }
        }

        postedSalesOrder.sort((a, b) => {
            return parseInt(b.salesOrderNo) - parseInt(a.salesOrderNo)
        })

        unpostedSalesOrder.sort((a, b) => {
            return parseInt(b.salesOrderNo) - parseInt(a.salesOrderNo)
        })

        return postedSalesOrder.concat(unpostedSalesOrder)
    })
}

/* Sort Sales Quote in Ascending Order */
module.exports.sortSalesQuoteAscending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).then(result => {
        result.sort((a, b) => {
            return parseInt(a.salesQuoteNo) - parseInt(b.salesQuoteNo)
        })
        return result
    })
}

/* Sort Status in Ascending Order */
module.exports.sortStatusAscending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({docStatus: 1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Status in Descending Order */
module.exports.sortStatusDescending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({docStatus: -1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Account ID in Ascending Order */
module.exports.sortAccountIdAscending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({accountId: 1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Account ID in Descending Order */
module.exports.sortAccountIdDescending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({accountId: -1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Account Name in Ascending Order */
module.exports.sortAccountNameAscending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({accountName: 1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Account Name in Descending Order */
module.exports.sortAccountNameDescending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({accountName: -1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Sales Group in Ascending Order */
module.exports.sortSalesGroupAscending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({salesGroup: 1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Sales Group in Descending Order */
module.exports.sortSalesGroupDescending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({salesGroup: -1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Customer Group in Ascending Order */
module.exports.sortCustomerGroupAscending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({customerGroup: 1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Customer Group in Descending Order */
module.exports.sortCustomerGroupDescending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({customerGroup: -1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Reference in Ascending Order */
module.exports.sortReferenceAscending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({externalReference: 1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Reference in Descending Order */
module.exports.sortReferenceDescending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({externalReference: -1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Amount in Ascending Order */
module.exports.sortAmountAscending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).then(result => {
        for(let i = 0; i < result.length; i++) {
            result[i].totalSalesQuoteAmount = parseFloat(result[i].totalSalesQuoteAmount.replace(/,/g, "")).toFixed(2)
        }

        result.sort((a, b) => {
            return a.totalSalesQuoteAmount - b.totalSalesQuoteAmount
        })

        return result
    })
}

/* Sort Amount in Descending Order */
module.exports.sortAmountDescending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1,
    }).then(result => {
        for(let i = 0; i < result.length; i++) {
            result[i].totalSalesQuoteAmount = parseFloat(result[i].totalSalesQuoteAmount.replace(/,/g, "")).toFixed(2)
        }

        result.sort((a, b) => {
            return b.totalSalesQuoteAmount - a.totalSalesQuoteAmount
        })

        return result
    })
}

/* Sort Validator in Ascending Order */
module.exports.sortValidatorAscending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({validator: 1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Validator in Descending Order */
module.exports.sortValidatorDescending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({validator: -1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Delivery Block in Ascending Order */
module.exports.sortDeliveryBlockAscending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({deliveryBlockReason: 1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Delivery Block in Descending Order */
module.exports.sortDeliveryBlockDescending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({deliveryBlockReason: -1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Cancel Reason in Ascending Order */
module.exports.sortCancelReasonAscending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({cancelReason: 1, salesQuoteNo: -1}).then(result => {
        return result
    })
}

/* Sort Cancel Reason in Descending Order */
module.exports.sortCancelReasonDescending = () => {
    return SalesOrder.find({}, {
        salesOrderNo: 1,
        salesQuoteNo: 1,
        docStatus: 1,
        accountId: 1,
        accountName: 1,
        creditLimit: 1,
        accountsReceivable: 1,
        salesGroup: 1,
        customerGroup: 1,
        externalReference: 1,
        totalSalesQuoteAmount: 1,
        creationDate: 1,
        employeeName: 1,
        validator: 1,
        validationDate: 1,
        deliveryBlockReason: 1,
        cancelReason: 1
    }).sort({cancelReason: -1, salesQuoteNo: -1}).then(result => {
        return result
    })
}