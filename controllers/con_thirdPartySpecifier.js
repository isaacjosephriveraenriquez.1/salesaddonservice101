const thirdPartySpecifier = require('../models/mod_thirdPartySpecifier');

// Add a TPS to the database
module.exports.addThirdPartySpecifier = (reqBody) => {
	let newThirdPartySpecifier = new thirdPartySpecifier ({
		code: reqBody.code,
		vendorAccount : reqBody.vendorAccount
	})

	return newThirdPartySpecifier.save().then((createdThirdPartySpecifer, error) => {
		if(error){
			return error
		} else {
			return createdThirdPartySpecifer
		}
	})
}

// View All TPS from the database
// Search Specific TPS using QUeryString
module.exports.queryThirdPartySpecifer = (reqParams) => {
	let sort;

	if (reqParams.sort == "Ascending"){
		sort = 1;
	} else {
		sort = -1;
	}

	return thirdPartySpecifier.find(reqParams).sort({$natural:`${sort}`}).then(result => {
		return result;
	})
}

//delete TPS
//change to id
module.exports.deleteThirdPartySpecifier = (reqBody) => {
	return thirdPartySpecifier.findOneAndDelete({code: reqBody.code}, {vendorAccount: reqBody.vendorAccount}).then(result => {
		if(result == null) {
			return false
		} else {
			return `Deleted ${result}`
		}
	})
}
