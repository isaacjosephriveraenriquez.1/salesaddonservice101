const Uom = require('../models/mod_unitOfMeasure');

// Add a Currency to the database
module.exports.addUom = (reqBody) => {
	let newUom = new Uom ({
		code: reqBody.code,
		description : reqBody.description
	})

	return newUom.save().then((createdUOM, error) => {
		if(error){
			return error
		} else {
			return createdUOM
		}
	})

}

// View all Currencies from the database
module.exports.viewAllUom = () => {
	return Uom.find().then(result => {
		return result;
	})
}

// View a specific Currency from the database
module.exports.viewUom = (reqParams) => {
	return Uom.find({code: reqParams.code}).then(result => {
		if(Object.keys(result).length < 1) {
			return false
		} else {
			return result
		}
	})
}
