const Category = require('../models/mod_productCategory');

/* Add a Category to the Database */
module.exports.addCategory = (reqBody) => {
	let newCategory = new Category ({
		categoryId: reqBody.categoryId
	})

	return newCategory.save().then((createdCategory, error) => {
		if(error){
			return error
		} else {
			return createdCategory
		}
	})
}

/* View all Categories from the Database */
module.exports.viewAllCategories = () => {
	return Category.find().then(result => {
		return result
	})
}

/* View a specific Currency from the Database */
module.exports.viewCategory = (reqParams) => {
	return Category.findOne({categoryId: reqParams.categoryId}).then(result => {
		if(Object.keys(result).length < 1) {
			return false
		} else {
			return result
		}
	})
}
