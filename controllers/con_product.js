const Products = require('../models/mod_product');
const axios = require('axios')
const parseString = require('xml2js').parseString

/* View All Materials from the Database */
module.exports.getAllProducts = () => {
    return Products.find().then( result => {
        return result
    })
}

/* View Specific Material from the Database */
module.exports.querySpecificProduct = (reqParams) => {
    let sort;

	if (reqParams.sort == "Ascending"){
		sort = 1;
	} else {
		sort = -1;
	}

	return Products.findOne({materialId: reqParams.materialId}).sort({$natural:`${sort}`}).then(result => {
		return result;
	})
}

/* View All Materials with the Product Category Indicated */
module.exports.getAllProductsByProductCategory = (reqParams) => {
	return Products.find({productCategoryId: reqParams.productCategoryId}).then(result => {
		return result;
	})
}

/* View All Materials with the Product Category Indicated */
module.exports.rapidEntry = (reqParams) => {
	return Products.find({productCategoryId: reqParams.productCategoryId}, {materialId: 1}).then(async (result) => {
        let returnedArray = []
		for(let i = 0; i < result.length; i++) {
            
            let xml = `
            <?xml version="1.0" encoding="utf-8"?>
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global">
                <soapenv:Header/>
                <soapenv:Body>
                    <n0:ProductAvailabilityDeterminationQuery_sync xmlns:n0="http://sap.com/xi/SAPGlobal20/Global">
                        <ProductAvailabilityDeterminationQuery>
                            <ConsiderScopeOfCheckIndicator>false</ConsiderScopeOfCheckIndicator>
                            <ProductAndSupplyPlanningArea>
                                <ProductInternalID>${result[i].materialId.replace(/-/g, "/")}</ProductInternalID>
                                <ProductTypeCode>1</ProductTypeCode>
                                <SupplyPlanningAreaID>M001000111</SupplyPlanningAreaID>
                            </ProductAndSupplyPlanningArea>
                            <ProductAvailabilityDeterminationHorizonDuration>P1D</ProductAvailabilityDeterminationHorizonDuration>
                        </ProductAvailabilityDeterminationQuery>
                    </n0:ProductAvailabilityDeterminationQuery_sync>
                </soapenv:Body>
            </soapenv:Envelope>
            `

            await axios.post('https://my355904.sapbydesign.com/sap/bc/srt/scs/sap/productavailabilitydeterminati', xml, {
                headers: {
                    'Content-Type': 'text/xml'
                },
                auth: {
                    username: '_AOS0001',
                    password: '@dd0nP@$$'
                }
            }).then(res => {
                parseString(res.data, (error, result) => {
                    if(result['soap-env:Envelope']['soap-env:Body']['0'].hasOwnProperty("soap-env:Fault")) {
                        let data = result['soap-env:Envelope']['soap-env:Body']['0']['soap-env:Fault']['0']['faultstring']['0']
                        console.log(data);
                    } else {
                        if(result['soap-env:Envelope']['soap-env:Body']['0'].hasOwnProperty("n0:ProductAvailabilityDeterminationResponse_sync")) {
                            let dataPair = {
                                product: result['soap-env:Envelope']['soap-env:Body']['0']['n0:ProductAvailabilityDeterminationResponse_sync']['0']['ProductAvailabilityDeterminationResponse']['0']['ProductInternalID']['0'],
                                stock: result['soap-env:Envelope']['soap-env:Body']['0']['n0:ProductAvailabilityDeterminationResponse_sync']['0']['ProductAvailabilityDeterminationResponse']['0']['AvailableQuantity']['0']['_']
                            }
                            returnedArray.push(dataPair)
                        }
                    }
                })
            })
        }

        return returnedArray
	})
}
//add new Product
module.exports.addNewProduct = (reqBody) => {
    let newProduct = new Products ({
        materialId: reqBody.materialId,
        materialDescription: reqBody.materialDescription,
        productCategoryId: reqBody.productCategoryId,
        productCategoryDescription: reqBody.productCategoryDescription,
        purchasingUom: reqBody.purchasingUom,
        sellingUom: reqBody.sellingUom,
        price: reqBody.price,
        customerSpecificPriceList: reqBody.customerSpecificPriceList 
    })

    return newProduct.save().then((createdProduct, error) => {
		if(error){
			return error
		} else {
			return createdProduct
		}
	})
}
/* Insert Customer Specific Price */ // To Test Ned Point
module.exports.addCustomerSpecificPrice = (reqBody) => {
	return Products.findByIdAndUpdate({materialId: reqBody.materialId}).then(result => {
        result.customerSpecificPriceList = reqBody.customerSpecificPriceList

        return result.save().then((updatePricelist, error) => {
			if(error){
				return error
			} else {
				return updatePricelist
			}
        })
    })}
     
    /* Delete Customer Specific Price */ // To Test Ned Point
module.exports.deleteCustomerSpecificPrice = (reqBody) => {
	return Products.deleteOne({materialId: reqBody.materialId}).then(result => {
        return result
        })
    }
	