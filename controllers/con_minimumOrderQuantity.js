const minimumOrderQuantity = require('../models/mod_minimumOrderQuantity');

// Add a MOQ to the database
module.exports.addMinimumOrderQuantity = (reqBody) => {
let newMinimumOrderQuantity = new minimumOrderQuantity ({
	productCategoryId: reqBody.productCategoryId,
    ktiOfASubstrate: reqBody.ktiOfASubstrate,
    ktiOfAItemGtCode: reqBody.ktiOfAItemGtCode,
    ktiOfADtsCode: reqBody.ktiOfADtsCode,
    ktiOfAKgPkgConv: reqBody.ktiOfAKgPkgConv,
    ktiOfASluWtConv: reqBody.ktiOfASluWtConv,
    minimumOrderQuantity: reqBody.minimumOrderQuantity
})
return newMinimumOrderQuantity.save().then((createdMinimumOrderQuantity, error) => {
		if(error){
			 return error
		} else {
			return createdMinimumOrderQuantity
		}
	})

}

// View All MOQ from the database
//Add queryString for parameter
//http://localhost:4000/api/moq/view/all
module.exports.queryMinimumOrderQuuantity = () => {
	return minimumOrderQuantity.find().then(result => {
		return result;
	})
}

// View a Specific MOQ from the database
//Parameter Query String Parameter/ Argument productCategory Id and minimum Order
//http://localhost:4000/api/moq/view/specific?productCategoryId=1569M-036DC&minimumOrder=123
module.exports.viewMoq = (reqParams) => {
	return minimumOrderQuantity.findOne({productCategoryId: reqParams.productCategoryId}).then(result => {	
		let moq = result.minimumOrderQuantity.find(minimumOrder => minimumOrder.minimumOrder >=  reqParams.minimumOrder)
		if (moq != null){
			return moq
		} else {
			return "No MOQ Maintained"
		} 
			})
		}


/*Update Moq */
module.exports.updateMoq = (reqBody) => {
	return minimumOrderQuantity.findByIdAndUpdate({productCategoryId: reqBody.productCategoryId}).then(result => {
        result.moq = reqBody.moq

        return result.save().then((updatedMoq, error) => {
			if(error){
				return error
			} else {
				return updatedMoq
			}
        })
    })}

//Push minimum order to Minimum order quantity array using product categoryy id as a query
	module.exports.pushToMinimumOrderQuantity = (reqBody) => {
		console.log(reqBody)
		return minimumOrderQuantity.findOneAndUpdate({productCategoryId: reqBody.productCategoryId}, {$push : { "minimumOrderQuantity" : {"Cheeses": reqBody.Cheeses , minimumOrder : reqBody.minimumOrder}}}).then((result=> {
					return result
		})
		)
	}

//delete moq in minimumorder Array 
	module.exports.pullToMinimumOrderQuantity = (reqBody) => {
		console.log(reqBody)
		return minimumOrderQuantity.findOneAndUpdate({productCategoryId: reqBody.productCategoryId}, {$pull : { "minimumOrderQuantity" : {"Cheeses": reqBody.Cheeses , minimumOrder : reqBody.minimumOrder}}}).then((result => {
				return result
		})
		)
	}
	/*delete Moq */
	module.exports.deleteMoq = (reqBody) => {
		return minimumOrderQuantity.deleteOne({productCategoryId: reqBody.productCategoryId}).then(result => {
			return result
			})
		}