const PaymentTerms = require('../models/mod_paymentTerm');

// Add a Product to the database
module.exports.addTerms = (reqBody) => {
	let newPaymentTerms = new PaymentTerms ({
		paymentTermsId: reqBody.paymentTermsId,
		paymentTermsDescription : reqBody.paymentTermsDescription,
	})

	return newPaymentTerms.save().then((createdPaymentTerms, error) => {
		if(error){
			return error
		} else {
			return createdPaymentTerms
		}
	})
}

// View All Products from the database
module.exports.viewAllTerms = () => {
	return PaymentTerms.find().then(result => {
		return result
	})
}

// View Specific Payment Term by ID from the database
module.exports.viewTerm = (reqParams) => {
	return PaymentTerms.find({paymentTermsId: reqParams.paymentTermsId}).then(result => {
		if(Object.keys(result).length < 1) {
			return false
		} else {
			return result
		}
	})
}