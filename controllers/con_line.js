const Line = require('../models/mod_line')

/*Upload Line*/
module.exports.uploadLine = (reqBody) => {

	let newLine = new Line({
        entryNo: reqBody.entryNo,
        productCategoryId: reqBody.productCategoryId,
        configId: reqBody.configId,
        productId: reqBody.productId,
        productDescription: reqBody.productDescription,
        leadTime: reqBody.leadTime,
        partNumber: reqBody.partNumber,
        quantity: reqBody.quantity,
        unitOfMeasure: reqBody.unitOfMeasure,
        listPrice: reqBody.listPrice,
        discount: reqBody.discount,
        netPrice: reqBody.netPrice,
        netValue: reqBody.netValue,
        taxAmount: reqBody.taxAmount,
        totalPerLine: reqBody.totalPerLine
	})

	return newLine.save().then((createdLine, error) => {
		if(error){
			return error
		} else {
			return createdLine
		}
	})
};

/*Retrieve the Length of the Line Collection*/
module.exports.getAllLine = () => {
    return Line.find({}, {_id: 1}).then(result => {
        return result
    })
};