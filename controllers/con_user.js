const User = require('../models/mod_user')
const auth = require('../auth')
const nodemailer = require('nodemailer');
const bcrypt = require("bcrypt"); 

/* Register User */
module.exports.register = (reqBody) => {
	let newUser = new User({
		employeeId: reqBody.employeeId,
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		userName: reqBody.userName,
		password: bcrypt.hashSync(reqBody.password, 10),
		isRequestor: reqBody.isRequestor,
		isApprover: reqBody.isApprover,
		isAdmin: reqBody.isAdmin,
		position:  reqBody.position,
		addOn: reqBody.addOn,
	})

	return newUser.save().then((createdUser, error) => {
		if(error){
			return error
		} else {
			return createdUser
		}
	})
};

/* Log-in */
module.exports.login = (reqBody) => {
	return User.findOne({userName : reqBody.userName }).then(result => {
		if (result === null || result === undefined) { 
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect) {
				return { accessToken: auth.createAccessToken(result)}
			} else {
				return false
			}	
		}
	})
};

/* Retrieve User Details */
module.exports.getUserDetails = (userData) => {
	return User.findOne({userName: userData.userName}).then(result => {
		return result
	})
}

/* Change Password */
module.exports.changePassword = (reqParams, reqBody) => {
	return User.findOne({userName: reqParams.userName}).then(result => {
		result.password = bcrypt.hashSync(reqBody.password, 10)

		return result.save().then((updatedResult, error) => {
			if(error) {
				return error
			} else {
				return updatedResult
			}
		})
	})
}

/* Validate Specific User from the Database */
module.exports.validateUser = (reqBody) => {
	return User.findOne({userName: reqBody.userName}).then(result => {
		if(result === null) {
			return false
		} else {
			return true
		}
	})
}

/* Retrieve All Users */ 
module.exports.viewAllUsers = () => {
	return User.find().then(result => {
		return result
	})
}

/* Retrieve Specific User */  
module.exports.viewSpecificUser = (reqParams) => {
	return User.findOne({employeeId: reqParams.employeeId}).then(result => {
		return result
	})
}

/* Edit Specific User */
module.exports.editUserAccount = (reqParams, reqBody) => {
	return User.findOne({employeeId: reqParams.employeeId}).then(result => {
		result.employeeId = reqBody.employeeId
		result.isRequestor = reqBody.isRequestor
		result.isApprover = reqBody.isApprover
		result.isAdmin = reqBody.isAdmin
		result.firstName = reqBody.firstName
		result.lastName = reqBody.lastName
		result.userName = reqBody.userName
		result.position	 = reqBody.position

		return result.save().then((updatedUser, error) => {
			if(error){
				return error
			} else {
				return updatedUser
			}
		})
	})
}

/* Enable User Account */
module.exports.enableUserAccount = (reqParams) => {
	return User.findOne({employeeId: reqParams.employeeId}).then(result => {
		result.isActive = true

		return result.save().then((updatedUser, error) => {
			if(error){
				return error
			} else {
				return updatedUser
			}
		})
	})
}

/* Disable User Account */
module.exports.disableUserAccount = (reqParams) => {
	return User.findOne({employeeId: reqParams.employeeId}).then(result => {
		result.isActive = false

		return result.save().then((updatedUser, error) => {
			if(error){
				return error
			} else {
				return updatedUser
			}
		})
	})
}

/* Email Admin Account upon Change Password */
module.exports.emailAdminAccount = (reqParams, reqBody) => {
	return User.findOne({userName: reqParams.userName}).then(result => {
		let employeeName = `${result.firstName} ${result.lastName}`
        let adminName = ''
        let adminEmail = ''

        User.findOne({isAdmin: true, addOn: 'Sales Quote'}).then(result => {
            adminName = `${result.firstName} ${result.lastName}`
            adminEmail = result.mailUsername
        }).then(() => {
            let transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: "manilabaythreadcorporationao@gmail.com",
                    pass: "qtcchhigwgyokxaw"
                }
            })

            let mailOptions = {
                from: "manilabaythreadcorporationao@gmail.com",
                to: `${adminEmail}`,
                subject: `Change Password: ${employeeName}`,
                text: `Hi ${adminName},
I changed my password into: ${reqBody.newPassword} 

Thank you and Best Regards,
${employeeName}
`
            }

            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    return error
                } else {
                    return `${info.response}`
                }
            })
        })  
	})
}