const Currency = require('../models/mod_currency');

// Add a Currency to the database
module.exports.addCurrency = (reqBody) => {
	let newCurrency = new Currency ({
		currencyName: reqBody.currencyName,
		currencyTicker : reqBody.currencyTicker
	})

	return newCurrency.save().then((createdCurrency, error) => {
		if(error){
			return error
		} else {
			return createdCurrency
		}
	})
}

// View all Currencies from the database
module.exports.queryCurrencies = (reqParams) => {
	let sort;

	if(reqParams.sort == "Ascending"){
		sort = 1;
	} else {
		sort = -1;
	}
	return Currency.find(reqParams).sort({$natural:`${sort}`}).then(result => {
		return result;
	})
}

// View a specific Currency from the database
module.exports.viewCurrency = (reqParams) => {
	return Currency.find({currencyTicker: reqParams.currencyTicker}).then(result => {
		if(Object.keys(result).length < 1) {
			return false
		} else {
			return result
		}
	})
}
