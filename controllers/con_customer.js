const Customer = require('../models/mod_customer');

/* Add Customer to the Database */
module.exports.addCustomer = (reqBody) => {
	let newCustomer = new Customer ({
		accountId: reqBody.accountId,
		accountName : reqBody.accountName,
		searchName : reqBody.searchName,
        address: reqBody.address,
		taxStatus: reqBody.taxStatus,
		currency: reqBody.currency,
		paymentTerms: reqBody.paymentTerms,
		employeeResponsible: reqBody.employeeResponsible,
		salesGroup: reqBody.salesGroup,
		customerGroup: reqBody.customerGroup,
		underTolerance: reqBody.underTolerance,
		overTolerance: reqBody.overTolerance,
		mbAccountId: reqBody.mbAccountId
	})

	return newCustomer.save().then((createdCustomer, error) => {
		if(error){
			return error
		} else {
			return createdCustomer
		}
	})
}

/* View All Customers from the Database */
module.exports.viewAllCustomers = () => {
    return Customer.find().then(result => {
        return result
    })
}

/* View Specific Customer by Account Name */
module.exports.viewCustomerByAccountName = (reqParams) => {
	return Customer.findOne({accountName: reqParams.accountName}).then(result => {
		return result;
	})
}

/* View Specific Customer by Account ID */
module.exports.viewCustomerByAccountId = (reqParams) => {
	return Customer.findOne({accountId: reqParams.accountId}).then(result => {
		return result;
	})
}