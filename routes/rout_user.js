const express = require('express');
const router = express.Router();
const auth = require('../auth')
const UserController = require('../controllers/con_user');

/* Register User */
router.post('/register', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
})

/* Log In User */
router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result))
})

/* Retrieve User Details */
router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
    UserController.getUserDetails(userData).then(user => res.send(user))
})

/* Change Password */ 
router.put('/changePassword/:userName', (req, res) => {
    UserController.changePassword(req.params, req.body).then(result => res.send(result))
})

/* Validate Specific Product from the Database */
router.post('/validate', (req, res) => {
	UserController.validateUser(req.body).then(result => res.send(result));
})

/* Retrieve All Users */ 
router.get('/view/all', (req, res) => {
    UserController.viewAllUsers().then(result => res.send(result))
})

/* Retrieve Specific User */ 
router.get('/view/specific/:employeeId', (req, res) => {
    UserController.viewSpecificUser(req.params).then(result => res.send(result))
})

/* Edit Specific User */
router.put('/function/edit/:employeeId', (req, res) => {
    UserController.editUserAccount(req.params, req.body).then(result => res.send(result))
})

/* Enable User Account */
router.put('/function/enable/:employeeId', (req, res) => {
    UserController.enableUserAccount(req.params).then(result => res.send(result))
})

/* Disable User Account */
router.put('/function/disable/:employeeId', (req, res) => {
    UserController.disableUserAccount(req.params).then(result => res.send(result))
})

/* Email Admin Account upon Change Password */
router.post('/function/email/:userName', (req, res) => {
	UserController.emailAdminAccount(req.params, req.body).then(result => res.send(result));
})

module.exports = router;