const express = require('express');
const salesCategoryController = require('../controllers/con_productCategory');
const router = express.Router();

/* Add a Category to the Database */
router.post('/add', (req, res) => {
	salesCategoryController.addCategory(req.body).then(result => res.send(result));
})

/* View all Categories from the Database */
router.get('/view/all', (req, res) => {
	salesCategoryController.viewAllCategories().then(result => res.send(result));
})

/* View a specific Currency from the Database */
router.get('/view/:categoryId', (req, res) => {
	salesCategoryController.viewCategory(req.params).then(result => res.send(result));
})


module.exports = router;