const express = require('express');
const customerGroupController = require('../controllers/con_customerGroup');
const router = express.Router();

/* Add Customer Group to the Database */
router.post('/add', (req, res) => {
	customerGroupController.addCustomerGroup(req.body).then(result => res.send(result));
})

/* View All Customer Groups from the Database */
router.get('/view/all', (req, res) => {
	customerGroupController.queryCustomerGroup(req.query).then(result => res.send(result));
})

/* View Customer Group from the Database */
router.get('/view/specific/:code', (req, res) => {
	customerGroupController.viewCustomerGroup(req.params).then(result => res.send(result));
})


module.exports = router;