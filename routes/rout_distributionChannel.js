const express = require('express');
const distributionChannelController = require('../controllers/con_distributionChannel');
const router = express.Router();

/* Add Distribution Channel to the Database */
router.post('/add', (req, res) => {
	distributionChannelController.addDistributionChannel(req.body).then(result => res.send(result));
})

/* View All Distribution Channels from the Database */
router.get('/view/all', (req, res) => {
	distributionChannelController.viewAllDistributionChannels().then(result => res.send(result));
})

/* View Distribution Channel from the Database */
router.get('/view/specific/:code', (req, res) => {
	distributionChannelController.viewDistributionChannel(req.params).then(result => res.send(result));
})


module.exports = router;