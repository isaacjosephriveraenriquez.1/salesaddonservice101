const express = require('express');
const uomController = require('../controllers/con_unitOfMeaseure');
const router = express.Router();

/* Add a UOM to the Database */
router.post('/add', (req, res) => {
	uomController.addUom(req.body).then(result => res.send(result));
})

/* View all UOM from the Database */
router.get('/view/all', (req, res) => {
	uomController.viewAllUom().then(result => res.send(result));
})

/* View a specific UOM from the Database */
router.get('/view/:code', (req, res) => {
	uomController.viewUom(req.params).then(result => res.send(result));
})


module.exports = router;