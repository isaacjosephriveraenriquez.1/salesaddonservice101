const express = require('express');
const minimumOrderQuantityController = require('../controllers/con_minimumOrderQuantity');
const router = express.Router();

/* Add a Minimum Order Quantity to the Database */
router.post('/add', (req, res) => {
	minimumOrderQuantityController.addMinimumOrderQuantity(req.body).then(result => res.send(result));
})

/* View All Minimum Order Quantities from the Database */
router.get('/view/all', (req, res) => {
	minimumOrderQuantityController.queryMinimumOrderQuuantity(req.query).then(result => res.send(result));
})

/* View a Specific Minimum Order Quantity from the Database */
router.get('/view/specific' , (req, res) => {
	minimumOrderQuantityController.viewMoq(req.query).then(result => res.send(result));
})

/* Validate a Specific Minimum Order Quantity from the Database */
router.get('/validate/:productCategoryId', (req, res) => {
	minimumOrderQuantityController.validateMoq(req.params).then(result => res.send(result));
})

router.post('/updateMoq', (req, res) => {
	minimumOrderQuantityController.updateMoq(req.body).then(result => res.send(result));
})

router.put('/push', (req, res) => {
	minimumOrderQuantityController.pushToMinimumOrderQuantity(req.body).then(result => res.send(result))
})

router.delete('/pull', (req, res) => {
	minimumOrderQuantityController.pullToMinimumOrderQuantity(req.body).then(result => res.send(result))
})

router.delete('/deleteMoq', (req, res) => {
	minimumOrderQuantityController.deleteMoq(req.body).then(result => res.send(result));
})


module.exports = router;