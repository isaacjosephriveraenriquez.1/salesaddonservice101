const express = require('express');
const tpsController = require('../controllers/con_thirdPartySpecifier');
const router = express.Router();

/* Add a Currency to the Database */
router.post('/add', (req, res) => {
	tpsController.addTps(req.body).then(result => res.send(result));
})

/* View all  from the Database */
//req.query = querystring
router.get('/query', (req, res) => {
	tpsController.queryThirdPartySpecifer(req.query).then(result => res.send(result));
})

/* Dlelete TPS */
router.delete('/deleteThirdPartySpceifier', (req, res) => {
	tpsController.deleteThirdPartySpecifier(req.body).then(result => res.send(result))
})


module.exports = router;