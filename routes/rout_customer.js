const express = require('express');
const router = express.Router();
const customerController = require('../controllers/con_customer');

/* Add Customer to the Database */
router.post('/add', (req, res) => {
	customerController.addCustomer(req.body).then(result => res.send(result));
})

/* View All Customers from the Database */
router.get('/view/all', (req, res) => {
	customerController.viewAllCustomers().then(result => res.send(result));
})

/* View Specific Customer by Account Name */
router.get('/view/specific/:accountName', (req, res) => {
	customerController.viewCustomerByAccountName(req.params).then(result => res.send(result));
})

/* View Specific Customer by Account ID */
router.get('/view/specific/byAccountId/:accountId', (req, res) => {
	customerController.viewCustomerByAccountId(req.params).then(result => res.send(result));
})

module.exports = router;