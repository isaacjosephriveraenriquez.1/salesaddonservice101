const express = require('express');
const router = express.Router();
const productsController = require('../controllers/con_product');

/* View All Materials from the Database */
router.get('/view/all', (req, res) => {
	productsController.getAllProducts().then( result => res.send(result))
})

/* View Specific Material from the Database */
router.get('/view/specific/:materialId', (req, res) => {
	productsController.querySpecificProduct(req.params).then(result => res.send(result));
})

/* View All Materials with the Product Category Indicated */
router.get('/view/byProductCategory/:productCategoryId', (req, res) => {
	productsController.getAllProductsByProductCategory(req.params).then( result => res.send(result))
})

/* View All Materials with the Product Category Indicated for Rapid Entry*/
router.get('/rapidEntry/:productCategoryId', (req, res) => {
	productsController.rapidEntry(req.params).then( result => res.send(result))
})
//add new items base price price // done testing
router.post('/addNewProduct', (req, res) => {
	 productsController.addNewProduct(req.body).then(result => res.send(result))
})
//add customer specific price 
router.post('/addCustomerSpecificPrice', (req, res) => {
	productsController.addCustomerSpecificPrice(req.body).then(result => res.send(result))
})
//Delete Items
router.delete('/deleteCustomerSpecificPrice', (req, res) => {
	productsController.deleteCustomerSpecificPrice(req.body).then(result => res.send(result))
})
//edit

module.exports = router;