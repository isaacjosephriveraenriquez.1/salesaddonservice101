const express = require('express');
const router = express.Router();
const salesOrderController = require('../controllers/con_salesOrder');

/*Create Sales Quote*/
router.post('/add', (req, res) => {
    salesOrderController.addSalesOrder(req.body).then(result => res.send(result))
});

/*Create Sales Quote from Uploaded File */
router.post('/createSalesQuoteFromUpload', (req, res) => {
    salesOrderController.addSalesQuoteFromUpload().then(result => res.send(result))
});

/*Retrieve All Sales Order in the Database*/
router.get('/status/all', (req, res) => {
	salesOrderController.getAllSalesOrder().then(result => res.send(result))
})

/*Retrieve All Status Sales Order - Requestor*/
router.get('/status/all/requestor/:employeeId', (req, res) => {
	salesOrderController.getAllSalesOrderRequestor(req.params).then( result => res.send(result))
})

/*Retrieve All Status Sales Order - Approver*/
router.get('/status/all/approver', (req, res) => {
    salesOrderController.getAllSalesOrderApprover().then(result => res.send(result))
});

/*Retrieve All In Preparation Sales Order*/
router.get('/status/all/inPreparation', (req, res) => {
	salesOrderController.getAllInPreparation().then( result => res.send(result))
})

/*Retrieve All For Approval Sales Order*/
router.get('/status/all/forApproval', (req, res) => {
	salesOrderController.getAllForApproval().then( result => res.send(result))
})

/*Retrieve All Posted Sales Order*/
router.get('/status/all/posted', (req, res) => {
	salesOrderController.getAllPosted().then( result => res.send(result))
})

/*Retrieve All Approved Sales Order*/
router.get('/status/all/approved', (req, res) => {
	salesOrderController.getAllApproved().then( result => res.send(result))
})

/*Retrieve All Cancelled Sales Order*/
router.get('/status/all/cancelled', (req, res) => {
	salesOrderController.getAllCancelled().then( result => res.send(result))
})

/*Retrieve All Rejected Sales Order*/
router.get('/status/all/rejected', (req, res) => {
	salesOrderController.getAllRejected().then( result => res.send(result))
})

/*Retrieve All Posting Error Sales Order*/
router.get('/status/all/postingError', (req, res) => {
	salesOrderController.getAllPostingError().then( result => res.send(result))
})
 
/*Retrieve In Preparation Sales Order - Per Requestor*/
router.get('/status/inPreparation/:employeeId', (req, res) => {
    salesOrderController.inPreparationSalesOrder(req.params).then(result => res.send(result))
});

/*Retrieve For Approval Sales Order - Per Requestor*/
router.get('/status/forApproval/:employeeId', (req, res) => {
    salesOrderController.forApprovalSalesOrder(req.params).then(result => res.send(result))
});

/*Retrieve Posted Sales Order - Per Requestor*/
router.get('/status/posted/:employeeId', (req, res) => {
    salesOrderController.postedSalesOrder(req.params).then(result => res.send(result))
});

/*Retrieve Approved Sales Order - Per Requestor*/
router.get('/status/approved/:employeeId', (req, res) => {
    salesOrderController.approvedSalesOrder(req.params).then(result => res.send(result))
});

/*Retrieve Cancelled Sales Order - Per Requestor*/
router.get('/status/cancelled/:employeeId', (req, res) => {
	salesOrderController.cancelledSalesOrder(req.params).then(result => res.send(result))
});

/*Retrieve Rejected Sales Order - Per Requestor*/
router.get('/status/rejected/:employeeId', (req, res) => {
    salesOrderController.rejectedSalesOrder(req.params).then(result => res.send(result))
});

/*Check External Refenece*/
router.get('/checkExternalReference/accountName/:accountName/externalReference/:externalReference', (req,res) =>{

	const params = {
		accountName :  req.params.accountName, 
		externalReference :  req.params.externalReference
	}
	salesOrderController.externalReference(params).then(result => {
		res.send(result)
	})
});

/*Retrieve Posting Error Sales Order - Per Requestor*/
router.get('/status/postingError/:employeeId', (req, res) => {
    salesOrderController.postingErrorSalesOrder(req.params).then(result => res.send(result))
});

/*Add Accounts Receivable*/
router.put('/add/accountsReceivable/:salesQuoteNo', (req, res) => {
	salesOrderController.addAccountsReceivable(req.body, req.params).then( result => res.send(result))
})

/*View Function*/
router.get('/function/view/:salesQuoteNo', (req, res) => {
	salesOrderController.getSalesOrder(req.params).then( result => res.send(result))
})

/*View Status Function*/
router.get('/function/viewStatus/:salesQuoteNo', (req, res) => {
	salesOrderController.getSalesOrderStatus(req.params).then( result => res.send(result))
})

/*Edit Function*/
router.put('/function/edit/:salesQuoteNo', (req, res) => {
	salesOrderController.editSalesOrder(req.body, req.params).then( result => res.send(result))
})

/*Post Function*/
router.post('/function/post/:salesQuoteNo', (req, res) => {
	salesOrderController.postSalesOrder(req.params, req.body).then( result => res.send(result))
})

/*Approve Function*/
router.post('/function/approve/:salesQuoteNo', (req, res) => {
	salesOrderController.approveSalesOrder(req.params, req.body).then( result => res.send(result))
})

/*Reject Function*/
router.put('/function/reject/:salesQuoteNo', (req, res) => {
	salesOrderController.rejectSalesOrder(req.params).then( result => res.send(result))
})

/*Cancel Function*/
router.put('/function/cancel/:salesQuoteNo', (req, res) => {
	salesOrderController.cancelSalesOrder(req.body, req.params).then( result => res.send(result))
})

/*Set Delivery Block Function*/
router.put('/function/setDeliveryBlock/:salesQuoteNo', (req, res) => {
	salesOrderController.setDeliveryBlock(req.body, req.params).then( result => res.send(result))
})

/*Set Invoice Block Function*/
router.put('/function/setInvoiceBlock/:salesQuoteNo', (req, res) => {
	salesOrderController.setInvoiceBlock(req.body, req.params).then( result => res.send(result))
})

/*For Credit Limit*/
router.get('/allSalesOrderinDatabase/:accountId', (req, res) => {
	salesOrderController.getAllSpecificCustomer(req.params).then(result => res.send(result))
});

/* Sort Sales Order in Ascending Order */
router.get('/sort/ascending/salesOrder', (req, res) => {
	salesOrderController.sortSalesOrderAscending().then(result => res.send(result))
});

/* Sort Sales Order in Descending Order */
router.get('/sort/descending/salesOrder', (req, res) => {
	salesOrderController.sortSalesOrderDescending().then(result => res.send(result))
});

/* Sort Sales Quote in Ascending Order */
router.get('/sort/ascending/salesQuote', (req, res) => {
	salesOrderController.sortSalesQuoteAscending().then(result => res.send(result))
});

/* Sort Status in Ascending Order */
router.get('/sort/ascending/status', (req, res) => {
	salesOrderController.sortStatusAscending().then(result => res.send(result))
});

/* Sort Status in Descending Order */
router.get('/sort/descending/status', (req, res) => {
	salesOrderController.sortStatusDescending().then(result => res.send(result))
});

/* Sort Account ID in Ascending Order */
router.get('/sort/ascending/accountId', (req, res) => {
	salesOrderController.sortAccountIdAscending().then(result => res.send(result))
});

/* Sort Account ID in Descending Order */
router.get('/sort/descending/accountId', (req, res) => {
	salesOrderController.sortAccountIdDescending().then(result => res.send(result))
});

/* Sort Account Name in Ascending Order */
router.get('/sort/ascending/accountName', (req, res) => {
	salesOrderController.sortAccountNameAscending().then(result => res.send(result))
});

/* Sort Account Name in Descending Order */
router.get('/sort/descending/accountName', (req, res) => {
	salesOrderController.sortAccountNameDescending().then(result => res.send(result))
});

/* Sort Sales Group in Ascending Order */
router.get('/sort/ascending/salesGroup', (req, res) => {
	salesOrderController.sortSalesGroupAscending().then(result => res.send(result))
});

/* Sort Sales Group in Descending Order */
router.get('/sort/descending/salesGroup', (req, res) => {
	salesOrderController.sortSalesGroupDescending().then(result => res.send(result))
});

/* Sort Customer Group in Ascending Order */
router.get('/sort/ascending/customerGroup', (req, res) => {
	salesOrderController.sortCustomerGroupAscending().then(result => res.send(result))
});

/* Sort Customer Group in Descending Order */
router.get('/sort/descending/customerGroup', (req, res) => {
	salesOrderController.sortCustomerGroupDescending().then(result => res.send(result))
});

/* Sort Reference in Ascending Order */
router.get('/sort/ascending/reference', (req, res) => {
	salesOrderController.sortReferenceAscending().then(result => res.send(result))
});

/* Sort Reference in Descending Order */
router.get('/sort/descending/reference', (req, res) => {
	salesOrderController.sortReferenceDescending().then(result => res.send(result))
});

/* Sort Amount in Ascending Order */
router.get('/sort/ascending/amount', (req, res) => {
	salesOrderController.sortAmountAscending().then(result => res.send(result))
});

/* Sort Amount in Descending Order */
router.get('/sort/descending/amount', (req, res) => {
	salesOrderController.sortAmountDescending().then(result => res.send(result))
});

/* Sort Validator in Ascending Order */
router.get('/sort/ascending/validator', (req, res) => {
	salesOrderController.sortValidatorAscending().then(result => res.send(result))
});

/* Sort Validator in Descending Order */
router.get('/sort/descending/validator', (req, res) => {
	salesOrderController.sortValidatorDescending().then(result => res.send(result))
});

/* Sort Delivery Block in Ascending Order */
router.get('/sort/ascending/deliveryBlock', (req, res) => {
	salesOrderController.sortDeliveryBlockAscending().then(result => res.send(result))
});

/* Sort Delivery Block in Descending Order */
router.get('/sort/descending/deliveryBlock', (req, res) => {
	salesOrderController.sortDeliveryBlockDescending().then(result => res.send(result))
});

/* Sort Cancel Reason in Ascending Order */
router.get('/sort/ascending/cancelReason', (req, res) => {
	salesOrderController.sortCancelReasonAscending().then(result => res.send(result))
});

/* Sort Cancel Reason in Descending Order */
router.get('/sort/descending/cancelReason', (req, res) => {
	salesOrderController.sortCancelReasonDescending().then(result => res.send(result))
});

module.exports = router;