const express = require('express');
const headerController = require('../controllers/con_header');
const router = express.Router();

/*Upload Header*/
router.post('/upload', (req, res) => {
	headerController.uploadHeader(req.body).then(result => res.send(result));
})

/*Retrieve the Length of the Header Collection*/
router.get('/all', (req, res) => {
	headerController.getAllHeader().then(result => res.send(result));
})

module.exports = router;