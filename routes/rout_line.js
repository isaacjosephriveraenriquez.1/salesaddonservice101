const express = require('express');
const lineController = require('../controllers/con_line');
const router = express.Router();

/*Upload Line*/
router.post('/upload', (req, res) => {
	lineController.uploadLine(req.body).then(result => res.send(result));
})

/*Retrieve the Length of the Line Collection*/
router.get('/all', (req, res) => {
	lineController.getAllLine().then(result => res.send(result));
})

module.exports = router;