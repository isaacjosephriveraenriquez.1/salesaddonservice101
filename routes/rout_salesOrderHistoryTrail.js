const express = require('express');
const router = express.Router();
const salesOrderHistoryTrailController = require('../controllers/con_salesOrderHistoryTrail')

//add history Trail route
router.post('/addHistory', (req, res) => {
    salesOrderHistoryTrailController.addNewHistoryEntry(req.body).then(result => res.send(result))
});

module.exports = router;