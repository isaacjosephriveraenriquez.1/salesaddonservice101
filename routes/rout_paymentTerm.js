const express = require('express');
const paymentTermsController = require('../controllers/con_paymentTerm');
const router = express.Router();

/* Add a Payment Term to the Database */
router.post('/add', (req, res) => {
	paymentTermsController.addTerms(req.body).then(result => res.send(result));
})

/* View All Payment Terms from the Database */
router.get('/view/all', (req, res) => {
	paymentTermsController.viewAllTerms().then(result => res.send(result));
})

/* View a Specific Payment Term from the Database */
router.get('/view/specific/:paymentTermsId', (req, res) => {
	paymentTermsController.viewTerm(req.params).then(result => res.send(result));
})


module.exports = router;